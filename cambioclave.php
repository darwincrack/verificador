<?php include('seguridad_trans.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="funciones/funciones1.js"></script>
<script type="text/javascript">
function submit_cambio(){
	var clave1;
	var clave2;
	
	if (document.f_cambioclave.contrasena1.value != document.f_cambioclave.contrasena2.value){
		alert('Clave Nueva No Coincide');
		return false;
	}
	
	clave1=trim(document.f_cambioclave.contrasena1.value);
	if (clave1.length == 0){
		alert('Clave Nueva No Debe ir en Blanco');
		document.f_cambioclave.contrasena1.focus();
		return false;
	}
	
	clave2=trim(document.f_cambioclave.contrasena2.value);
	if (clave2.length == 0){
		alert('Clave Nueva No Debe ir en Blanco')
		document.f_cambioclave.contrasena2.focus();
		return false;
	}
	
	document.f_cambioclave.action='cambioclave2.php'
	document.f_cambioclave.submit();

}
</script>
</head>
<body>
<div class="contenedor">
	<h1>Cambio de Clave</h1>
    <h3><font size="-4">(10 Digitos Max.)</font></h3>
</div>
<div class="contenedor" id="consulta">      	
	<form  action="" id="consulta" method="POST" name="f_cambioclave" onsubmit="return submit_cambio();">	
      <table align="center" width="35%" border="0" cellspacing="0" cellpadding="1" class="tablas">
      <tr>
        <td height="27" colspan="2" align="center" bgcolor="#FFFFFF"><span><b>Introduzca su Clave</b></span>&nbsp;</td>
      </tr>
      <tr>
        <td width="68%" align="right"><font size="2">Clave Actual:</font></td>
        <td width="32%"><input type="password" name="claveactual" size="8" maxlength="10" /></td>
      </tr>
      <tr>
        <td align="right"><font color="#000000" size="2">Clave Nueva:</font></td>
        <td><input type="password" name="contrasena1" size="8" maxlength="10" /></td>
      </tr>
      <tr>
        <td align="right"><font color="#000000" size="2">Repita Clave Nueva:</font></td>
        <td><input type="password" name="contrasena2" size="8" maxlength="10" /></td>
      </tr>
      <tr>
        <td colspan="2" align="center"><input type="submit" value="ENTRAR" /></td>
      </tr>
      </table>
    </form>
</div>
</body>
</html>