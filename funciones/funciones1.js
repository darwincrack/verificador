
function rndDecimal(valor,cant_decimales){
	var valorNdecimales = null;
	valorNdecimales=Math.round(valor*cant_decimales*10) / (cant_decimales*10); 
	return valorNdecimales.toFixed(2) ;
}
function Mostrar(Obj){
	document.getElementById(Obj).style.display='block';
}
function Ocultar(Obj){
	document.getElementById(Obj).style.display='none';
}	
function MostrarTabla(obj_id){
	document.getElementById(obj_id).style.display='table';
}

function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}
 
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++){
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1){
         IsNumber = false;
      }
   }
   return IsNumber;
}
function IsNumericInt(sText)
{ //funcion identica a la anterior pero solo para numero enteros
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++){
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1){
         IsNumber = false;
      }
   }
   return IsNumber;
}

function soloNumeros(evt){
//asignamos el valor de la tecla a keynum
	if(window.event){// IE
		keynum = evt.keyCode;
	}else{
		keynum = evt.which;
	}
	//comprobamos si se encuentra en el rango
	if((keynum>47 && keynum<58) || keynum == 8 || keynum==46 || keynum ==0){  //46 es el punto, 47 es el simbolo de dividir
		return true;
	}else{
		return false;
	}
}

function soloEnteros(evt){ //para retornar solo numeros enteros
//asignamos el valor de la tecla a keynum
	if(window.event){// IE
		keynum = evt.keyCode;
	}else{
		keynum = evt.which;
	}
	//alert (keynum);
	//comprobamos si se encuentra en el rango
	if((keynum>47 && keynum<58) || keynum == 8 || keynum ==0){
		return true;
	}else{
		return false;
	}
}


//hecho por darwin 28/11/2012
function soloNumeros2(evt){
//asignamos el valor de la tecla a keynum
	if(window.event){// IE
		keynum = evt.keyCode;
	}else{
		keynum = evt.which;
	}
	//comprobamos si se encuentra en el rango
	if((keynum>45 && keynum<58) || keynum == 8  || keynum ==0){  //46 es el punto, 47 es el simbolo de dividir
		return true;
	}else{
		return false;
	}
}





function validar_rif(){
	if (trim($('#nombre_empresa').val()).length<1){
	alert('Nombre de la Persona Natural o Juridica Otorgante, no debe estar vacio');
	$('#nombre_empresa').focus();
	return false;
	}
	/// validar SI EL RIF CONTIENE 0 O 1 CARACTER
if ((trim($('#rif').val()).length==0)||(trim($('#rif').val()).length==1)){
	alert('El Campo Rif no debe estar vacio o contener un solo caracter');
	$('#rif').focus();
	$('#rif').val('');
	return false;
	}	

// VALIDA SI LA PRIMERA LETRA DEL RIF ESTA ENTRE LAS PERMITIDAS
 if ((trim($('#rif').val()).charAt(0)!='V') && (trim($('#rif').val()).charAt(0)!='G')&& (trim($('#rif').val()).charAt(0)!='J') && (trim($('#rif').val()).charAt(0)!='E')){
	alert("El RIF debe comenzar con algunas de estas letras: J,G,V,E");
	$('#rif').focus();
	return false;
	}
	
// VALIDA SI EL RIF ES MAYOR DE UN CARACTER, Y AGREGA CEROS A LA IZQUIERDA PARA COMPLETAR
if((trim($('#rif').val()).length>1)&&(trim($('#rif').val()).length<10)){
	$('#rif').val(pad_rif($('#rif').val().charAt(0),$('#rif').val().substring(1,10), 9));
}



// VALIDA SI A PARTIR DE LA SEGUNDA LETRA ES NUMERICO
 if(IsNumericInt(trim($('#rif').val()).substring(2,10))==false){
	alert('El RIF debe estar en este formato: J000000000');
	$('#rif').focus();
	$('#rif').val('');
	return false;
	}
	//SI SE CUMPLE TODO LO ANTERIOR ES PORQUE EL RIF ES VALIDO
	return true;
	
	
	}
//funcion que formatea el rif;
function pad_rif(char_inicial,n, length){
   n = n.toString();
   while(n.length < length) n = "0" + n;
   return char_inicial+n;
}



//validar accionista y direccion del local

function validar_accionista_direc_local()
{
	
	if ( ! $('#calle').length){
 	//Ejecutar si existe el elemento
	return true;
	}
	var calle 				= 	trim(document.getElementById('calle').value);
	var avenida				= 	trim(document.getElementById('avenida').value);
	var urb					= 	trim(document.getElementById('urb').value);
	var esq					= 	trim(document.getElementById('esq').value);
	var edif				= 	trim(document.getElementById('edif').value);
	var casa				= 	trim(document.getElementById('casa').value);
	var cc					= 	trim(document.getElementById('cc').value);
	var galpon				= 	trim(document.getElementById('galpon').value);
	var local				= 	trim(document.getElementById('local').value);
	var ci_accionista		= 	document.getElementById('ci_accionista');
	var total_char;
	var tipo_acto			=	$("#tipo_acto").val();
	
	if(tipo_acto == "RESERVA DE NOMBRE" || tipo_acto == "FIDEICOMISOS" || tipo_acto == "PODER JURIDICO" || tipo_acto == "PODER NATURAL" )
	{
		return true;
	}
	
	total_char=parseInt(calle.length)+parseInt(avenida.length)+parseInt(urb.length)+parseInt(esq.length)+parseInt(edif.	length)+parseInt(casa.length)+parseInt(cc.length)+parseInt(galpon.length)+parseInt(local.length);
	
	
	if($( "#parroquia option:selected" ).val()=="")
	{
		alert('Debes Seleccionar una Parroquia');
		return false;
	}
	
	
	if(total_char<=0)
	{
		alert('La Dirección Fiscal del Local no Puede Estar Vacia');
		return false;
	}
	
	
	if($( "#nacionalidad_accionista option:selected" ).val()=="")
	{
		alert('Debe Seleccionar la nacionalidad del Accionista');
		return false;
	}

	if( ! $.isNumeric( ci_accionista.value ))
	{
		alert("Error en la Cedula del Accionista");
		document.getElementById('ci_accionista').focus();
		return false;
	}

	
	if($( "#nacionalidad_accionista option:selected" ).val()=="J")
	{
		if (trim($('#razonsocial').val()).length<1)
		{
			alert('La razon social es obligatoria');
			$('#razonsocial').focus();
			return false;
		}

	
	}
	else
	{
		
		if (trim($('#name1').val()).length<1)
		{
			alert('El primer nombre, es obligatorio');
			$('#name1').focus();
			return false;
		}
	
		if (trim($('#apellido1').val()).length<1)
		{
			alert('El  primer apellido es obligatorio');
			$('#apellido1').focus();
			return false;
		}
	}


	return true;
	
}




//AJAX- BUSCA LA CI DEL ACCIONISTA EN LA BASE DE DATOS
/*function get_ci_accionista()
{
	
	var nacionalidad=$( "#nacionalidad_accionista option:selected" ).val();
	var ci_accionista= trim(document.getElementById('ci_accionista').value);
	if (ci_accionista.length<=0) return false;

	 $("#img").html('<IMG SRC="images/ajax-loader_mini.gif">');
	 
	 if(nacionalidad=="J")
	 {
		
		$("#content_razon_social").load("get_name_accionista.php",{nacionalidad: nacionalidad,ci_accionista: 		ci_accionista},function(responseTxt,statusTxt,xhr){
		  if(statusTxt=="error")
		  {
			  alert("Error: "+xhr.status+": "+xhr.statusText);
		  }
		  else
		  {
			  $("#img").html('');
		  }	
		});
	
		
		
			
	 }
	 else
	 {
		$("#content_name_accionista").load("get_name_accionista.php",{nacionalidad: nacionalidad,ci_accionista: 		ci_accionista},function(responseTxt,statusTxt,xhr){
		  if(statusTxt=="error")
		  {
			  alert("Error: "+xhr.status+": "+xhr.statusText);
		  }
		  else
		  {
			  $("#img").html('');
		  }	
		});
	
	}

}

*/



 $(document).ready(function(){  // comienza jquery
 //AJAX- BUSCA LA CI DEL ACCIONISTA EN LA BASE DE DATOS
  $("#ci_accionista").blur(function(){
	  
	var nacionalidad=$( "#nacionalidad_accionista option:selected" ).val();
	var ci_accionista= trim(document.getElementById('ci_accionista').value);
	if (ci_accionista.length<=0) return false;

	 $("#img").html('<IMG SRC="images/ajax-loader_mini.gif">');
	 
	 if(nacionalidad=="J")
	 {
		$.getJSON('get_name_accionista.php',
		{nacionalidad:nacionalidad,
	    ci_accionista:ci_accionista},
		function(data)
		{
			$.each(data , function(i, v) {
				razon_social=v.razon_social;
  			 });
			 
			 if(razon_social==undefined)
			 {
				$("#razonsocial").val(rif_seniat(nacionalidad+ci_accionista));
			 }
			 else
			 {
			 	$("#razonsocial").val(razon_social);
			 }
			 $("#img").html('');
		});
		 
		 
		/*
		$("#content_razon_social").load("get_name_accionista.php",{nacionalidad: nacionalidad,ci_accionista: ci_accionista},function(responseTxt,statusTxt,xhr){
		  if(statusTxt=="error")
		  {
			  alert("Error: "+xhr.status+": "+xhr.statusText);
		  }
		  else
		  {
			  $("#img").html('');
		  }	
		});*/
			
	 }
	 else
	 {
		 
		$.getJSON('get_name_accionista.php',
		{nacionalidad:nacionalidad,
	    ci_accionista:ci_accionista},
		function(data)
		{
			
			$.each(data , function(i, v) {
				if (i != 'error')
				{
					$("#name1").val(v.nombre1);
					$("#name2").val(v.nombre2);
					$("#apellido1").val(v.apellido1);
					$("#apellido2").val(v.apellido2);
					
				}
  			 });
			 
			$("#img").html(''); 
		}); 
		 
/*
		$("#content_name_accionista").load("get_name_accionista.php",{nacionalidad: nacionalidad,ci_accionista: 		ci_accionista},function(responseTxt,statusTxt,xhr){
		  if(statusTxt=="error")
		  {
			  alert("Error: "+xhr.status+": "+xhr.statusText);
		  }
		  else
		  {
			  $("#img").html('');
		  }	
		});*/
	
	}


   });
 
 
$('#nacionalidad_accionista').on('change', function() {  
		 
		 if($(this).val()=="J")
		 {
		 	 $("#content_name_accionista").css("display","none");
			 $("#content_razon_social").css("display","inline");
			 $("#name1").val('');
			 $("#name2").val('');
			 $("#apellido1").val('');
			 $("#apellido2").val('');
		 }
		 else
		 {
			$("#content_razon_social").css("display","none");
			$("#content_name_accionista").css("display","inline");
			$("#razonsocial").val('');
		 }
		
		 });
		 
	

		 
// obtener direccion del local
    $("#rif").blur(function(){
		
	if($("#rif").val().length==8)
{
		
		var rif=pad_rif($('#rif').val().charAt(0),$('#rif').val().substring(1,10), 9);
		var nombre_empresa;
	    $("#img_rif").html('<IMG SRC="images/ajax-loader_mini.gif">');
		$.getJSON('get_direccion_local.php',
		{rif:rif},
		function(data)
		{
			
			$.each(data , function(i, v) {
				nombre_empresa=v.nombre_empresa;
				if(data.error!=1)
				{
				  $("#avenida").val(v.av);
				  $("#calle").val(v.calle);
				  $("#urb").val(v.urbanizacion);
				  $("#esq").val(v.esquina);
				  $("#edif").val(v.edificio);
				  $("#casa").val(v.casa);
				  $("#cc").val(v.centro_comercial);
				  $("#galpon").val(v.galpon);
				  $("#local").val(v.local);
				  $("#parroquia option[value="+ v.parroquia_id +"]").attr("selected",true);
				}
				
				
  			 });
			 
			 if(nombre_empresa==undefined)
			 {
				rif=rif_seniat(rif);
				if(rif!='')
				{
					$("#nombre_empresa").val(rif);
				}
			 }
			 else
			 {
			 	$("#nombre_empresa").val(nombre_empresa);
			 }
			 $("#img_rif").html('');
			 
			
		});
	}
			
    });
	
	
//////solo para buscar rif

function rif_seniat(rif){

	var rif= rif;
	var razon_social='';
	var parametros = {
	"rif" : rif
	};
	$.ajax({
	dataType:  'json',	
	url:   'funciones/seniat/get_rif.php',
	data:  parametros,
	async: false,
	success:  function (data) {
		
			if(data.code_result==1) 
			{
				razon_social= data.seniat.nombre;
			}
	},
	});
	
return razon_social;	
	
}

	


// validar num de planilla
$("#num_planilla").blur(function(){
	if($("#num_planilla").val().length>=1){
	   $("#img_num_planilla").html('<IMG SRC="images/ajax-loader_mini.gif">');
		var tipo_planilla = $("#tipo_planilla").val();
		var num_planilla  = $("#num_planilla").val();
		$.getJSON('get_planilla.php',
		{tipo_planilla:tipo_planilla,
	    num_planilla  :num_planilla},
		function(data)
		{
			
			if(data.result==1)
			{
			    $("#img_num_planilla").html('<IMG SRC="images/cancel_16x16.png"> <i style="display:inline;color: #FFE382;font-weight: bold;">Ya Existe en el Sistema<i>');
			}
			else if (data.result==0)
			{
				$("#img_num_planilla").html('<IMG SRC="images/check_16x16.png"> <i style="display:inline;color: #11F611;font-weight: bold;">Planilla Valida<i>');
			}
		});
		 
	}
	else
	{
		 $("#img_num_planilla").html('');
	}
});

 }); // fin jquery



//hecho por yosmar 27-10-2014
//carga la informacion de la planilla se utiliza en rptmodifplanilla.php
function open_dialog(planilla_id){
	$("#dialog").dialog({
   	modal: true,
   	title: "Informacion detallada de la planilla",
   	height:230,
   	width: 455,
   	minWidth: 430,
   	maxWidth: 1100,
   	show: { effect: "clip", duration: 800 },
   	hide: "scale"
	
	});	
	
	$('#detalles_planillas').html('<br /><br /><br /><center><img src="../images/ajax-loader.gif" width="38" height="38"/></center>');
	$("#detalles_planillas").load("list_detalles_planilla_ajax.php",{planilla_id: planilla_id},function(responseTxt,statusTxt,xhr){
    	if(statusTxt=="error")
      		alert("Error: "+xhr.status+": "+xhr.statusText);
  		});
  }



