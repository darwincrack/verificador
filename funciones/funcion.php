<?php 
include ('validacion.php');
//require_once('./conex.php');
//$uri=$_SERVER['HTTP_REFERER'];


function encrypt($string, $key) {
   $result = '';
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)+ord($keychar));
      $result.=$char;
   }
   return base64_encode($result);
}

function decrypt($string, $key) {
   $result = '';
   $string = base64_decode($string);
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($key, ($i % strlen($key))-1, 1);
      $char = chr(ord($char)-ord($keychar));
      $result.=$char;
   }
   return $result;
}

function porcentaje($cantidad,$porciento){
	return $cantidad*($porciento/100);
}

function bsf($monto){
	$monto=str_replace(',','.',$monto);
	if (trim($monto)==''){
		return '';
	}else{
		settype($monto,"float");
		return number_format($monto,2,',','.');
	}
}

function bsf3($monto){
	if (trim($monto)==''){
		return '';
	}else{
		settype($monto,"float");
		return number_format($monto,2,'.','');
	}
}

function texto_limpio($valor){
	return trim(stripslashes(html_entity_decode($valor,ENT_QUOTES)));
}
function cedula($ced){
	return number_format($ced,0,',','.');
}

function SinHtml($str){
	return remove_HTML(stripslashes(htmlspecialchars($str, ENT_QUOTES)));
}

function ConHtml($str){
	return htmlentities($str,ENT_QUOTES);
}
function mysql_fecha($fecha){
	$val=substr($fecha,6,4).'-'.substr($fecha,3,2).'-'.substr($fecha,0,2);
	return $val;
}


function getBrowser(){
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	if(strpos($useragent,"MSIE") !== false && strpos($useragent,"Opera") === false && strpos($useragent,"Netscape") === false) {
		$found = preg_match("/MSIE ([0-9]{1}\.[0-9]{1,2})/",$useragent, $mathes);
		if($found)  {
			return "Internet Explorer " . $mathes[1];
		}
	}elseif(strpos($useragent,"Gecko"))  {
		$found = preg_match("/Firefox\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
		if($found)  {
			return "Mozilla Firefox " . $mathes[1];
		}
		$found = preg_match("/Netscape\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);  
		if($found)  {
			return "Netscape " . $mathes[1];
		}
		$found = preg_match("/Chrome\/([^\s]+)/",$useragent, $mathes);
		if($found)  {
			return "Google Chrome " . $mathes[1];
		}
		$found = preg_match("/Safari\/([0-9]{2,3}(\.[0-9])?)/",$useragent, $mathes);
		if($found)  {
			return "Safari " . $mathes[1];
		}
		$found = preg_match("/Galeon\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
		if($found)  {
			return "Galeon " . $mathes[1];
		}
		$found = preg_match("/Konqueror\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
		if($found)  {
			return "Konqueror " . $mathes[1];  
		}
		return "Gecko based";
	}elseif(strpos($useragent,"Opera") !== false){
		$found = preg_match("/Opera[\/ ]([0-9]{1}\.[0-9]{1}([0-9])?)/",$useragent,$mathes);
		if($found)  {  
			return "Opera " . $mathes[1];
		}
	}elseif (strpos($useragent,"Lynx") !== false)  {
		$found = preg_match("/Lynx\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
		if($found)  {
			return "Lynx " . $mathes[1];
		}
	}elseif (strpos($useragent,"Netscape") !== false)  {
		$found = preg_match("/Netscape\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
		if($found)  {
			return "Netscape " . $mathes[1];
		}
	}else{
		return false;  
	}  
}


function procedencia_reportes_admin($str){
	/*PROCEDIMIENTO QUE VALIDA SI LA PAGINA SE INVOCO POR UN ADMINISTRADOR DESDE EL MENU DE "REPORTES DE ADMINISTRADORES"*/
	$salida=false;
	$piezas = explode('/',$str);
	$total_elementos = count($piezas);
	$directorio=$piezas[$total_elementos-2];
	if (($directorio=='reportes')&&($_SESSION["nivel"]== 1)){
		$salida=true;
	}
	return $salida;
}


/* hacer consulta a la tabla ut donde vigente=1 */



//$link=mysql_connect($host.':'.$puerto, $user,$clave) or die(mysql_error());
//mysql_select_db($db) or die(mysql_error());		
//$sql = "select ut.`valor` from ut where ut.`vigente` = 1";
//$result = mysql_query($sql) or die(mysql_error());
//
//$row = mysql_fetch_assoc($result); /*VALOR DE LA UNIDAD TRIBUTARIA ACTUAL, ESTE VALOR ES SOBREESCRITO CUANDO LA PLANILLA YA HA SIDO REGISTRADA*/
//$ut=$row["valor"];
//mysql_close($link);


$ut=107;

?>