<?php
include('seguridad_trans.php');
include('conex.php');
include('funciones/funcion.php');?><head>
	<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</head>
<?php

if (!isset($_GET["p"])){
	die();
}else{
	$p=$_GET["p"];
}
if (!isset($_GET["adm"])){ //variable que indica si se busca la informacion de planilla para reimprimir por parte de la seccion de 				
	$adm=0;	 			//reportes para que salga el menu principal pertenecientea reportes
}else{
	$adm=$_GET["adm"];
}

if (!isset($_GET["planilla"])){
	die();
}else{
	$num_planilla=$_GET["planilla"];
}

if (isset($_GET["saved"])){
	$guardo=$_GET["saved"];
}else{
	$guardo=0; /*variable es uno o cero,  es para redirigir el browser a inicio despues de mandar a imprimir*/
}

//la variable $p nos indica si los datos que se van a buscar son para imprimir, o solo para mostrar



$con = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_error()) {
	die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
}
$stmt = $con->stmt_init();
$stmt->prepare('call sel_planilla(?)');
$stmt->bind_param('i',$num_planilla);
if(!$stmt->execute()){
	throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
}else{
	$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
	$cuantos_registros = $stmt->num_rows;
	if($cuantos_registros>0){
		$stmt->bind_result($planilla_id_sp,$numeral_sp,$num_planilla_sp,$fecha_sp,$registro_sp,$estado_sp,
						   $nombre_presentante_sp,$nacionalidad_sp,$cedula_presentante_sp,$nombre_empresa_sp,$rif_sp,
						   $tipo_acto_sp,$ut_sp,$monto_sp,$folio_sp,$total_sp,$creadopor_sp,$fecha_creado_sp,$ip_sp,$obsrv_sp,
						   $porcentaje_sp,$capital_sp,$tasa_sp);
		while($stmt->fetch()){	
			$planilla_id=$planilla_id_sp;
			$numeral=$numeral_sp;
			$num_planilla=$num_planilla_sp;
			$fecha=$fecha_sp;
			$registro=$registro_sp;
			$estado=ConHtml($estado_sp);
			$nombre_presentante=$nombre_presentante_sp;
			$nacionalidad=$nacionalidad_sp;
			$cedula_presentante=$cedula_presentante_sp;
			$nombre_empresa=$nombre_empresa_sp;
			$rif=$rif_sp;
			$tipo_acto=$tipo_acto_sp;
			$ut=$ut_sp;
			$monto=$monto_sp;
			$folio=$folio_sp;
			$total=$total_sp;
			$creadopor=$creadopor_sp;
			$fecha_creado=$fecha_creado_sp;
			$ip=$ip_sp;
			$obsrv=$obsrv_sp;
			$porcentaje=$porcentaje_sp;
			$capital=$capital_sp;
			$tasa=$tasa_sp;
		}                      	
	}else{
    	?><link href="styles/contenido.css" rel="stylesheet" type="text/css" />
		<div class="contenedor" id="resp_serv">
			<br  /><br  /><br  />
			<table width="58%" border="1" cellspacing="0" cellpadding="0"  align="center">
				<tr>
					<td bgcolor="#FF0000">NUMERO DE PLANILLA NO ENCONTRADA</td>
				</tr>
			</table>
		</div><?php
		die();
	}
}
$stmt->free_result();
$stmt->close();
while($con->next_result()) { }
$con->close();
		
if ($p==1){/* SI $P = 1 IMPRIME, 0 NO IMPRIME SOLO MUESTRA PARA PAGAR, 2 MUESTRA SOLO PARA REIMPRIMIR   */ 
	$browser = getBrowser();
	if (substr($browser,0,17) <> 'Internet Explorer') {
		die('IMPRESION SOLO DISPONIBLE PARA NAVEGADOR: INTERNET EXPLORER 7, 8');
	}else{
		?><script type="text/javascript">window.open("imprime.php?fecha=<?php echo $fecha ?>&registro=<?php echo $registro ?>&estado=<?php echo urlencode($estado) ?>&nombre_presentante=<?php echo urlencode($nombre_presentante) ?>&nacionalidad=<?php echo $nacionalidad ?>&ced_present=<?php echo urlencode($cedula_presentante) ?>&nombre_empresa=<?php echo urlencode($nombre_empresa) ?>&rif=<?php echo urlencode($rif) ?>&tipo_acto=<?php echo urlencode($tipo_acto) ?>&monto=<?php echo bsf($monto) ?>&porcentaje=<?php echo urlencode($porcentaje) ?>&tasa=<?php echo bsf($tasa) ?>&total=<?php echo bsf($total) ?>&capital=<?php echo bsf($capital) ?>","Imprime","location=0,toolbar=no,titlebar=no,menubar=no,status=no,resizable=yes");<?php
		if ($guardo==1){
			?>window.location="body.php";<?php	
		}else{
			?>history.go(-1);<?php
		}
	}
	?></script><?php	
}elseif ($p==0 || $p==2){
	if ($numeral==1){
		$pagina='lit_1.php?';
	}elseif ($numeral==2){
		$pagina='lit_2.php?';	
	}elseif ($numeral==3){
		$pagina='lit_3.php?';
	}elseif ($numeral==4){
		$pagina='lit_4.php?';
	}elseif ($numeral==5){
		$pagina='lit_5.php?';
	}elseif ($numeral=='7a'){
		//$pagina='lit_7.php?tipo_persona=2&';
		$pagina='lit_7a.php?';
	}elseif ($numeral=='7b'){
		//$pagina='lit_7.php?tipo_persona=1&';
		$pagina='lit_7b.php?';
	}elseif ($numeral==6){
		$pagina='lit_6.php?';
	}elseif ($numeral==9){
		$pagina='lit_9.php?';
	}
	if ($p==0){
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call sel_planilla2(?)'); // comprobamos que no se haya pagado la planilla, devuleve 1 si consigue el pago
		$stmt->bind_param('i',$num_planilla);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
			$cuantos_registros = $stmt->num_rows;
			if($cuantos_registros>0){
				$stmt->bind_result($resp);
				while($stmt->fetch()){	
					if ($resp==1){ /*resp es 1 si existe pago registrado en la planilla*/
						?><link href="styles/contenido.css" rel="stylesheet" type="text/css" />
						 <div class="contenedor" id="resp_serv">
							<br  /><br  /><br  />
							<table width="58%" border="1" cellspacing="0" cellpadding="0"  align="center">
								<tr>
									<td bgcolor="#FF0000">LA PLANILLA YA FUE PAGADA</td>
								</tr>
							</table>
						</div><?php
						$stmt->free_result();
						$stmt->close();
						while($con->next_result()) { }
						$con->close();						
						die();
					}
				}                      	
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
		$con->close();		
	}		
	
	?><script type="text/javascript">window.location="<?php echo $pagina ?>adm=<?php echo $adm ?>&i_folios=<?php echo addslashes($folio) ?>&p=<?php echo $p ?>&fecha=<?php echo $fecha ?>&registro=<?php echo $registro ?>&estado=<?php echo $estado ?>&nombre_presentante=<?php echo urlencode($nombre_presentante) ?>&nacionalidad=<?php echo $nacionalidad ?>&ced_present=<?php echo urlencode($cedula_presentante) ?>&nombre_empresa=<?php echo  urlencode($nombre_empresa) ?>&rif=<?php echo urlencode($rif) ?>&tipo_acto=<?php echo urlencode($tipo_acto) ?>&monto=<?php echo bsf($monto) ?>&porcentaje=<?php echo urlencode($porcentaje) ?>&tasa=<?php echo bsf($tasa) ?>&total=<?php echo bsf($total) ?>&i_monto=<?php echo $capital ?>&planilla=<?php echo $num_planilla ?>&lit=<?php echo $numeral ?>&reg_id=";</script><?php
}
?>