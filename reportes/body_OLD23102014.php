<?php include('seguridad_adm.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Menu</title>
<script type="text/javascript" src="../funciones/ajax.js"></script>
<link href="../styles/estilo.css" rel="stylesheet" type="text/css" />
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
</head>
<body onload="loadurl('menu.php','menu')">
<span id="menu">
</span>
<div class="contenedor" align="center">
	<h1>Reportes</h1>
    <ul>
        <!--<li><span style="background-color:#FFFF00"><a href="../reg_pago.php">Cargar Pagos a Planillas</a></span><IMG SRC="images/nuevo.gif"></li>-->
         <?php if ($_SESSION["func_area_id"]==5)
		 {
			 echo "<li><span style='background-color:#FFFF00'><a href='edit_pagosplanillas.php'>Corrección de Depositos (Por Numero de Planilla)</a></span><IMG SRC='images/nuevo.gif'></li>";
		 }?>
         
        <li><a href="edit_pagosplanillas.php">Modificar Pagos(todos los usuarios)</a></li>
        <li><a href="../reg_pago.php">Cargar Pagos a Planillas</a></li>
        <li><a href="rptnumplanilla.php">Buscar por Numero de Planilla</a></li>
        <li><a href="rptempresa.php">Buscar Empresas</a></li>
        <li><a href="rptdepo.php">Buscar Depositos</a></li>
        <li><a href="rptptoventa2.php">Buscar Puntos de Venta</a></li>
        <li><span style="background-color:#FFFF00"><a href="rpt_name_presentante.php">Buscar Presentante</a></span></li>
        <li><a href="rpt_ced_presentante.php" >Buscar Presentante por Numero de Cedula</a></li>
        
        <br />
		<li><a href="rptplanilla.php">Solo Informaci&oacute;n de Planillas Cargadas en el Sistema (con o sin pagos asociados) </a></li>
        <br />
    	<li><a href="rptpagoplanilla.php">Planillas con Pagos Asociados (Basado en la Fecha de Guardado de las Planillas)</a></li>
        <li><a href="rpttotalesplanilla.php">Totales Por Registro (Basado en la Fecha de Guardado de los Pagos)</span></a></li>
		<li><a href="rptdetalplanilla.php">Información Detallada Por Registro (Basado en la Fecha de Guardado de los Pagos)</a></li>
        <br />                
        <li><a href="rptdeposito.php">Listado de Depositos Cargados en Sistema</a></li>
        <li><a href="rptptoventa.php">Listado de Pagos mediante Pto de Venta Cargados en Sistema</a></li>
        <li><a href="rpttimbre.php">Listado de Pagos mediante Timbres Cargados en Sistema</a></li>
        <br />
        <li><a href="rptnopagoplanilla.php">Planillas sin Pago Asociado</a></li>
        <li><span><a href="rptactos_fecha.php">Total Actos por Rango de Fecha (en la que las planillas fueron Registradas)</a></span></li>
             <li><span style="background-color:#FFFF00"><a href="rptactos_pago.php">Total Actos por Rango de Fecha y Ubicaci&oacute;n (con  pagos asociados)  </a></span></li>
        <br />
      	<li><a href="rptlogin_dia.php">Ingresos y Salidas al Sistema (por dia)</a></span></li>
		<li><a href="rptlogin_rang.php">Ingresos y Salidas al Sistema (por rango de fechas)</a></span></li>      
        <li><a href="darwin.php">probando darwin</a></span></li>         
  	</ul>
</div>
</body>
</html>
