// JavaScript Document
var forma_pago = null;
var banco_id_deposito = null;
var banco_deposito = null;
var cuenta_deposito = null;
var num_deposito= null;
var monto_deposito = null;
var banco_id_transac =null;
var banco_transac = null;
var num_aprob_transac = null;
var monto_transac = null;
var	timbre_id = null;
var	timbre=null;
var cantidad_timbre = null;
var monto_timbre=null;
var cuenta_deposito_ptoVenta=null;

// Color final que se quiere mostrar
var Rfinal = 255;
var Gfinal = 0;
var Bfinal = 0;
// Incremento en el degradado
var inc = 10;
// Para evitar que se pulsen dos filas a la vez y pueda dar problemas
//var concurrencia = "";


var selector = null;
var indice = null;

function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

function OcultarTodo(){
	Ocultar('deposito');
	Ocultar('pto_venta');
	Ocultar('boton_enviar');
}
function MuestraTodo(){
	Mostrar('deposito');
	Mostrar('pto_venta');
	Mostrar('boton_enviar');
}
function attach_file( p_script_url ) {
		// create new script element, set its relative URL, and load it
		script = document.createElement( 'script' );
		script.src = p_script_url;
		document.getElementsByTagName( 'head' )[0].appendChild( script );
}

function check_selected(valor){
	if (valor==0){
		OcultarTodo();
		return false;
	}else if (valor==1){
		OcultarTodo();
		Mostrar('deposito');
		Mostrar('boton_enviar');
		return true;
	}else if (valor==2){
		OcultarTodo();
		Mostrar('pto_venta');
		Mostrar('boton_enviar');
		return true;
	}else if (valor==3){
		MuestraTodo();
		return true;
	}
}
function valida_deposito(){
	
	var sel_banco_obj=document.getElementById('sel_banco');
	var indice_sel_banco = sel_banco_obj.selectedIndex;
	var sel_tipo_obj=document.getElementById('sel_tipo');
	var indice_sel_tipo=sel_tipo_obj.selectedIndex;
	banco_id_deposito = sel_banco_obj.options[indice_sel_banco].value;
	banco_deposito=sel_banco_obj.options[indice_sel_banco].text;
	//alert(document.getElementById('num_deposito').value);
	//num_deposito_obj=document.getElementById('num_deposito').value;
	//num_deposito= trim(num_deposito_obj);
	num_deposito= trim(document.getElementById('num_deposito').value);
	//alert('num_deposito_obj');
	//num_deposito=trim(num_deposito_obj.value);
	monto_deposito=trim(document.getElementById('monto_deposito').value);

	if (banco_id_deposito==0){
		alert('NO SE HA SELECCIONADO UN BANCO EN LA SECCION DEPOSITO');
		sel_banco_obj.focus();
		return false;
	}else{
		var cuenta_deposito_obj = document.getElementById('sel_cuenta');
		var indice_cuenta = cuenta_deposito_obj.selectedIndex;
		cuenta_deposito = cuenta_deposito_obj.options[indice_cuenta].text;
	}
	if(monto_deposito!=''){
		if(IsNumeric(monto_deposito)) {
			if(num_deposito!=''){
				if (IsNumericInt(num_deposito)){
					if (monto_deposito<=0){
						alert('DEBE AGREGAR UN MONTO MAYOR A CERO');
						return false;
					}else{
						if (deposito_repetido(num_deposito,banco_deposito)==false){
							
							if(validar_deposito_bd(num_deposito,banco_id_deposito,indice_sel_tipo,0)=='a9b7ba70783b617e9998dc4dd82eb3c5')
							 {
								alert('ERROR EL NUMERO DEL DEPOSITO YA SE ENCUENTRA EN LA BASE DE DATOS');
								document.getElementById('num_deposito').focus();
								return false;
							 }
							 else
							 {
							return true;	 
							 }
							 							
						}						
					}					
				}else{
					alert('ERROR EN EL NUMERO DEL DEPOSITO');
					document.getElementById('num_deposito').focus();
					return false;
				}
			}else{
				alert('ERROR EN EL NUMERO DEL DEPOSITO');
				document.getElementById('num_deposito').focus();
				return false;			
			}
		}else{
			alert('ERROR EN EL MONTO DEL DEPOSITO');
			document.getElementById('monto_deposito').focus();
			return false;
		}
	}else{
		alert('ERROR EN EL MONTO DEL DEPOSITO');
		document.getElementById('monto_deposito').focus();
		return false;	
	}
}


function deposito_repetido(numdeposit,banco){
	var tabla = document.getElementById('t_deposito');
	var filas = tabla.rows.length;
	if (filas == 1){
		return false;
	}else{
		for (var i=1;i<tabla.rows.length;i++){
			num_deposito_tabla = tabla.rows[i].cells[1].innerHTML;
			banco_deposito_tabla = tabla.rows[i].cells[2].innerHTML;
			if (num_deposito_tabla==numdeposit && banco_deposito_tabla==banco){
				alert('EL DEPOSITO YA SE ENCUNTRA AGREGADO EN LA TABLA');
				return true;			
			}
		}
		return false;
	}
}

function pto_venta_repetido(NumAprob,banco){
	var tabla = document.getElementById('tb_pto_venta');
	var filas = tabla.rows.length;
	if (filas == 1){

		return false;
	}else{
		for (var i=1;i<tabla.rows.length;i++){
			num_aprob_tabla = tabla.rows[i].cells[1].innerHTML;
			banco_aprob_tabla = tabla.rows[i].cells[2].innerHTML;
			if (num_aprob_tabla==NumAprob && banco_aprob_tabla==banco){
				alert('LA TRANSACCION YA SE ENCUNTRA AGREGADA EN LA TABLA');
				return true;			
			}			
		}
		return false;
	}
}

function pto_venta_repetido_con_lote(NumAprob,banco,num_lote){
	var tabla = document.getElementById('tb_pto_venta');
	var filas = tabla.rows.length;
	if (filas == 1){

		return false;
	}else{
		for (var i=1;i<tabla.rows.length;i++){
			num_lote_tabla = tabla.rows[i].cells[1].innerHTML;
			num_aprob_tabla = tabla.rows[i].cells[2].innerHTML;
			banco_aprob_tabla = tabla.rows[i].cells[3].innerHTML;
			if (num_aprob_tabla==NumAprob && banco_aprob_tabla==banco && num_lote_tabla==num_lote){
				alert('LA TRANSACCION YA SE ENCUNTRA AGREGADA EN LA TABLA');
				return true;			
			}			
		}
		return false;
	}
}

function valida_pto_venta(){
	var sel_banco_obj=document.getElementById('sel_banco');
	var indice_sel_banco = sel_banco_obj.selectedIndex;	
	var sel_tipo_obj=document.getElementById('sel_tipo');
	var indice_sel_tipo=sel_tipo_obj.selectedIndex;
	banco_id_transac = sel_banco_obj.options[indice_sel_banco].value;
	banco_transac=sel_banco_obj.options[indice_sel_banco].text;
	num_aprob_transac_obj=document.getElementById('num_deposito');
	num_aprob_transac=trim(num_aprob_transac_obj.value);
	
	monto_transac=trim(document.getElementById('monto_deposito').value);
	
	if (banco_id_transac==0){
		alert('NO SE HA SELECCIONADO UN BANCO EN LA SECCION PUNTO DE VENTA');
		sel_banco_obj.focus();
		return false;
	}
		else{
			var cuenta_deposito_obj = document.getElementById('sel_cuenta');
		var indice_cuenta = cuenta_deposito_obj.selectedIndex;
		cuenta_deposito = cuenta_deposito_obj.options[indice_cuenta].text;
			}
	
	if (num_aprob_transac!=''){
		if (IsNumericInt(num_aprob_transac)){
			if (monto_transac!=''){
				if(IsNumeric(monto_transac)) {
					if (monto_transac<=0){
						alert('MONTO NO PUEDE SER CERO');
						return false;
					}else{
						if (pto_venta_repetido(num_aprob_transac,banco_transac)==false){
							
							if(validar_deposito_bd(num_aprob_transac,banco_id_transac,indice_sel_tipo,0)=='a9b7ba70783b617e9998dc4dd82eb3c5')
							 {
								alert('ERROR EL NUMERO DE APROBACION YA SE ENCUENTRA EN LA BASE DE DATOS');
								num_aprob_transac_obj.focus();;
								return false;
							 }
							 else
							 {
							return true;	 
							 }
							
							
						}
					}		
				}else{
					alert('ERROR EN MONTO (PUNTO DE VENTA)');
					document.getElementById('monto_deposito').focus();
					return false;			
				}
			}else{
				alert('ERROR EN MONTO (PUNTO DE VENTA)');
				document.getElementById('monto_deposito').focus();
				return false;		
			}
		}else{
			alert('ERROR EN NUMERO DE APROBACION');
			num_aprob_transac_obj.focus();
			return false;
		}
	}else{
		alert('ERROR EN NUMERO DE APROBACION');
		num_aprob_transac_obj.focus();
		return false;	
	}
	//verifica en la bd si ya esta numero de aprobacion existe
//	var status=validar_deposito_bd(num_aprob_transac,banco_id_transac);
//	if (status==1)
//	{
//		alert('ERROR EL NUMERO DEL DEPOSITO YA SE ENCUENTRA EN LA BASE DE DATOS');
//		document.getElementById('num_deposito').focus();
//		return false;
//		
//	}
}

//add  darwin 11-03-2014, busca en la base de datos si el deposito esta repetido
function validar_deposito_bd(num_deposito,banco,id_tipo_pago,num_lote)
{
	var status;
	var boton_agregar= document.getElementById('boton_agregar');
	boton_agregar.value='';
	boton_agregar.style.background='url(./images/ajax-loader_mini.gif)';
	boton_agregar.style.backgroundRepeat='no-repeat';
	boton_agregar.style.backgroundPosition='center';
	$.ajax({
		 async:false,
		 type: 'POST',
		 dataType:"html",
		 url:"valida_existe_depo.php",
		 data:"banco="+banco+"&num_deposito="+num_deposito+"&id_tipo_pago="+id_tipo_pago+"&num_lote="+num_lote,
		 success:function(result)
		 {
		 	status=result.trim();
    	 }});
boton_agregar.value='Agregar';
boton_agregar.style.background='buttonface';
return status;

}


function AgregaFila(tabla_id){
	var tabla = document.getElementById(tabla_id);
	var row = document.createElement("tr");

	if (tabla_id=='tb_deposito'){
		var c_eliminar= document.createElement("TD");
/*		var newlink = document.createElement('a'); 
		newlink.setAttribute('href', 'javascript:void(0)');
		newlink.setAttribute('onclick', 'alert(this.attributes)');
		tn = document.createTextNode('Eliminar');
		newlink.appendChild(tn);
		c_eliminar.appendChild(newlink);*/		
		var c_num_deposito = document.createElement("TD");
		c_num_deposito.appendChild(document.createTextNode(num_deposito));
		var c_banco_deposito = document.createElement("TD");
		c_banco_deposito.appendChild(document.createTextNode(banco_deposito));
		var c_fecha_deposito = document.createElement("TD");
		c_fecha_deposito.appendChild(document.createTextNode(document.f_deposito.datepicker.value));
		var c_monto_deposito = document.createElement("TD");
		monto_deposito=parseFloat(monto_deposito);
		monto_deposito = monto_deposito.toFixed(2);
		c_monto_deposito.appendChild(document.createTextNode(monto_deposito));										
		var c_cuenta_deposito = document.createElement("TD");
		c_cuenta_deposito.appendChild(document.createTextNode(cuenta_deposito));
		//td2.appendChild (document.createTextNode("columna 2"))
		row.appendChild(c_eliminar);
		row.appendChild(c_num_deposito);
		row.appendChild(c_banco_deposito);
		row.appendChild(c_cuenta_deposito);
		row.appendChild(c_fecha_deposito);
		row.appendChild(c_monto_deposito);
		tabla.appendChild(row);
		transformar(true,'tb_deposito');
	}else if (tabla_id=='tb_pto_venta'){
		var c_eliminar= document.createElement("TD");	
		var c_num_aprob = document.createElement("TD");		
		var c_banco_aprob = document.createElement("TD");
		var c_fecha_aprob = document.createElement("TD");	
		var c_monto_aprob = document.createElement("TD");
		var c_cuentas_ptoventa = document.createElement("TD");
		
		c_num_aprob.appendChild(document.createTextNode(num_aprob_transac));
		c_banco_aprob.appendChild(document.createTextNode(banco_transac));
		c_fecha_aprob.appendChild(document.createTextNode(document.f_deposito.datepicker.value));
		monto_transac=parseFloat(monto_transac);
		monto_transac=monto_transac.toFixed(2);
		c_monto_aprob.appendChild(document.createTextNode(monto_transac));
		c_cuentas_ptoventa.appendChild(document.createTextNode(cuenta_deposito));

		row.appendChild(c_eliminar);
		row.appendChild(c_num_aprob);
		row.appendChild(c_banco_aprob);
		row.appendChild(c_cuentas_ptoventa);
		row.appendChild(c_fecha_aprob);
		row.appendChild(c_monto_aprob );
		tabla.appendChild(row);
		transformar(true,'tb_pto_venta');			
	}else if (tabla_id=='tb_timbre'){
		var c_eliminar= document.createElement("TD");	
		var c_valor_timbre = document.createElement("TD");		
		var c_cantidad_timbre = document.createElement("TD");
		var c_ut = document.createElement("TD");
		var c_monto_timbre = document.createElement("TD");	
		
		c_valor_timbre.appendChild(document.createTextNode(timbre));
		c_cantidad_timbre.appendChild(document.createTextNode(cantidad_timbre));
		c_ut.appendChild(document.createTextNode(ut));
		monto_timbre=parseFloat(monto_timbre);
		monto_timbre=monto_timbre.toFixed(2);
		c_monto_timbre.appendChild(document.createTextNode(monto_timbre));
		row.appendChild(c_eliminar);
		row.appendChild(c_valor_timbre);
		row.appendChild(c_cantidad_timbre);		
		row.appendChild(c_ut);
		row.appendChild(c_monto_timbre);
		tabla.appendChild(row);
		transformar(true,'tb_timbre');			
	}

}
function valida_pto_venta_con_lote(){
	var sel_banco_obj=document.getElementById('sel_banco');
	var indice_sel_banco = sel_banco_obj.selectedIndex;
	var sel_tipo_obj=document.getElementById('sel_tipo');
	var indice_sel_tipo=sel_tipo_obj.selectedIndex;
	banco_id_transac = sel_banco_obj.options[indice_sel_banco].value;
	banco_transac=sel_banco_obj.options[indice_sel_banco].text;
	num_lote_transac=trim(document.getElementById('num_lote_transac').value);
	num_aprob_transac_obj=document.getElementById('num_deposito');
	num_aprob_transac=trim(num_aprob_transac_obj.value);
	var num_aprob_transac_max=num_aprob_transac.length;
	
	num_lote_transac_obj=document.getElementById('num_lote_transac');
	num_lote_transac=trim(num_lote_transac_obj.value);
	monto_transac=trim(document.getElementById('monto_deposito').value);

	

	if (banco_id_transac==0){
		alert('NO SE HA SELECCIONADO UN BANCO EN LA SECCION PUNTO DE VENTA');
		sel_banco_obj.focus();
		return false;
	}else{
		var cuenta_transac_obj = document.getElementById('sel_cuenta');
		var indice_cuenta = cuenta_transac_obj.selectedIndex;
		cuenta_transac = cuenta_transac_obj.options[indice_cuenta].text;
	}	
	
	if(num_lote_transac!='')
	{
		if (!IsNumericInt(num_lote_transac))
		{
			alert('ERROR EN NUMERO DE LOTE NO ES NUMEROo');
			num_lote_transac_obj.focus();
			return false;
		}
		if(num_lote_transac<=0)
		{
			alert('ERROR NUMERO DE LOTE NO PUEDE SER CERO');
			num_lote_transac_obj.focus();
			return false;
		}
	}
	else
	{
		alert('ERROR EN NUMERO DE LOTE VACIO');
		num_lote_transac_obj.focus();
		return false;
	}
	
	if (num_aprob_transac!=''){
		if (IsNumericInt(num_aprob_transac)){
			if (monto_transac!=''){
				if(IsNumeric(monto_transac)) {
					if (monto_transac<=0){
						alert('MONTO NO PUEDE SER CERO');
						return false;
					}else{
						if(num_aprob_transac_max>6)
						{
							alert('ERROR NUMERO DE DEPOSITO NO PUEDE SER MAS DE SEIS CARACTERES');
							num_aprob_transac_obj.focus();
							return false;
						}
						if (pto_venta_repetido_con_lote(num_aprob_transac,banco_transac,num_lote_transac)==false){
							if(validar_deposito_bd(num_aprob_transac,banco_id_transac,indice_sel_tipo,num_lote_transac)=='a9b7ba70783b617e9998dc4dd82eb3c5')
							 {
								alert('ERROR EL NUMERO DE APROBACION YA SE ENCUENTRA EN LA BASE DE DATOS');
								num_aprob_transac_obj.focus();
								return false;
							 }
							 else
							 {
							return true;	 
							 }
						}
					}		
				}else{
					alert('ERROR EN MONTO (PUNTO DE VENTA)');
					document.getElementById('monto_deposito').focus();
					return false;			
				}
			}else{
				alert('ERROR EN MONTO (PUNTO DE VENTA)');
				document.getElementById('monto_deposito').focus();
				return false;		
			}
		}else{
			alert('ERROR EN NUMERO DE APROBACION');
			num_aprob_transac_obj.focus();
			return false;

			
			
		}
	
	}else{
		alert('ERROR EN NUMERO DE APROBACION');
		num_aprob_transac_obj.focus();
		return false;	
	}
}

function AgregaFilaConLote(tabla_id){
	var tabla = document.getElementById(tabla_id);
	var row = document.createElement("tr");


	if (tabla_id=='tb_pto_venta'){
		var c_eliminar = document.createElement("TD");		
		var c_num_aprob = document.createElement("TD");	
		var c_num_lote = document.createElement("TD");	
		var c_banco_aprob = document.createElement("TD");
		var c_cuenta_aprob = document.createElement("TD");
		var c_fecha_aprob = document.createElement("TD");	
		var c_monto_aprob = document.createElement("TD");												
		
		c_cuenta_aprob.appendChild(document.createTextNode(cuenta_transac));		
		c_num_aprob.appendChild(document.createTextNode(num_aprob_transac));
		c_num_lote.appendChild(document.createTextNode(num_lote_transac));
		c_banco_aprob.appendChild(document.createTextNode(banco_transac));
		c_fecha_aprob.appendChild(document.createTextNode(document.f_deposito.datepicker.value));
		monto_transac=parseFloat(monto_transac);
		monto_transac=monto_transac.toFixed(2);
		c_monto_aprob.appendChild(document.createTextNode(monto_transac));

		row.appendChild(c_eliminar);
		row.appendChild(c_num_lote);		
		row.appendChild(c_num_aprob);		
		row.appendChild(c_banco_aprob);
		row.appendChild(c_cuenta_aprob );
		row.appendChild(c_fecha_aprob);
		row.appendChild(c_monto_aprob);
		tabla.appendChild(row);
		transformar(true,'tb_pto_venta');			
	}

}


function transformar(ok,tabla_id) {
	// Obtengo la tabla
	var tabla = document.getElementById(tabla_id);	
	var celdas=null;
/*	var vinculos= tabla.getElementsByTagName('a');

	for (var y=1; i<tabla.rows.length; y++) {
		var celda = tabla.rows[i].cells;		
		celda[0].removeChild(vinculo);
	}*/
	// Guardo los datos de la tabla y marco filas alternas
	for (var i=1; i<tabla.rows.length; i++) {
		celdas = tabla.rows[i].cells;
		
		// La primera vez se llama a la funci�n con ok=true
		// para crear los id y asignar el evento onclick					
		if (ok) {
			
			var id = "TR"+Math.random();
			tabla.rows[i].id = id;

			newlink = document.createElement('a'); 
			//newlink.setAttribute('href', 'javascript:borrarFila(\''+id+'\',100,\''+tabla_id+'\')');
			newlink.setAttribute('href', 'javascript:void(0)');
			newlink.setAttribute('onclick', 'borrarFila(\''+id+'\',100,\''+tabla_id+'\')');
			//newlink.onclick=function() {alert('this.id='+this.id);alert('id='+id);
					//concurrencia=id;
					//borrarFila(this.id, 100, tabla_id);
					//}
			tn = document.createTextNode('Eliminar');
			newlink.appendChild(tn);
			celdas[0].innerHTML = '';
			celdas[0].appendChild(newlink);			
/*			tabla.rows[i].onclick = function() {
					//concurrencia=id;
					borrarFila(this.id, 100, tabla_id);
					}*/
		}
	}
}

function borrarFila(id, porc,tabla_id) {
	var tabla= document.getElementById(tabla_id);
	var filas = tabla.rows.length;

	borrarFila2(id, porc,tabla_id);
}

function borrarFila2(id, porc,tabla_id){
	//alert('id = '+id);
	var obj = document.getElementById(id);
	var tabla= document.getElementById(tabla_id);
	var filas = tabla.rows.length;
	//alert ('obj='+obj );
	// Obtengo el color de fondo
	var estilo = obj.tagName + (obj.className == ""? "":"."+obj.className);
	
	// Si el objeto no tiene color de fondo
	// lo buscamos dentro de los estilos
	// y si no se le asigna el blanco
	if (obj.style.backgroundColor == "") {
		var estilos = document.styleSheets;
		for (var i=0; i<estilos.length; i++) {
			var rules = (estilos[i].cssRules)?estilos[i].cssRules:estilos[i].rules;
		}
		if (obj.style.backgroundColor == "") {
			obj.style.backgroundColor = "#FFFFFF";
		}
	}
	if (obj.style.backgroundColor.charAt(0) == "#") {
		obj.style.backgroundColor = cambiaEstiloHex2Dec(obj.style.backgroundColor);
	}
	
	// Si se est� efectuando el degradado de colores
	if (porc >= 0) {
		// Se calcula el nuevo color, se trata de
		// el incremento o decremento de color
		// del color inicial (o actual) hacia el final
		var colores = obj.style.backgroundColor.match(/rgb\((\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\)/);
		var RGB = "rgb(";
		RGB += (porc==0? Rfinal:parseInt(parseInt(colores[1])-(colores[1]-Rfinal)/(porc/inc)))+",";
		RGB += (porc==0? Gfinal:parseInt(parseInt(colores[2])-(colores[2]-Gfinal)/(porc/inc)))+",";
		RGB += (porc==0? Bfinal:parseInt(parseInt(colores[3])-(colores[3]-Bfinal)/(porc/inc)))+")";
		obj.style.backgroundColor = RGB;
		// Seguimos con el degradado
		setTimeout(function() {borrarFila2(id, porc-inc,tabla_id);}, 30);
	} else {
		// Se ha acabado con el degradado
		// por lo que se borra
		obj.parentNode.removeChild(obj);
		// Se redibujan las filas alternas
		transformar(true,tabla_id);
		filas = tabla.rows.length;
		if (filas==1){
			if (tabla_id=='tb_deposito'){
				Ocultar('t_deposito');
			}else if (tabla_id=='tb_pto_venta'){				
				Ocultar('t_pto_venta');
			}else if (tabla_id=='tb_timbre'){				
				Ocultar('t_timbre');
			}
		}
		totalizar();
		// Se libera el proceso
		//concurrencia = "";
	}
}


function cambiaEstiloHex2Dec(valor) {
	var colores = valor.match(/\#([0-9A-F]{2})([0-9A-F]{2})([0-9A-F]{2})/i);
	return "rgb("+hex2dec(colores[1])+","+hex2dec(colores[2])+","+hex2dec(colores[3])+")";
}
 
// Transforma un numero hexadecimal de dos 
// en uno decimal
function hex2dec(num) {
	var _hex = new Array();
	_hex["0"]=0; _hex["1"]=1; _hex["2"]=2; _hex["3"]=3; 
	_hex["4"]=4; _hex["5"]=5; _hex["6"]=6; _hex["7"]=7; 
	_hex["8"]=8; _hex["9"]=9; _hex["A"]=10; _hex["B"]=11; 
	_hex["C"]=12; _hex["D"]=13; _hex["E"]=14; _hex["F"]=15; 
	
	if (num.match(/([0-9A-F]){2}/i)) {
		return _hex[(num.charAt(0)+"").toUpperCase()]+_hex[(num.charAt(1)+"").toUpperCase()]*16;
	}
	
	return 0;
}
var total_pagar_full;
function totalizar(){
	var tabla_deposito = document.getElementById('t_deposito');
	var tabla_pto_venta=document.getElementById('t_pto_venta');
	var tabla_timbre=document.getElementById('t_timbre');
	var filas_deposito = tabla_deposito.rows.length;
	var filas_pto_venta=tabla_pto_venta.rows.length;
	var filas_timbre = tabla_timbre.rows.length;
	var celdas_deposito =null;
	var celdas_pto_venta=null;
	var celdas_timbre = null;
	var total_pto_venta=0;
	var total_deposito = 0;
	var total_timbre = 0;
	var celda_actual = null;
	var total_span = document.getElementById('total');
	var div_total= document.getElementById('content_total');
	var total =0;
	var total_planilla = parseFloat(document.getElementById('total_planilla').value);
		
if (filas_deposito>1){
		for (var i=1;i<tabla_deposito.rows.length;i++){
			celdas_deposito = tabla_deposito.rows[i].cells;
			celda_actual = celdas_deposito[5].innerHTML;
			total_deposito = parseFloat(total_deposito) + parseFloat(celda_actual);			
		}
	}
	
	
if (filas_pto_venta>1){
		for (var y=1;y<tabla_pto_venta.rows.length;y++){
			celdas_pto_venta = tabla_pto_venta.rows[y].cells;
			t=celdas_pto_venta.length - 1;
			celda_actual = celdas_pto_venta[t].innerHTML;
			total_pto_venta = parseFloat(total_pto_venta) + parseFloat(celda_actual);			
		}
	}
	
	if (filas_timbre>1){
		for (var z=1;z<tabla_timbre.rows.length;z++){
			celdas_timbre = tabla_timbre.rows[z].cells;
			celda_actual = celdas_timbre[4].innerHTML;
			total_timbre = parseFloat(total_timbre) + parseFloat(celda_actual);			
		}
	}

	total=total_deposito + total_pto_venta + total_timbre;
	
	if (total>0){
		div_total.style.border='1px solid grey';
		total_span.innerHTML='<center>TOTAL: ' + total.toFixed(2)+'</center>';
		total_pagar_full=total.toFixed(2);
		if (total.toFixed(2) >= total_planilla){
			Mostrar('miSubmit');
		}else{
			Ocultar('miSubmit');
			//Mostrar('miSubmit');darwin
		}
	}else{
		div_total.style.border='0px';
		total_span.innerHTML='';
		Ocultar('miSubmit');
		//Mostrar('miSubmit');darwin
	}
	
}

function submit_form(){
	var tabla_deposito = document.getElementById('t_deposito');
	var tabla_pto_venta=document.getElementById('t_pto_venta');
	var tabla_timbre =document.getElementById('t_timbre');
	var filas_deposito = tabla_deposito.rows.length;
	var filas_pto_venta=tabla_pto_venta.rows.length;
	var filas_timbre=tabla_timbre.rows.length;
	var celdas_deposito =null;
	var celdas_pto_venta=null;
	var celdas_timbre=null;
	var total_celdas_deposito =null;
	var total_celdas_pto_venta=null;
	var total_celdas_timbre=null;
	var celda_actual = null;		
	var textos = '';
	var input_tabla = document.getElementById('tablas_str');
	var formulario = document.getElementById('f_monto');
	var boton = document.getElementById('miSubmit');
	var st = document.getElementById('security_token').value;
	var total_planilla= document.getElementById('total_planilla').value;
	var total_planilla_cien=parseFloat(total_planilla)+100;
	if((parseFloat(total_pagar_full) >parseFloat(total_planilla_cien) )&&(st=='5618576f143fc779084bc0c5df965779'))
	{
		alert('El TOTAL INGRESADO '+total_pagar_full+' Bs. ES MAYOR AL MONTO TOTAL PERMITIDO, COMUNIQUESE CON SU SUPERIOR INMEDIATO PARA PROCESAR ESTE PAGO');
		return false;
		}
		
	if (filas_deposito>1){
		for (var i=1;i<filas_deposito;i++){
			celdas_deposito = tabla_deposito.rows[i].cells;
			total_celdas_deposito = celdas_deposito.length;
			textos=textos + 'frm_deposito|';
			for (var y=1;y<total_celdas_deposito;y++){
				celda_actual= celdas_deposito[y].innerHTML;
				textos = textos + celda_actual + '|';
			}
		}
	}
	
	if (filas_pto_venta>1){
		for (var i=1;i<filas_pto_venta;i++){
			celdas_pto_venta = tabla_pto_venta.rows[i].cells;
			total_celdas_pto_venta = celdas_pto_venta.length;
			textos=textos + 'frm_pto_venta|';
			for (var y=1;y<total_celdas_pto_venta;y++){
				celda_actual= celdas_pto_venta[y].innerHTML;
				textos = textos + celda_actual + '|';
			}
		}
	}	

	if (filas_timbre>1){
		for (var i=1;i<filas_timbre;i++){
			celdas_timbre = tabla_timbre.rows[i].cells;
			total_celdas_timbre = celdas_timbre.length;
			textos=textos + 'frm_timbre|';
			for (var y=1;y<total_celdas_timbre;y++){
				celda_actual= celdas_timbre[y].innerHTML;
				textos = textos + celda_actual + '|';
			}
		}
	}
	
	input_tabla.value = textos;
	formulario.method='post';
	formulario.action='pago_post.php';
	boton.disabled;
	Ocultar('miSubmit'); 
	document.getElementById('mensaje').innerHTML='ESPERE....  PROCESANDO....';
	formulario.submit();
}

//add 30-01-2014 darwin
function redirec_Add_pagos(){
var tipo_pago= document.getElementById('sel_tipo').value;
var st= document.getElementById('security_token').value;

var exist_num_lote=(st=='4ceb8bac827f1f1bc9aabdedc5d5e238')?'c4ca4238a0b923820dcc509a6f75849b':'cfcd208495d565ef66e7dff9f98764da';

	switch(tipo_pago){
	//deposito
	case "1":
  		agregar_deposito();
  		break;
  //pto_venta
	case "2":
  		agregar_pto_venta(exist_num_lote);
  		break;
	case "3":
  		alert('ESTA OPCION AUN ESTA EN DESARROLLO');
  		break;
	default:
	alert('DEBE SELECCIONAR UN TIPO DE PAGO VALIDO');
	break;
	}
}



function agregar_deposito(){
	var tabla=document.getElementById('t_deposito')
	var filas =tabla.rows.length;
	if (valida_deposito()==true){
		if (filas==1){
			MostrarTabla('t_deposito');
			AgregaFila('tb_deposito');
		}else{
			AgregaFila('tb_deposito');
		}
		totalizar();
	}
}

function agregar_pto_venta(lote){
	/*variable lote retorna 1 si se va a guardadr un pago con punto de venta y num lote, 0 sin num lote*/
	var tabla=document.getElementById('t_pto_venta')
	var filas =tabla.rows.length;
	
	if(lote==='c4ca4238a0b923820dcc509a6f75849b'){
		if (valida_pto_venta_con_lote()==true){			  
			if (filas==1){
				MostrarTabla('t_pto_venta');
				AgregaFilaConLote('tb_pto_venta');
			}else {
				AgregaFilaConLote('tb_pto_venta');
			}
			totalizar();
		}			
	}else if(lote==='cfcd208495d565ef66e7dff9f98764da'){
		if (valida_pto_venta()==true){			  
			if (filas==1){
				MostrarTabla('t_pto_venta');
				AgregaFila('tb_pto_venta');
			}else{
				AgregaFila('tb_pto_venta');
			}
			totalizar();
		}	
	}
	           
}

function agregar_timbre(){
	var tabla=document.getElementById('t_timbre')
	var filas =tabla.rows.length;
	if (valida_timbre()==true){
		if (filas==1){
			MostrarTabla('t_timbre');
			AgregaFila('tb_timbre');
		}else{
			AgregaFila('tb_timbre');
		}
		totalizar();
	}
}

function valida_timbre(){
	var sel_timbre_obj=document.getElementById('sel_timbre');
	var indice_sel_timbre = sel_timbre_obj.selectedIndex;	
	timbre_id = sel_timbre_obj.options[indice_sel_timbre].value;
	timbre=sel_timbre_obj.options[indice_sel_timbre].text;
    cantidad_timbre = document.getElementById('cant_timbre').value;
	monto_timbre=0;

	if (timbre_id==0){
		alert('NO SE HA SELECCIONADO VALOR DEL TIMBRE');
		sel_timbre_obj.focus();
		return false;
	}
	if (cantidad_timbre!=''){
		if (IsNumericInt(cantidad_timbre)){
			if (cantidad_timbre<=0){
				alert('CANTIDAD DE TIMBRES NO PUEDE SER CERO');
				return false;
			}else{
				if (timbre_repetido(timbre)==false){
					monto_timbre=(parseFloat(ut) * parseFloat(timbre_id)) * cantidad_timbre;
					monto_timbre=rndDecimal(monto_timbre,2);
					return true
				}
			}		
		}else{
			alert('ERROR EN CANTIDAD DE TIMBRES');
			document.getElementById('cant_timbre').focus();
			return false;			
		}
	}else{
		alert('ERROR EN CANTIDAD DE TIMBRES');
		document.getElementById('cant_timbre').focus();
		return false;
	}
}
function timbre_repetido(valor_timbre){
	var tabla = document.getElementById('t_timbre');
	var filas = tabla.rows.length;
	if (filas == 1){
		return false;
	}else{
		for (var i=1;i<tabla.rows.length;i++){
			valor_timbre_tabla = tabla.rows[i].cells[1].innerHTML;
			if (valor_timbre_tabla==valor_timbre){
				alert('TIMBRE YA AGREGADO EN LA TABLA');
				return true;			
			}
		}
		return false;
	}
}

///darwin 03/07/2014
addEventListener("load",cargar_eventos,false);
//carga los eventos asociados a los diferentes elementos html
function cargar_eventos()
{
	var sel_tipo= document.getElementById('sel_tipo');
	var boton_agregar= document.getElementById('boton_agregar');
	var sel_banco= document.getElementById('sel_banco');
	var num_deposito= document.getElementById('num_deposito');
	var miSubmit = document.getElementById('miSubmit');
	var text_obser= document.getElementById('text_obser');
	sel_tipo.addEventListener("change",show_lote,false);
	sel_tipo.addEventListener("keyup",show_lote,false);
	boton_agregar.addEventListener("click",redirec_Add_pagos,false);
	sel_banco.addEventListener("change",select_banco,false);
	sel_banco.addEventListener("keyup",select_banco,false);
	num_deposito.addEventListener("focus",max_lenght_depo,false);
	miSubmit.addEventListener("click",submit_form,false);
	text_obser.addEventListener("keyup",cuenta_caracteres,false);
}

//cuenta caracteres textarea
function cuenta_caracteres(e)
{
	var tx_char_rest= document.getElementById('tx_char_rest');
	var ta_char= 2000-parseInt(e.target.value.length);
	tx_char_rest.innerHTML="Caracteres Restantes: "+ta_char;
	
}
//mostrar u oculta el input de lote
 function show_lote()
{
	var st = document.getElementById('security_token').value;
	var sel_tipo=$("#sel_tipo option:selected").val();
	if (st=='4ceb8bac827f1f1bc9aabdedc5d5e238'){
		if (sel_tipo==2)
		{	
			$('#td_lote').css("display","block");
			$('#td_lote_n').css("display","block");
			$('#num_lote_transac').val('');
		}
		else
		{
			$('#td_lote').css("display","none");
			$('#td_lote_n').css("display","none");
		}
	}
}

//dependiento el banco muestra sus numeros de cuenta
function select_banco(e){
	var valor=e.target.value;
	var txtbox= document.getElementById('num_deposito');
	if (valor==0){
		Ocultar('sel_cuenta');
		return false;
	}else{		
	$('#cuentas').html('<center><IMG SRC="images/ajax-loader_mini.gif"></center>'); 
	$('#cuentas').load("form_sel_cuenta.php?banco_id="+valor+"&v=1", {banco_id:valor,v:1},function(responseTxt,statusTxt,xhr)
	{
    	if(statusTxt=="error")
		{
			alert("Error: "+xhr.status+": "+xhr.statusText);
		}
   });			
		return true;
	}	
}

//el input de fecha
$(function() {
   $.datepicker.setDefaults($.datepicker.regional["es"]);
$("#datepicker").datepicker({
	changeMonth: true, //muestra para seleccionar el mes, en el calendario
	changeYear: true,
	dateFormat: 'yy-mm-dd'
});
  });

//determina el numero de caracteres del num deposito dependiendo del banco
function max_lenght_depo()
{	
$('#num_deposito').val('');
if($("#sel_tipo option:selected").val()==2)
{
	 document.getElementById('num_deposito').maxLength=6;
}
else
{
var id_banco = $("#sel_banco option:selected").val();
	switch(id_banco)
	{
		case "1":
  			document.getElementById('num_deposito').maxLength=8; //venezuela
 	 		break;
		case "2":
 			document.getElementById('num_deposito').maxLength=9; // tesoro
  			break;
 		case "3":
  			document.getElementById('num_deposito').maxLength=8; //bicentenario
  			break;
		default:
			//sino conozco q tipo de pago es entonces, no permitir escribir el numero de deposito
			document.getElementById('num_deposito').maxLength=0; 
	}
}
		
}