<?php
include('seguridad_trans.php');
include('conex.php');
include('funciones/funcion.php');?><head>
	<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</head>
<?php

if (!isset($_GET["p"])){
	die();
}else{
	$p=$_GET["p"];
}
if (!isset($_GET["adm"])){ //variable que indica si se busca la informacion de planilla para reimprimir por parte de la seccion de 				
	$adm=0;	 			//reportes para que salga el menu principal pertenecientea reportes
}else{
	$adm=$_GET["adm"];
}

if (!isset($_GET["planilla"])){
	$num_planilla="";
}else{
	$num_planilla=$_GET["planilla"];
}

if (!isset($_GET["tplid"])){
	$tipo_planilla_id="";
}else{
	$tipo_planilla_id=$_GET["tplid"];
	$_SESSION['tplid'] = $_GET["tplid"];
}

if (!isset($_GET["plid"])){
	$planilla_id="";
}else{
	$planilla_id=$_GET["plid"];
}

if (isset($_GET["saved"])){
	$guardo=$_GET["saved"];
}else{
	$guardo=0; /*variable es uno o cero,  es para redirigir el browser a inicio despues de mandar a imprimir*/
}

//la variable $p nos indica si los datos que se van a buscar son para imprimir, o solo para mostrar



$con = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_error()) {
	die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
}
$stmt = $con->stmt_init();
$stmt->prepare('call sel_planilla(?,?)');
$stmt->bind_param('ii',$num_planilla,$tipo_planilla_id);
if(!$stmt->execute()){
	throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
}else{
	$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
	$cuantos_registros = $stmt->num_rows;
	if($cuantos_registros>0){
		$stmt->bind_result($planilla_id_sp,$numeral_sp,$tipo_planilla_sp,$num_planilla_sp,$fecha_sp,$registro_sp,$estado_sp,
						   $nombre_presentante_sp,$nacionalidad_sp,$cedula_presentante_sp,$nombre_empresa_sp,$rif_sp,
						   $tipo_acto_sp,$ut_sp,$monto_sp,$folio_sp,$total_sp,$creadopor_sp,$fecha_creado_sp,$ip_sp,
						   $obsrv_sp,$porcentaje_sp,$capital_sp,$tasa_sp,$name_accionista_concat_sp,$ced_accionista_sp,$name_accionista_sp,$direccion_local_sp);
		while($stmt->fetch()){	
			$planilla_id			=	$planilla_id_sp;
			$numeral				=	$numeral_sp;
			$tipo_planilla			=	$tipo_planilla_sp;
			$num_planilla			=	$num_planilla_sp;
			$fecha					=	$fecha_sp;
			$registro				=	$registro_sp;
			$estado					=	ConHtml($estado_sp);
			$nombre_presentante		=	$nombre_presentante_sp;
			$nacionalidad			=	$nacionalidad_sp;
			$cedula_presentante		=	$cedula_presentante_sp;
			$nombre_empresa			=	$nombre_empresa_sp;
			$rif					=	$rif_sp;
			$tipo_acto				=	$tipo_acto_sp;
			$ut						=	$ut_sp;
			$monto					=	$monto_sp;
			$folio					=	$folio_sp;
			$total					=	$total_sp;
			$creadopor				=	$creadopor_sp;
			$fecha_creado			=	$fecha_creado_sp;
			$ip						=	$ip_sp;
			$obsrv					=	$obsrv_sp;
			$porcentaje				=	$porcentaje_sp;
			$capital				=	$capital_sp;
			$tasa					=	$tasa_sp;
			$name_accionista		=	$name_accionista_sp;
			$ced_accionista			=	$ced_accionista_sp;
			$direccion_local		=	$direccion_local_sp;
			$name_accionista_ced	=	$ced_accionista_sp.', '.$name_accionista_sp;
			$name_accionista_concat	=	$name_accionista_concat_sp;
		}                      	
	}else{
    	?><link href="styles/contenido.css" rel="stylesheet" type="text/css" />
		<div class="contenedor" id="resp_serv">
			<br  /><br  /><br  />
			<table width="58%" border="1" cellspacing="0" cellpadding="0"  align="center">
				<tr>
					<td bgcolor="#FF0000">NUMERO DE PLANILLA NO ENCONTRADA</td>
				</tr>
			</table>
		</div><?php
		die();
	}
}
$stmt->free_result();
$stmt->close();
while($con->next_result()) { }
$con->close();
		
if ($p==1){/* SI $P = 1 IMPRIME, 0 NO IMPRIME SOLO MUESTRA PARA PAGAR, 2 MUESTRA SOLO PARA REIMPRIMIR   */ 
	$browser = getBrowser();
/*	if (substr($browser,0,17) <> 'Internet Explorer') {
		die('IMPRESION SOLO DISPONIBLE PARA NAVEGADOR: INTERNET EXPLORER 7, 8');
	}else{*/
		$fecha 					= 	urlencode(encrypt($fecha,'123'));
		$registro				=	urlencode(encrypt($registro,'123'));
		$estado					=	urlencode(encrypt($estado,'123'));
		$nombre_presentante		=	urlencode(encrypt($nombre_presentante,'123'));
		$nacionalidad			=	urlencode(encrypt($nacionalidad,'123'));		
		$cedula_presentante		=	urlencode(encrypt($cedula_presentante,'123'));
		$nombre_empresa			=	urlencode(encrypt($nombre_empresa,'123'));
		$rif					=	urlencode(encrypt($rif,'123'));
		$tipo_acto				=	urlencode(encrypt($tipo_acto,'123'));
		$monto					=	urlencode(encrypt(bsf($monto),'123'));
		$porcentaje				=	urlencode(encrypt($porcentaje,'123'));
		$tasa 					= 	urlencode(encrypt(bsf($tasa),'123'));
		$total					=	urlencode(encrypt(bsf($total),'123'));
		$capital				=	urlencode(encrypt(bsf($capital),'123'));
		$usuario				=	urlencode(encrypt($creadopor,'123'));
		$ut						=	urlencode(encrypt($ut,'123'));
		$name_accionista_ced	=	urlencode(encrypt($name_accionista_concat,'123'));
	/*  $cedula_presentante		=	encrypt(urlencode($cedula_presentante),'intentalo');
		$nombre_empresa			=	encrypt(urlencode($nombre_empresa),'intentalo');
		$rif					=	encrypt(urlencode($rif),'intentalo');
		$tipo_acto				=	encrypt(urlencode($tipo_acto),'intentalo');
		$porcentaje				=	encrypt(urlencode($porcentaje),'intentalo');
		$estado					=	encrypt(urlencode($estado),'intentalo');
		$nombre_presentante		=	encrypt(urlencode($nombre_presentante),'intentalo');*/
		
		if ($tipo_planilla=='FDC'){
			$destino='imprime_fdc.php';
		}elseif ($tipo_planilla=='FM'){
			$destino='imprime.php';
		}
				
		
		?><script type="text/javascript">window.open("<?php echo $destino ?>?af=<?php echo $fecha ?>&ra=<?php echo $registro ?>&es=<?php echo $estado ?>&no=<?php echo $nombre_presentante ?>&na=<?php echo $nacionalidad ?>&cpre=<?php echo $cedula_presentante ?>&noe=<?php echo $nombre_empresa ?>&lo=<?php echo $rif ?>&tas=<?php echo $tipo_acto ?>&om=<?php echo $monto ?>&pte=<?php echo $porcentaje ?>&ass=<?php echo $tasa ?>&tt=<?php echo $total ?>&us=<?php echo $usuario ?>&tut=<?php echo $ut ?>&pit=<?php echo $capital ?>&na=<?php echo $name_accionista_ced ?>","Imprime","location=0,toolbar=no,titlebar=no,menubar=no,status=no,resizable=yes");<?php
		if ($guardo==1){
			?>window.location="body.php";<?php	
		}else{
			?>history.go(-1);<?php
		}
/*	}*/
	?></script><?php	
}elseif ($p==0 || $p==2){
	if ($numeral==1){
		$pagina='lit_1.php?';
	}elseif ($numeral==2){
		$pagina='lit_2.php?';	
	}elseif ($numeral==3){
		$pagina='lit_3.php?';
	}elseif ($numeral==4){
		$pagina='lit_4.php?';
	}elseif ($numeral==5){
		$pagina='lit_5.php?';
	}elseif ($numeral=='7a'){
		//$pagina='lit_7.php?tipo_persona=2&';
		$pagina='lit_7a.php?';
	}elseif ($numeral=='7b'){
		//$pagina='lit_7.php?tipo_persona=1&';
		$pagina='lit_7b.php?';
	}elseif ($numeral==6){
		$pagina='lit_6.php?';
	}elseif ($numeral==9){
		$pagina='lit_9.php?';
	}
	if ($p==0){ /*REDIRECCIONAMOS A LA PAGINA DE PAGAR*/
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call sel_planilla2(?,?)'); // comprobamos que no se haya pagado la planilla, devuleve 1 si consigue el pago
		$stmt->bind_param('ii',$num_planilla,$tipo_planilla_id);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
			$cuantos_registros = $stmt->num_rows;
			if($cuantos_registros>0){
				$stmt->bind_result($resp);
				while($stmt->fetch()){	
					if ($resp==1){ /*resp es 1 si existe pago registrado en la planilla*/
						?><link href="styles/contenido.css" rel="stylesheet" type="text/css" />
						 <div class="contenedor" id="resp_serv">
							<br  /><br  /><br  />
							<table width="58%" border="1" cellspacing="0" cellpadding="0"  align="center">
								<tr>
									<td bgcolor="#FF0000">LA PLANILLA YA FUE PAGADA</td>
								</tr>
							</table>
						</div><?php
						$stmt->free_result();
						$stmt->close();
						while($con->next_result()) { }
						$con->close();						
						die();
					}
				}                      	
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
		$con->close();		
	}		
	
	?><script type="text/javascript">window.location="<?php echo $pagina ?>adm=<?php echo $adm ?>&plid=<?php echo $planilla_id ?>&i_folios=<?php echo addslashes($folio) ?>&p=<?php echo $p ?>&fecha=<?php echo $fecha ?>&registro=<?php echo $registro ?>&estado=<?php echo $estado ?>&nombre_presentante=<?php echo urlencode($nombre_presentante) ?>&nacionalidad=<?php echo $nacionalidad ?>&ced_present=<?php echo urlencode($cedula_presentante) ?>&nombre_empresa=<?php echo  urlencode($nombre_empresa) ?>&rif=<?php echo urlencode($rif) ?>&tipo_acto=<?php echo urlencode($tipo_acto) ?>&monto=<?php echo bsf($monto) ?>&porcentaje=<?php echo urlencode($porcentaje) ?>&tasa=<?php echo bsf($tasa) ?>&total=<?php echo bsf($total) ?>&i_monto=<?php echo $capital ?>&ut=<?php echo urlencode($ut) ?>&tipo_planilla=<?php echo $tipo_planilla_id ?>&planilla=<?php echo $num_planilla ?>&fech=<?php echo $fecha_creado ?>&direc_local=<?php echo urlencode($direccion_local) ?>&name_acci=<?php echo urlencode($name_accionista) ?>&ci_accionista=<?php echo urlencode($ced_accionista) ?>&lit=<?php echo $numeral ?>&reg_id=";</script><?php
}
?>