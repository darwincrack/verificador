<?php include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');
include('../funciones/mysql.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Pago de Planillas</title>
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript" src="../funciones/calendarDateInput.js"></script>
<script type="text/javascript">
function ColorUno(src,color_entrada) {
	src.style.backgroundColor=color_entrada;
} 
function ColorDos(src,color_default) { 
    src.style.backgroundColor=color_default;
	//src.style.cursor="default"; 
}
</script>
</head>
<body onload="loadurl('menu.php','menu')">
<span id="menu"></span>
<div class="contenedor">
	<h1>Tipo de Acto</h1>
    <h3>Para listados de un solo dia, colocar la misma fecha en ambos cuadros.</h3>
    <form  action="" method="get" id="consulta" name="f_providencia">
        <table width="71%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas3" >
          <tr>
            <td align="center"><a>Fecha Desde:</a></td>
            <td  ><script>DateInput('fechadesde',true)</script></td>
            <td  align="center"><a>Fecha Hasta:</a></td>
            <td ><script>DateInput('fechahasta',true)</script></td>
          </tr><?php
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechadesde"],8,2);
                $mes=substr($_GET["fechadesde"],5,2)-1;
                $a�o=substr($_GET["fechadesde"],0,4);
                echo "<script>fechadesde_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechahasta"],8,2);
                $mes=substr($_GET["fechahasta"],5,2)-1;
                $a�o=substr($_GET["fechahasta"],0,4);
                echo "<script>fechahasta_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }?>
          <tr>
            <td colspan="4" align="center"><input name="buscar" type="submit" value="Buscar" class="boton_grande" /></td>
          </tr>
        </table>
    </form>
    <br  />
    <?php
	if (isset($_GET["fechadesde"]) && isset($_GET["fechahasta"])){
		$fecha_desde = $_GET["fechadesde"];
		$fecha_hasta = $_GET["fechahasta"];

		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ')'. mysqli_connect_error());
		}	
		$stmt = $con->stmt_init();
		$stmt->prepare('call rpt_tipo_acto_por_fecha(?,?)');
		$stmt->bind_param('ss',$fecha_desde,$fecha_hasta);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
			$cuantos_registros = $stmt->num_rows;			
			if($cuantos_registros>0){				
				$rs=fetch($stmt);		
				$rs_total=count($rs);
				$export="N�\tREGISTRO\tTIPO DE ACTO\tCANTIDAD\n\n";
				$tabla='<table width="66%" border="0" align="center" cellpadding="1" cellspacing="1" class="reporte"  >';					
                $encabezado='<thead>
                     <tr>
					   <th></th>	
                       <th width="29%">REGISTRO</th>
                       <th width="53%">TIPO DE ACTO</th>
                       <th width="18%">CANTIDAD</th>
                     </tr>
                    </thead>';
				$cuerpo='<tbody>';
				$filas='';
				$total=0;
				for($i=0;$i<$rs_total;$i++){
					$total=$total+trim($rs[$i]['total_planillas']);
					$filas.='<tr>
								<td>'.($i + 1).'</td>
								<td>'.trim($rs[$i]['nombre_registro']).'</td>
			 					<td>'.trim($rs[$i]['descripcion']).'</td>
			 					<td>'.trim($rs[$i]['total_planillas']).'</td>
			 				</tr>';
					$export.=($i + 1)."\t".ConHtml(trim($rs[$i]['nombre_registro']))."\t".ConHtml(trim($rs[$i]['descripcion']))."\t".ConHtml(trim($rs[$i]['total_planillas']))."\n";
			 	}                    
			 	$cuerpo.=$filas.'</tbody>';
			 	$pie_tabla='<tfoot>
                     <tr>
                       <td colspan="3">TOTAL</td>           
                       <td>'.$total.'</td>
                     </tr>
                    </tfoot>';
				$export.=" \t \t \t".$total."\n";
				$tabla.=$encabezado.$cuerpo.$pie_tabla.'</table>';
				echo $tabla;
				?><br />
				<form  action="export.php" method = "POST" name"f_export" >
                    <input type="hidden" name="export" value="<?php echo $export ?>"/>
                    <input type="hidden" name="titulo" value="Tipos_De_Acto"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type = "submit" value = "Exportar a Excel" class="boton_grandeExport"/>
  				</form><br />
				<?php
								
			}else{
			/*no hay registros para mostrar*/
			echo '<table width="420" border="1" align="center" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                  </table>';
			}			
		}
		$stmt->free_result();
		$stmt->close();
		$con->close();
	}?>
                    
</div>

</body>
</html>