<?php
ob_start();
include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');

if (isset($_GET["fechadesde"]) && isset($_GET["fechahasta"])){
	$desde =$_GET["fechadesde"];
	$hasta=$_GET["fechahasta"];
}else{
	$desde ="";
	$hasta="";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Pago de Planillas</title>
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript" src="../funciones/calendarDateInput.js"></script>
<script type="text/javascript">
function ColorUno(src,color_entrada) {
	src.style.backgroundColor=color_entrada;
} 
function ColorDos(src,color_default) { 
    src.style.backgroundColor=color_default;
	//src.style.cursor="default"; 
}
function enviar_form(){
	cadena=document.getElementById('exp_t').innerHTML;
	document.env_tabla.tabla_html.value = cadena;
	}
</script>
</head>
<body onload="loadurl('menu.php','menu')">
<span id="menu"></span>
<div class="contenedor">
<h1>Pago de Planillas</h1>
    <h3>Para listados de un solo dia, colocar la misma fecha en ambos cuadros.</h3>
    <form  action="" method="get" id="consulta" name="f_providencia">
        <table width="71%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas3" >
          <tr>
            <td align="center"><a>Fecha Desde:</a></td>
            <td  ><script>DateInput('fechadesde',true)</script></td>
            <td  align="center"><a>Fecha Hasta:</a></td>
            <td ><script>DateInput('fechahasta',true)</script></td>
          </tr><?php
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechadesde"],8,2);
                $mes=substr($_GET["fechadesde"],5,2)-1;
                $a�o=substr($_GET["fechadesde"],0,4);
                echo "<script>fechadesde_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechahasta"],8,2);
                $mes=substr($_GET["fechahasta"],5,2)-1;
                $a�o=substr($_GET["fechahasta"],0,4);
                echo "<script>fechahasta_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }?>
          <tr>
            <td colspan="4" align="center"><input name="buscar" type="submit" value="Buscar" class="boton_grande" /></td>
          </tr>
        </table>
    </form><br  />
	</div>
	<?php
	if (isset($_GET["fechadesde"]) || isset($_GET["fechahasta"])){
		$fecha_desde = $_GET["fechadesde"];
		$fecha_hasta = $_GET["fechahasta"];
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call rpt_detal_registros(?,?)');
		$stmt->bind_param('ss',$fecha_desde,$fecha_hasta);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result();
			$cuantos_registros = $stmt->num_rows;
			$excel='';
			if($cuantos_registros>0){
				$stmt->bind_result($tipo,$tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$total_planilla,$registro_id,$registro_nomb,$deposito_id,$num_deposito,$banco,$deposito_monto,
								   $num_aprob,$pto_venta_monto,$timbre_monto,$creadopor,$fecha_creado,$ip);
																								
				?><div class="contenedor2"  id="exp_t">                
                  <table id="t_deposito"  border="1" cellpadding="0" cellspacing="0">
                    <tbody id="tb_deposito">
                        <tr bgcolor="#9999FF">
                            <td width="4%"></td>
                            <td >Tipo de Info</td>
                            <td>Registro</td>
                            <td >Num. Planilla</td>
                            <td >Total en Planilla</td>
                            <td >Num. Deposito</td>
                            <td >Banco</td>
                            <td >Monto Deposito</td>
                            <td >Num. Aprob</td>
                            <td >Monto de Transaccion</td>
                            <td >Monto por Timbre</td>
                            <td >Operacion Registrada Por</td>
                            <td >Fecha Operacion</td>
                            <td >IP</td>                                                                                    
                        </tr><?php
						//$excel = "TIPO DE INFO\tNUM. DE PLANILLA\tTOTAL EN PLANILLA\tNUM. DE DEPOSITO\tBANCO\tMONTO DEL DEPOSITO\tAPROBACION DE TRANSACCION\tMONTO DE TRANSACCION\tMONTO TIMBRE\tREGISTRADO POR\tFECHA DE OPERACION\tIP\n\n";
				$cont=0;
				$cco=0;
				$color1='#FFFFFF';
				$color2='#E6F2E1';
				$color_temp='';
				$num_planilla_ultimo='';
				while($stmt->fetch()){
					$cco++; 
					$cont++;
/*					if ($cont==1){
						$num_planilla_ultimo=$num_planilla;	
					}
					
					if ($num_planilla_ultimo==$num_planilla){
						$color=$color1;
					}else{
						$color=$color2;
						$color_temp=$color1;
						$color1=$color2;
						$color2=$color_temp;
						$num_planilla_ultimo==$num_planilla;
					}*/
					
/*					if(($cco%2)==0){ 
						$color = '#FFFFFF'; 
					}else{
						$color = '#E6F2E1'; 
					}*/$color = '#FFFFFF'; 
					?><tr onmouseover="ColorUno(this,'#CCCCCC');" onmouseout="ColorDos(this,'<?php echo $color; ?>')" bgcolor="<?php echo $color; ?>"><td><?php echo $cont ?></td><td><?php echo $tipo ?></td><td><?php echo $registro_nomb ?></td><td ><a href="../redirect2.php?p=2&planilla=<?php echo $num_planilla ?>&tplid=<?php echo $tipo_planilla_id ?>&adm=1"><?php echo $tipo_planilla_desc.'-'.$num_planilla ?></a></td><td ><?php echo bsf($total_planilla) ?></td><td><?php echo $num_deposito ?></td><td ><?php echo $banco ?></td> <td ><?php echo bsf($deposito_monto) ?></td><td ><?php echo $num_aprob ?></td><td ><?php echo bsf($pto_venta_monto) ?></td><td ><?php echo bsf($timbre_monto) ?></td><td ><?php echo $creadopor ?></td><td ><?php echo $fecha_creado ?></td><td ><?php echo $ip ?></td></tr><?php										
				}?></tbody>
                </table>
            </div>
            <div class="contenedor2">
			<br />
            <table>
            	<tr>
                	<td>            
                        <form action="rptdetalplanilla_export.php" method = "POST" name"f_export" >
                            <input type="hidden" name="desde" value="<?php echo $desde ?>" />
                            <input type="hidden" name="hasta" value="<?php echo $hasta ?>" />
                            <input type = "submit" value = "Exportar a Excel" />
                        </form>
                    </td>
                    <td>                        
                    <td>
                        <form action="export_table.php"  method="post" id="env_tabla" name="env_tabla" onsubmit="return enviar_form()">
                        	<input type="hidden" name="tabla_html" id="tabla_html" value="">
                            <input type="hidden" name="f_nombre" value="Detalles_Registros" />
                            <input type = "submit" value = "Exportar a Excel # 2"  />
                        </form>
                    </td>
                </tr>
            </table>                                     
				</div><?php
			}else{
				?><div class="contenedor" align="center">
                <table width="420" border="1" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                </table>
				</div><?php
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
	}?>

</body>
</html>