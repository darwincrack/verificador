<?php include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php')?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Pago de Planillas</title>
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript" src="../funciones/calendarDateInput.js"></script>
<script type="text/javascript">
function ColorUno(src,color_entrada) {
	src.style.backgroundColor=color_entrada;
} 
function ColorDos(src,color_default) { 
    src.style.backgroundColor=color_default;
	//src.style.cursor="default"; 
}
</script>
</head>
<body onload="loadurl('menu.php','menu')">
<span id="menu"></span>
<div class="contenedor">
<h1>Depositos Cargados en el Sistema</h1>
    <h3>Para listados de un solo dia, colocar la misma fecha en ambos cuadros.</h3>
    <form  action="" method="get" id="consulta" name="f_providencia">
        <table width="71%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas3" >
          <tr>
            <td align="center"><a>Fecha Desde:</a></td>
            <td  ><script>DateInput('fechadesde',true)</script></td>
            <td  align="center"><a>Fecha Hasta:</a></td>
            <td ><script>DateInput('fechahasta',true)</script></td>
          </tr><?php
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechadesde"],8,2);
                $mes=substr($_GET["fechadesde"],5,2)-1;
                $a�o=substr($_GET["fechadesde"],0,4);
                echo "<script>fechadesde_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechahasta"],8,2);
                $mes=substr($_GET["fechahasta"],5,2)-1;
                $a�o=substr($_GET["fechahasta"],0,4);
                echo "<script>fechahasta_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }?>
          <tr>
            <td colspan="4" align="center"><input name="buscar" type="submit" value="Buscar" class="boton_grande" /></td>
          </tr>
        </table>
    </form><br  />
	</div>
	<?php
	if (isset($_GET["fechadesde"]) || isset($_GET["fechahasta"])){
		$fecha_desde = $_GET["fechadesde"];
		$fecha_hasta = $_GET["fechahasta"];
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call rpt_depositos(?,?)');
		$stmt->bind_param('ss',$fecha_desde,$fecha_hasta);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result();
			$cuantos_registros = $stmt->num_rows;
			if($cuantos_registros>0){
				$stmt->bind_result($tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$nomb_presentante,$ced_presentante,$nomb_empresa,$rif,$num_deposito,$banco, $deposito_fecha,$num_cuenta,$deposito_monto,$creadopor,$fecha_creado,$ip,$name_accionista,$direccion_local);																								
				?><div class="contenedor2" align="left">                
                  <table id="t_deposito"  border="1" cellpadding="0" cellspacing="0" align="left">
                    <tbody id="tb_deposito">
                        <tr bgcolor="#9999FF">
                            <td width="4%"></td>
                            <td >Num. Planilla</td>
                            <td >Nombre Presentante</td>
                            <td >Cedula Presentante</td>
                            <td >Nombre Empresa</td>                            
                            <td >R.I.F</td>
                            <td>Direccion Local</td>
                            <td>Accionista</td>
                            <td >Num. Deposito</td>
                            <td >Banco</td>
                            <td>Fecha de Deposito</td>
                            <td>Num. Cuenta</td>
                            <td >Monto Deposito</td>
                            <td >Operacion Registrada Por</td>
                            <td >Fecha Operacion</td>
                            <td >IP</td>                                                                                    
                        </tr><?php
						$excel = "NUM. DE PLANILLA\tNOMBRE DEL PRESENTANTE\tCED. DEL PRESENTANTE\tNOMBRE DE LA EMPRESA\tR.I.F\tDIRECCION LOCAL\tACCIONISTA\tNUM. DE DEPOSITO\tBANCO\tFECHA DEL DEPOSITO\tNUM. DE CUENTA\tMONTO DEL DEPOSITO\tREGISTRADO POR\tFECHA DE OPERACION\tIP\n\n";
				$cont=0;
				$cco=0;
				while($stmt->fetch()){
					$cco++; 
					$cont++;

					if(($cco%2)==0){ 
						$color = '#FFFFFF'; 
					}else{
						$color = '#E6F2E1'; 
					} ?>
					<tr onmouseover="ColorUno(this,'#CCCCCC');" onmouseout="ColorDos(this,'<?php echo $color; ?>')" bgcolor="<?php echo $color; ?>">
                    		<td><?php echo $cont ?></td>
                            <td ><a href="../redirect2.php?p=2&planilla=<?php echo $num_planilla ?>&tplid=<?php echo $tipo_planilla_id ?>&adm=1"><?php echo $tipo_planilla_desc.'-'.$num_planilla ?></a></td>
                            <td ><?php echo $nomb_presentante ?></td>
                           <!-- <td ><a href="../redirect2.php?p=2&planilla=<?php //echo $deposito_id ?>&adm=1"><?php //echo $num_deposito ?></a></td>-->                            
                            <td ><?php echo $ced_presentante ?></td>
                            <td ><?php echo $nomb_empresa ?></td>
                            <td ><?php echo $rif ?></td>
                            <td ><?php echo $direccion_local ?></td>
                            <td><?php echo $name_accionista ?></td>                      
                            <td><?php echo $num_deposito ?></td>                       
                            <td ><?php echo $banco ?></td>
                            <td ><?php echo $deposito_fecha ?></td>
                            <td ><?php echo $num_cuenta ?></td>                            
                            <td ><?php echo $deposito_monto ?></td>
                            <td ><?php echo $creadopor ?></td>
                            <td ><?php echo $fecha_creado ?></td>
                            <td ><?php echo $ip ?></td>                             
					</tr><?php
						$excel .="$tipo_planilla_desc-$num_planilla\t".trim(ConHtml($nomb_presentante))."\t".trim(ConHtml($ced_presentante))."\t".trim(ConHtml($nomb_empresa))."\t".trim(ConHtml($rif))."\t".trim(ConHtml($direccion_local))."\t".trim(ConHtml($name_accionista))."\t$num_deposito\t".trim(ConHtml($banco))."\t$deposito_fecha\t$num_cuenta\t".trim(bsf($deposito_monto))."\t$creadopor\t$fecha_creado\t$ip\n";					
				}?></tbody>
                </table><br /><br />
          <form action="export.php" method = "POST" name"f_export" >
                    <input type="hidden" name="export" value="<?php echo $excel ?>"/>
                    <input type="hidden" name="titulo" value="Depositos_Cargados"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type = "submit" value = "Exportar a Excel" class="boton_grandeExport"/>
            </form>
				</div><?php
			}else{
				?><div class="contenedor" align="center">
                <table width="420" border="1" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                </table>
				</div><?php
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
		$con->close();
	}?>

</body>
</html>