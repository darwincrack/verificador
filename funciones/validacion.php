<?php
	function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	function validateName($name){
		//if it's NOT valid
		if(strlen($name) < 4)
			return false;
		//if it's valid
		else
			return true;
	}
	function validateEmail($email){
		return ereg("^[a-zA-Z0-9]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$", $email);
	}
	/*function validatePasswords($pass1, $pass2) {
		//if DOESN'T MATCH
		if(strpos($pass1, ' ') !== false)
			return false;
		//if are valid
		return $pass1 == $pass2 &amp;amp;&amp;amp; strlen($pass1) > 5;
	}*/
	function validateMessage($message){
		//if it's NOT valid
		if(strlen($message) < 10)
			return false;
		//if it's valid
		else
			return true;
	}

function ValidarDatos($campo){
	//Array con las posibles cadenas a utilizar por un hacker
	$CadenasProhibidas = array("Content-Type:",
	"MIME-Version:", //evita email injection
	"Content-Transfer-Encoding:",
	"Return-path:",
	"Subject:",
	"From:",
	"Envelope-to:",
	"To:",
	"bcc:",
	"cc:",
	"UNION", // evita sql injection
	"DELETE",
	"DROP",
	"SELECT",
	"INSERT",
	"UPDATE",
	"CRERATE",
	"TRUNCATE",
	"ALTER",
	"INTO",
	"DISTINCT",
	"GROUP BY",
	"WHERE",
	"RENAME",
	"DEFINE",
	"UNDEFINE",
	"PROMPT",
	"ACCEPT",
	"VIEW",
	"COUNT",
	"HAVING",
	"'",
	'"',
	"{",
	"}",
	"[",
	"]",
	"HOTMAIL", // evita introducir direcciones web
	"WWW",
	".COM",
	"@",
	"W W W",
	". c o m",
	"http://",
	"$", //variables y comodines
	"&",
	"*",
	"<",
	">"
	);
	//Comprobamos que entre los datos no se encuentre alguna de 
	//las cadenas del array. Si se encuentra alguna cadena se 
	//dirige a una p�gina de Forbidden 
	
	foreach($CadenasProhibidas as $valor){ 
		if(strpos(strtolower($campo), strtolower($valor)) !== false){
			return '';
			exit; 		
	/*		echo("<script>
			alert('No puede introducir direcciones web, comillas, corchetes, c�digo de programaci�n o cualquier dato no relativo a los campos del formulario');
			history.back();</script>");*/		
		}else{
			return $campo;
		} 
	} 
}

function remove_HTML($s , $keep = ' ' , $expand = 'script|style|noframes|select|option'){
	/**///prep the string
	$s = ' ' . $s;
	
	/**///initialize keep tag logic
	if(strlen($keep) > 0){
		$k = explode('|',$keep);
		for($i=0;$i<count($k);$i++){
			$s = str_replace('<' . $k[$i],'[{(' . $k[$i],$s);
			$s = str_replace('</' . $k[$i],'[{(/' . $k[$i],$s);
		}
	}
	
	//begin removal
	/**///remove comment blocks
	while(stripos($s,'<!--') > 0){
		$pos[1] = stripos($s,'<!--');
		$pos[2] = stripos($s,'-->', $pos[1]);
		$len[1] = $pos[2] - $pos[1] + 3;
		$x = substr($s,$pos[1],$len[1]);
		$s = str_replace($x,'',$s);
	}
	
	/**///remove tags with content between them
	if(strlen($expand) > 0){
		$e = explode('|',$expand);
		for($i=0;$i<count($e);$i++){
			while(stripos($s,'<' . $e[$i]) > 0){
				$len[1] = strlen('<' . $e[$i]);
				$pos[1] = stripos($s,'<' . $e[$i]);
				$pos[2] = stripos($s,$e[$i] . '>', $pos[1] + $len[1]);
				$len[2] = $pos[2] - $pos[1] + $len[1];
				$x = substr($s,$pos[1],$len[2]);
				$s = str_replace($x,'',$s);
			}
		}
	}
	
	/**///remove remaining tags
	while(stripos($s,'<') > 0){
		$pos[1] = stripos($s,'<');
		$pos[2] = stripos($s,'>', $pos[1]);
		$len[1] = $pos[2] - $pos[1] + 1;
		$x = substr($s,$pos[1],$len[1]);
		$s = str_replace($x,'',$s);
	}
	
	/**///finalize keep tag
	for($i=0;$i<count($k);$i++){
		$s = str_replace('[{(' . $k[$i],'<' . $k[$i],$s);
		$s = str_replace('[{(/' . $k[$i],'</' . $k[$i],$s);
	}
	
	return trim($s);
}


?>