<?php
include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');
 $fechadesde='';
 $fechahasta='';  
?>
<!doctype html>
<html>
<head>
<meta charset="iso-8859-1">
<title>Planillas Actualizadas</title>
<link rel="stylesheet" href="../styles/contenido.css" type="text/css"/>
<link rel="stylesheet" href="../styles/jquery-ui.css"/>

</head>

<body onload="loadurl('menu.php','menu')">
	<span id="menu"></span>
    
	<div class="agrupar" align="right" style="font-family: verdana; padding:10px;">
<br />

	  <table class ="table_radius" width="55%" align="center" style="width: 60%">
       <form method="post">
			<tr>
			  <th colspan="5">Fecha de Modificaci&oacute;n del Deposito</th></tr>
			<tr>
            <td width="25%" align="center">Fecha Desde:</td>
            <td width="28%" align="center"><input align="middle" type="text" id="fechadesde" 
			<?php if (isset($_POST['fechadesde'])) $fechadesde=$_POST['fechadesde'];
echo"value='$fechadesde'" ?> name="fechadesde" readonly style="cursor:pointer; width:100px;"></td>
            <td width="25%" align="center">Fecha Hasta:</td>
            <td width="28%" align="center"><input  align="middle" type="text" id="fechahasta"  
			<?php if (isset($_POST['fechahasta'])) $fechahasta=$_POST['fechahasta'];
echo"value='$fechahasta'" ?> name="fechahasta" readonly required style="cursor:pointer; width:100px;"></td>


<td><div align="center"> <input  type="submit" value="Enviar" style="
    border-radius: 5px;
    width: 80px;
    font-weight: bold;
    cursor: pointer;"/></div></td> </tr>
  </form>
		</table>
    
        
		</div>
        <br />
        <br />
<?php

if (isset($_POST['fechadesde']) && isset($_POST['fechahasta']) && ! empty($_POST['fechadesde']) && ! empty($_POST['fechahasta']) )
{
	
	 $fechadesde = texto_limpio($_POST['fechadesde']);
	 $fechahasta = texto_limpio($_POST['fechahasta']);	

$mysqli = new mysqli($host,$user,$clave,$db,$puerto);

	if (mysqli_connect_errno())
	{
		printf('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. %s\n', mysqli_connect_error());
   		exit();
	}	

	$result = $mysqli->query("CALL sel_deposito_modif('$fechadesde','$fechahasta')");
	if(!$result) die("CALL failed: (" . $mysqli->errno . ") " . $mysqli->error);	

		if($result->num_rows > 0) 
		{ 
		  echo"<div class='contenedor2'  id='content_info_depo' style='width:90%'> 
		  <table class='table' id='table_radius'>
                   		<thead>
                        <tr style='font-size:10px;'>
                            <th width='4%' style='border-top-left-radius:10px;'>N&deg;</th>
                            
							<th width='6%'>Planilla</a></th>
                            <th>Banco</th>
							<th># de cuenta
                            <th># de deposito</th>
                            <th>Fecha del deposito</th>
                            <th>Monto</th>
                            <th>Tipo de pago</th>
                            <th>Observaciones</th>
							<th>Creador por</th>
                            <th>Fecha de creacion</th>
							<th style='border-top-right-radius:10px;'>Fecha de ultima modificacion</th>
                            </tr></thead>";
		}
		else {
			echo"<div class='agrupar' style='font-family: verdana; padding:10px; width:75%' align='center'>
			<div class='contenedor' style='width:50%'>
                <table width='420' class='table'>
                  	<tr><td width='396' align='center' style='border-top-left-radius:10px; color:#000; border-top-right-radius:10px;'>  NO SE ENCONTRARON REGISTROS PARA SU SOLICITUD  </td></tr>
                </table>
				</div>
				</div>";
			}
		$cont=1;
		
		while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
		
		{
			$planilla_id=$row["planilla_id"];
			 echo "<tr>
			<td>".$cont++."</td>"?>
			<td style="text-decoration:underline" ><a style="cursor:pointer" onClick="open_dialog(<?php echo '\''. $planilla_id.'\'' ?>);"><?php echo $row['planilla_comp']?></a></td>
			<?php echo"<td>".$row['banco_nombre']."</td>
			
			<td width='13%'>".$row['num_cuenta']."</td> 
			<td>".$row['num_depo']."</td>
			<td>".$row['fecha_depo']."</td>
			<td>".bsf($row['monto_depo'])."</td>
			<td>".$row['des_pago']."</td>
			
			<td ><div class='readmore'>".htmlentities($row['obs_depo'])."</div></td> 
		
			<td>".$row['creado_por']."</td>
			<td>".$row['fecha_creado']."</td>
			<td>".$row['fecha_modif']."</td>
			</tr>
			";}
			echo "</table>";
			}?>
            
<div id="dialog" style="display: none;" title="T&iacute;tulo del popup">
	<div id='detalles_planillas'>
     </div>
</div>
<script type="text/javascript" src="../funciones/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript" src="../funciones/jquery-ui.js"></script>
<script type="text/javascript" src="../funciones/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript" src="../funciones/lib/readmore-js/jqueryvermas_min.js"></script>
<script type='text/javascript' src='../funciones/funciones1.js'></script>
<script>
$( document ).ready(function() {
//el input de fecha
$(function() {
   $.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fechadesde").datepicker({
	changeMonth: true, //muestra para seleccionar el mes, en el calendario
	changeYear: true,
	dateFormat: 'yy-mm-dd'
});


$("#fechahasta").datepicker({
	changeMonth: true, //muestra para seleccionar el mes, en el calendario
	changeYear: true,
	dateFormat: 'yy-mm-dd'
});

  //VER MAS
$('.readmore').readmore({
  maxHeight:25,
  moreLink: '<a href="#">Ver mas</a>',
lessLink: '<a href="#">Cerrar</a>',

});
});
  });//fin jquery
  
  

</script>

</body>
</html>