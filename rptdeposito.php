<?php include('seguridad_trans.php'); 
include('conex.php');
//include('funciones/funcion.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Buscar Depositos</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="funciones/ajax.js"></script>
</head>
<body onload="javascript:loadurl('menu2.php','menu');">
<span id="menu"></span>
<div class="contenedor">
<br>
<br>
<h1>Depositos Cargados en el Sistema</h1>

    <form  action="" method="get" id="consulta" name="f_providencia">
        <table width="47%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas3" >
          <tr>
            <td align="center"><a>Nº de Deposito</a></td>
            
            
            <td  align="center"><a><input name="numero" type="text" /></a></td>
            <td ><script>DateInput('numero',true)</script></td>
          </tr>
          
          <tr>
            <td colspan="4" align="center"><input name="buscar" type="submit" value="Buscar" class="boton_grande" /></td>
          </tr>
        </table><br>
  </form>

</div>
	<?php
	if (isset($_GET["numero"])){
		$numero = $_GET["numero"];
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call rpt_depo(?)');
		$stmt->bind_param('i',$numero);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result();
			$cuantos_registros = $stmt->num_rows;
			if($cuantos_registros>0){
				$stmt->bind_result($tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$nomb_presentante,$ced_presentante,$nomb_empresa,$rif,$num_deposito,$banco,
								   $deposito_fecha,$num_cuenta,$deposito_monto,$creadopor,$fecha_creado,$ip);																								
				?><div class="contenedor2" align="left">                
                  <table id="t_deposito"  border="1" cellpadding="0" cellspacing="0" align="left">
                    <tbody id="tb_deposito">
                        <tr bgcolor="#9999FF">
                            <td width="4%">Nº</td>
                            <td >Num Planilla</td>
                            <td >Nombre Presentante</td>
                            <td >Cedula Presentante</td>
                            <td >Nombre Empresa</td>                            
                            <td >R.I.F</td>
                            <td >Num. Deposito</td>
                            <td >Banco</td>
                            <td>Fecha de Deposito</td>
                            <td>Num. Cuenta</td>
                            <td >Monto Deposito</td>
<!--                            <td >Operacion Registrada Por</td>
                            <td >Fecha Operacion</td>
                            <td >IP</td>-->                                                                                    
                        </tr><?php
						
				$cont=0;
				$cco=0;
				while($stmt->fetch()){
					$cco++; 
					$cont++;

					if(($cco%2)==0){ 
						$color = '#FFFFFF'; 
					}else{
						$color = '#E6F2E1'; 
					} ?>
					<tr onmouseover="ColorUno(this,'#CCCCCC');" onmouseout="ColorDos(this,'<?php echo $color; ?>')" bgcolor="<?php echo $color; ?>">
                    		<td><?php echo $cont ?></td>
                            <td ><a href="redirect2.php?p=2&planilla= <?php echo $num_planilla?>&tplid=<?php echo $tipo_planilla_id ?>&adm=0">  <?php echo $tipo_planilla_desc.'-'.$num_planilla ?></a></td>
                            <td ><?php echo $nomb_presentante ?></td>
                           <!-- <td ><a href="../redirect2.php?p=2&planilla=<?php //echo $deposito_id ?>&adm=1"><?php //echo $num_deposito ?></a></td>-->                            
                            <td ><?php echo $ced_presentante ?></td>
                            <td ><?php echo $nomb_empresa ?></td>
                            <td ><?php echo $rif ?></td>                            
                            <td><?php echo $num_deposito ?></td>                                                        
                            <td ><?php echo $banco ?></td>
                            <td ><?php echo $deposito_fecha ?></td>
                            <td ><?php echo $num_cuenta ?></td>                            
                            <td ><?php echo $deposito_monto ?></td>
<!--                            <td ><?php //echo $creadopor ?></td>
                            <td ><?php //echo $fecha_creado ?></td>
                            <td ><?php //echo $ip ?></td>-->                             
					</tr><?php
											
				}?></tbody>
                
                </table><br /><br />
                
          
				</div>
				<?php
			}else{
				?><div class="contenedor" align="center">
                <table width="420" border="1" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                </table>
				</div><?php
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
		$con->close();
	}?>




</body>
</html>