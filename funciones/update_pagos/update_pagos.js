// JavaScript Document

var num_depo_original;
var banco_original;
var monto_planilla;
var monto_depo;
function attach_file( p_script_url ) {
		// create new script element, set its relative URL, and load it
		script = document.createElement( 'script' );
		script.src = p_script_url;
		document.getElementsByTagName( 'head' )[0].appendChild( script );
}
function submit_form(pag) {
	var planilla = trim(document.f_monto.planilla.value);
	if (planilla != ''){
		if (IsNumericInt(planilla)){
			//document.f_monto.action=pag;
			document.f_monto.submit();
		}else{
			alert('ERROR EN NUMERO DE PLANILLA');
			return false;
		}
	}else{
		alert('ERROR EN NUMERO DE PLANILLA');
		return false;
	}
}
function pulsar(e,pag) { 
  tecla = (document.all) ? e.keyCode :e.which; 
  
  if (tecla == 13){
  	return submit_form(pag); 
  }else{
  	return soloEnteros(e);
  }
}
function enviar_form(){
	cadena=document.getElementById('exp_t').innerHTML;
	document.env_tabla.tabla_html.value = cadena;
}
  //funcion que abre, la ventana dialogo

  function abrir_dialog(td_id,bank_id,cuentas_id,depo){
	$("#dialog").dialog({
   	modal: true,
   	title: "Editar Depositos",
   	height:180,
   	width: 1100,
   	minWidth: 800,
   	maxWidth: 1150,
   	show: "fold",
   	hide: "scale"
	});
   $("#sel_banco option[value="+bank_id+"]").attr("selected",true);
   $('#num_deposito').val($.trim($('#depo_'+td_id+'').html()));
   monto_depo=$.trim($('#monto_'+td_id+'').html());
   monto_depo= monto_depo.replace('.','');
   monto_depo=monto_depo.replace(',00','');
   monto_depo=monto_depo.replace(',','.');
   monto_planilla=$.trim($('#montop_'+td_id+'').html());
   monto_planilla= monto_planilla.replace('.','');
   monto_planilla= monto_planilla.replace(',00','');
   monto_planilla= monto_planilla.replace(',','.');
   
   $('#monto_deposito').val(monto_depo);
   $('#datepicker').val($.trim($('#fecha_depo_'+td_id+'').html()));
   select_banco(bank_id);
   $('#id_depo').val(depo);
   banco_original=$("#sel_banco option:selected").val();
   num_depo_original= $.trim($('#depo_'+td_id+'').html());
   $('#motivo').val('');
  }
  
  function get_form_depositos()
  {
	 var num_deposito = $('#num_deposito').val();
	 var fecha_depo =$('#datepicker').val();
	 var monto=$('#monto_deposito').val();
	 var banco=$("#sel_banco option:selected").val();
	 var cuentas=$("#sel_cuenta option:selected").val()
	 var id_depo=$('#id_depo').val();
	 var motivo_depo=$.trim($('#motivo').val());
	 
	 
	 //validar que solo haya numeros y no letras en el numero deposito
	 for(i=0; i<num_deposito.length; i++)
	 {
		if(num_deposito.charCodeAt(i)<48 || num_deposito.charCodeAt(i)>57 )
		{
			alert("No se Permiten Letras o Caracteres Especiales en el Numero de Deposito");
			$('#num_deposito').focus();
			return false;
		}
		
	 }
	 
	 if ($.trim($('#motivo').val()).length<=0)
	 {
		 alert('El campo Motivo no puede estar vacio');
		 $('#motivo').focus();
		 return false;
     }
	
	if(!depos_planillas(monto)) return false;
	 
	 //esta parte comentada valida los pagos repetidos
	 //var status=get_deposito(num_deposito,banco);
//	if (status==1)
//	{
//		document.getElementById('existe_depo').innerHTML='Deposito Repetido';
//		setTimeout("document.getElementById('existe_depo').innerHTML='';",3000);
//		return false;
//	}
	$('#content_info_depo').html('<center><IMG SRC="../images/ajax-loader2.gif"></center>'); 
	$('#content_info_depo').load("edit_pagosplanillas_ajax.php", {num_deposito:num_deposito,fecha_depo:fecha_depo,monto:monto,banco:banco,cuentas:cuentas,id_depo:id_depo,motivo_depo:motivo_depo},function(responseTxt,statusTxt,xhr){
			
    	if(statusTxt=="error")
		{
			alert("Error: "+xhr.status+": "+xhr.statusText);
		}
		else
		{
			$("#dialog").dialog("close");
		}
   });
}
//calcula si con el monto nuevo del deposito corresponde a valores correctos, es decir no se exceda
//de 100 Bs y no sea menor al valor de la planilla 
function depos_planillas(monto_new)
{
	var montop=0;
	var retorno = true;
	$("#table_radius tbody tr").each(function () {
            $(this).find("td").eq(9).each(function () {

				montop_conv= $(this).html().replace('.','');
   				montop_conv=montop_conv.replace(',00','');
   				montop_conv=montop_conv.replace(',','.');
				montop=parseFloat(montop)+parseFloat(montop_conv);
            })
	})
	var dif_depo=parseFloat(monto_depo)-monto_new;
	var montoa=montop-parseFloat(dif_depo);
	if (parseFloat(montoa)<parseFloat(monto_planilla))
	{
		alert('La suma total de los pagos es menor al total de la planilla, imposible procesar esto.');
		retorno=false;
	}
	else if(montoa>(parseFloat(monto_planilla)+100))
	{
		alert('La suma total de los pagos es 100Bs mayor al total de la planilla, imposible procesar esto.');	
		retorno=false;
	}
return retorno;
}
function get_deposito(num_deposito,banco)
{
	var status;
	if(banco_original!=banco ||num_depo_original!=num_deposito)
	{
		 $.ajax({
			 async:false,
			 type: 'POST',
			 dataType:"html",
			 url:"valida_existe_depo.php",
			 data:"banco="+banco+"&num_deposito="+num_deposito,
			 success:function(result){
			status=result;
    }});
	}
return status;
}
	 
function select_banco(valor){
	var txtbox= document.getElementById('num_deposito');
		
	if (valor==0){
		Ocultar('sel_cuenta');
		return false;
	}else{		
	$('#cuentas').html('<center><IMG SRC="../images/ajax-loader2.gif"></center>'); 
	$('#cuentas').load("../form_sel_cuenta.php?banco_id="+valor+"&v=1", {banco_id:valor,v:1},function(responseTxt,statusTxt,xhr)
	{
    	if(statusTxt=="error")
		{
			alert("Error: "+xhr.status+": "+xhr.statusText);
		}
   });			
		return true;
	}	
	
	
	
}
$(function() {
   $.datepicker.setDefaults($.datepicker.regional["es"]);
$("#datepicker").datepicker({
	changeMonth: true, //muestra para seleccionar el mes, en el calendario
	changeYear: true,
	dateFormat: 'dd-mm-yy'
});
  });
  
function pid(){
	 alert( $("#pid").val());
	}
	
// fin del document