<?php 
header('Content-Type: text/html; charset=UTF-8'); 
include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');

if(isset($_POST['num_deposito'])){
	$num_deposito=$_POST['num_deposito'];
}else{
	die('No se recibio el parametro esperado, lo sentimos1');
}

if(isset($_POST['fecha_depo'])){
	$date = new DateTime($_POST['fecha_depo']);
    $fecha_depo=$date->format('Y-m-d');
}else{
	 die('No se recibio el parametro esperado, lo sentimos2');
}

if(isset($_POST['monto'])){
    $monto=$_POST['monto'];
}else{
	die('No se recibio el parametro esperado, lo sentimos3');
}


if(isset($_POST['banco'])){
    $banco=$_POST['banco'];
}else{
	die('No se recibio el parametro esperado, lo sentimos4');
}

if(isset($_POST['cuentas'])){
    $cuentas=$_POST['cuentas'];
}else{
	die('No se recibio el parametro esperado, lo sentimos5');
}


if(isset($_POST['id_depo'])){
	$id_depo=decrypt($_POST["id_depo"],'123');
}else{
	die('No se recibio el parametro esperado, lo sentimos6');
}

if(isset($_POST['motivo_depo'])){
	$motivo_depo=$_POST["motivo_depo"];
}else{
	die('No se recibio el parametro esperado, lo sentimos7');
}

if(isset($_SESSION["usuario"])){
	 $usuario=$_SESSION["usuario"];
}else{
	die('No se recibio el parametro esperado, lo sentimos8');
}
$ip=getRealIpAddr();

$mysqli = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_errno())
{
	printf('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. %s\n', mysqli_connect_error());
   	exit();
}

$result = $mysqli->query("CALL pagos_planilla_update('$banco','$cuentas','$fecha_depo','$num_deposito','$monto','$id_depo','$motivo_depo','$usuario','$ip')");
if(!$result) die("CALL failed: (" . $mysqli->errno . ") " . $mysqli->error);

if($result->num_rows > 0) 
{
		  echo"<table class='table' id='table_radius'>
		  		<thead>
                        <tr style='font-size:10px;'>
                            <th width='4%' style='border-top-left-radius:10px;'>N&deg;</th>
                            <th ondblclick='pid()'>Num. Planilla</a></th>
							<th >Monto. Planilla</th>
                            <th>Registro</th>
                            <th >Tipo de Pago</th>
                            <th >Banco</th>
                            <th >Numero de Cuenta</th>
                            <th >Fecha Deposito</th>
                            <th >Num. Deposito</th>
                            <th >Monto</th>
                            <th >Observaciones</th>
                            <th style='border-top-right-radius:10px;'>Editar</th>                                
                        </tr></thead><tbody id='tb_deposito'>";
		$cont=1;
	while($rows[]=$row = mysqli_fetch_array($result, MYSQLI_ASSOC))
	{
			$status_result=$row["status_depo"];
			$id_deposito=encrypt($row["id_deposito"],'123');
			 echo "<tr style=''>
			<td>".$cont++."</>
			<td>".$row["num_planilla"]."</td>
			<td id='montop_$cont'>".bsf($row["monto_planilla"])."</td>
			<td>".$row["descripcion_registro"]."</td>
			<td id='tipo_$cont'>".$row["descripcion_pago"]."</td>
			<td id='banco_$cont'>".$row["banco_nombre"]."</td>
			<td id='num_cuenta_$cont' width='18%'>".$row["NumCuenta"]."</td>
			<td id='fecha_depo_$cont' width='10%'>".$row["fecha_depo"]."</td>
			<td id='depo_$cont'>".$row["num_deposito"]."</td>
			<td id='monto_$cont'>".bsf($row["monto"])."</td>
			<td>".str_replace('|','<br><hr>',$row["observaciones"])."</td>";?>
			<td><input name="" type="button" value="Editar" onclick="abrir_dialog(<?php echo $cont.','.$row["banco_id"].','.$row["cuentas_id"].',\''.$id_deposito.'\'';?>)" /></td>
 		</tr><?php 
	}echo"</tbody>
          </table>
          </div>";
		  
	if($status_result==0)
	{
   	echo "
	<script>
			document.getElementById('existe_depo').innerHTML='Guardado Con Exito!';
	</script>";
	}
	else
	{
	 "<script>
			document.getElementById('existe_depo').innerHTML='Deposito Repetido';
	</script>";
	}
		  
}
else 
{
    echo"<div class='content_no_found'><H4>NO SE ENCONTRARON REGISTROS PARA SU SOLICITUD</h4></div>";
}

 /* liberar el resultset */
$result->free();
/* cerrar la conexión */
$mysqli->close();
