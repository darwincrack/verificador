<?php include('seguridad_trans.php');
include('funciones/funcion.php');
include('conex.php');
if(isset($_SESSION['tplid'])){
	$tplid=$_SESSION['tplid'];
}else{
	$tplid='';
}

$admin=procedencia_reportes_admin($_SERVER['HTTP_REFERER']);
$perfil_tipo_acto_id=$_SESSION["perfil_tipo_acto_id"];


if($admin==true){
	$_SESSION['pago_admin']=true;
}else{
	$_SESSION['pago_admin']=false;
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="funciones/funciones1.js"></script>
<script type="text/javascript" src="funciones/ajax.js"></script>
<script type="text/javascript">
function attach_file( p_script_url ) {
		// create new script element, set its relative URL, and load it
		script = document.createElement( 'script' );
		script.src = p_script_url;
		document.getElementsByTagName( 'head' )[0].appendChild( script );
}
function submit_form(pag) {
	var planilla = trim(document.f_monto.planilla.value);
	if (planilla != ''){
		if (IsNumericInt(planilla)){
			document.f_monto.action=pag;
			document.f_monto.submit();
		}else{
			alert('ERROR EN NUMERO DE PLANILLA');
			return false;
		}
	}else{
		alert('ERROR EN NUMERO DE PLANILLA');
		return false;
	}
}
function pulsar(e,pag) { 
  tecla = (document.all) ? e.keyCode :e.which; 
  
  if (tecla == 13){
  	return submit_form(pag); 
  }else{
  	return soloEnteros(e);
  }
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
</head>
<?php
	if($admin==true){
		echo '<body onload="javascript:loadurl(\'reportes/menu2.php\',\'menu\');">';
	}else{
		echo '<body onload="javascript:attach_file(\'menu.php\');">';
	}
?>

<span id="menu">
</span>
<br />
<div class="contenedor">
	<h1>Registrar Pago de Planilla</h1>
    <?php echo ($admin==true)?'<h2>Nivel Administrativo</h2>':''; ?>
</div>
<div class="contenedor" id="consulta">
    <form id="f_monto" name="f_monto" method="get" >
        <table width="55%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas">
          <tr>
            <td width="48%" ><h2>Introducir N� de Planilla:</h2></td>
            <td width="33%" align="center" ><?php 
				if ($perfil_tipo_acto_id==1){ /*SI EL USUARIO PERTENECE AL GRUPO REGISTRO MERCANTIL */
            	  	echo '<input name="tplid" value="1" type="hidden" />FM-';
				}else{ /*SI EL USUARIO PERTENECE A SAPI Y REGISTROS*/
                	echo '<select name="tplid" id="tplid">';
					$con = new mysqli($host,$user,$clave,$db,$puerto);
					if (mysqli_connect_error()) {
						die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
					}
					$stmt = $con->stmt_init();
					$stmt->prepare('call list_tipo_planilla()');
					if(!$stmt->execute()){
						throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
					}else{
						$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
						$cuantos_registros = $stmt->num_rows;
						if($cuantos_registros>0){
							$stmt->bind_result($tipo_planilla_id,$descrip);
							while($stmt->fetch()){
								if($tplid==$tipo_planilla_id){
									print "<option value=\"$tipo_planilla_id\" selected=\"selected\">$descrip</option>";
								}else{
									print "<option value=\"$tipo_planilla_id\">$descrip</option>";
								}	                                      
							}                      	                      	
						}
					}
					$stmt->free_result();
					$stmt->close();
					while($con->next_result()) { }
					$con->close();
                	echo '</select>';
                }?>                 
             <input name="planilla" type="text" size="10" maxlength="7" onkeypress="return pulsar(event,'redirect2.php');"/><input name="p" type="hidden" value="0" /></td>
            <td width="19%"><input type="button" value="  Enviar  " class="boton" name="busca"  onclick="return submit_form('redirect2.php')" /></td>
          </tr>
        </table>
  </form>
</div>
</body>
</html>