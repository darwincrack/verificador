<?php include('seguridad_adm.php');
include('../conex.php')?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript" src="../funciones/calendarDateInput.js"></script>
<script type="text/javascript">
function ColorUno(src,color_entrada) {
	src.style.backgroundColor=color_entrada;
} 
function ColorDos(src,color_default) { 
    src.style.backgroundColor=color_default;
	//src.style.cursor="default"; 
}
</script>
</head>
<body onload="loadurl('menu.php','menu')">
<span id="menu"></span>
<div class="contenedor">
	<h1>Accesos al Sistema (Rango de Fechas)</h1>
    <form  action="" method="get" id="consulta" name="f_providencia">
        <table width="44%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas3" >
          <tr>
            <td align="center"><a>Seleccione Dia:</a></td>
            <td  ><script>DateInput('fechadesde',true)</script></td>
          </tr><?php
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechadesde"],8,2);
                $mes=substr($_GET["fechadesde"],5,2)-1;
                $a�o=substr($_GET["fechadesde"],0,4);
                echo "<script>fechadesde_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }?>
          <tr>
            <td colspan="4" align="center"><input name="buscar" type="submit" value="Buscar" class="boton_grande" /></td>
          </tr>
        </table>
    </form><br  /><?php
	if (isset($_GET["fechadesde"])){
		$fecha_desde = $_GET["fechadesde"];
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call rpt_login(?,?)');
		$stmt->bind_param('ss',$fecha_desde,$fecha_desde);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result();
			$cuantos_registros = $stmt->num_rows;
			if($cuantos_registros>0){
				$stmt->bind_result($fecha_log,$nombre,$login,$ip_log,$accion);
				?><table id="t_deposito"  border="1" cellpadding="0" cellspacing="0">
                    <tbody id="tb_deposito">
                        <tr bgcolor="#9999FF">
                            <td width="2%"></td>
                            <td>ACCI�N</td>
                            <td width="19%">FECHA</td>
                            <td width="33%">FUNCIONARIO</td>
                            <td width="12%">USUARIO</td>
                            <td width="19%">IP</td>
                        </tr><?php
				$cont=0;
				$cco=0;
				while($stmt->fetch()){
					$cco++; 
					$cont++;
					if($accion=='ENTRADA'){ 
						$color = '#FFFFFF'; 
					}elseif($accion=='SALIDA'){
						$color = "#FFFF66"; 
					}else{
						$color = '#E6F2E1'; 
					}?>
					<tr onmouseover="ColorUno(this,'#CCCCCC');" onmouseout="ColorDos(this,'<?php echo $color; ?>')" bgcolor="<?php echo $color; ?>">
						<td align="center"><?php echo $cont ?></td>
                        <td align="center"><?php echo $accion ?></td>
						<td align="center"><?php echo $fecha_log ?></td>                        
						<td align="center"><?php echo $nombre ?></td>
						<td align="center"><?php echo $login ?></td>                    
						<td align="center"><?php echo $ip_log ?></td>
					</tr><?php
				}?></tbody>
                </table><?php
			}else{
				?><table width="420" border="1" align="center" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                  </table><?php
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
	}?>
</div>
</body>
</html>