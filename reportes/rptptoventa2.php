<?php include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');

if (isset($_GET["numero"])){
	$numero = $_GET["numero"];
}else{
	$numero = '';
}	
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Depositos</title>
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript">
function ColorUno(src,color_entrada) {
	src.style.backgroundColor=color_entrada;
} 
function ColorDos(src,color_default) { 
    src.style.backgroundColor=color_default;
	//src.style.cursor="default"; 
}
</script>
</head>
<body onload="loadurl('menu.php','menu')">
<span id="menu"></span>
<div class="contenedor">
<h1>N&deg; de Aprobaci&oacute;n Cargados en el Sistema</h1>
   
    <form  action="" method="get" id="consulta" name="f_providencia">
        <table width="47%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas3" >
          <tr>
            <td align="center"><a>N� de Aprobaci&oacute;n</a></td>            
            <td align="left"><a><input align="left" name="numero" type="text" value="<?php echo $numero ?>" /></a></td>
          </tr>          
          <tr>
            <td colspan="2" align="center"><input name="buscar" type="submit" value="Buscar" class="boton_grande" /></td>
          </tr>
        </table>
  </form><br  />
</div>
	<?php
	if (isset($_GET["numero"])){
		$numero = $_GET["numero"];
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call rpt_pto_venta2(?)');
		$stmt->bind_param('i',$numero);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result();
			$cuantos_registros = $stmt->num_rows;
			if($cuantos_registros>0){
				/*$stmt->bind_result($num_planilla,$nomb_presentante,$ced_presentante,$nomb_empresa,$rif,$num_deposito,$banco,
								   $deposito_fecha,$num_cuenta,$deposito_monto,$creadopor,$fecha_creado,$ip);		*/
								   	
				$stmt->bind_result($tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$nomb_presentante,$ced_presentante,$nomb_empresa,$rif,$banco,$num_lote,$num_deposito,$deposito_fecha,$deposito_monto,$creadopor,$fecha_creado,$ip,$observ,$name_accionista,$direccion_local);		   																					
				?><div class="contenedor2" align="left">                
                  <table id="t_deposito"  border="1" cellpadding="0" cellspacing="0" align="left">
                    <tbody id="tb_deposito">
                        <tr bgcolor="#9999FF">
                            <td width="4%"></td>
                            <td >Num. Planilla</td>
                            <td >Nombre Presentante</td>
                            <td >Cedula Presentante</td>
                            <td >Nombre Empresa</td>                            
                            <td >R.I.F</td>
                            <td>Direccion Local</td>
                            <td>Accionista</td>
                            <td >Banco</td>
                            <td>Num. Lote</td>
							<td>Num. Aprob</td>                            
                            <td>Fecha de Aprob</td>                          
                            <td >Monto Aprob</td>
                            <td >Observaciones</td>
                            <td >Operacion Registrada Por</td>
                            <td >Fecha Operacion</td>
                            <td >IP</td>                                                                                    
                        </tr><?php
						$excel = "NUM. DE PLANILLA\tNOMBRE DEL PRESENTANTE\tCED. DEL PRESENTANTE\tNOMBRE DE LA EMPRESA\tR.I.F\tBANCO\tNUM. DE APROBACION\tFECHA DE APROBACION\tMONTO\tREGISTRADO POR\tFECHA DE OPERACION\tIP\n\n";
				$cont=0;
				$cco=0;
				while($stmt->fetch()){
					$cco++; 
					$cont++;

					if(($cco%2)==0){ 
						$color = '#FFFFFF'; 
					}else{
						$color = '#E6F2E1'; 
					} ?>
					<tr onmouseover="ColorUno(this,'#CCCCCC');" onmouseout="ColorDos(this,'<?php echo $color; ?>')" bgcolor="<?php echo $color; ?>">
                    		<td><?php echo $cont ?></td>
                            <td ><a href="../redirect2.php?p=2&planilla=<?php echo $num_planilla ?>&tplid=<?php echo $tipo_planilla_id ?>&adm=1"><?php echo $tipo_planilla_desc.'-'.$num_planilla ?></a></td>
                            <td ><?php echo $nomb_presentante ?></td>
                           <!-- <td ><a href="../redirect2.php?p=2&planilla=<?php //echo $deposito_id ?>&adm=1"><?php //echo $num_deposito ?></a></td>-->                            
                            <td ><?php echo $ced_presentante ?></td>
                            <td ><?php echo $nomb_empresa ?></td>
                            <td ><?php echo $rif ?></td>
                            <td><?php echo $direccion_local ?></td>
                            <td ><?php echo  $name_accionista?></td>        
                            <td ><?php echo $banco ?></td>
                            <td><?php echo $num_lote ?></td>
                            <td ><?php echo $num_deposito ?></td>                   
                            <td ><?php echo $deposito_fecha ?></td>                                                        
                            <td ><?php echo $deposito_monto ?></td>
                            <td><?php echo $observ ?></td>
                            <td ><?php echo $creadopor ?></td>
                            <td ><?php echo $fecha_creado ?></td>
                            <td ><?php echo $ip ?></td>
                            <td></td>                             
					</tr><?php
						$excel .="$num_planilla\t".trim(ConHtml($nomb_presentante))."\t".trim(ConHtml($ced_presentante))."\t"
								.trim(ConHtml($nomb_empresa))."\t".trim(ConHtml($rif))."\t".trim(ConHtml($banco))."\t$num_deposito\t$deposito_fecha\t".trim(bsf($deposito_monto))."\t$creadopor\t$fecha_creado\t$ip\n";					
				}?></tbody>
                
                </table><br /><br />
                
          
				</div><?php
			}else{
				?><div class="contenedor" align="center">
                <table width="420" border="1" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUENTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                </table>
				</div><?php
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
		$con->close();
	}?>

</body>
</html>