<?php
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=rpt_cargas.xls");
header("Pragma: no-cache");
header("Expires: 0");
include('seguridad_trans.php'); 
include('conex.php');
include('funciones/funcion.php');

$fecha_desde =$_POST["desde"];
$fecha_hasta=$_POST["hasta"];
$usuario=$_POST["usuario"];

$con = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_error()) {
    die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}
$stmt = $con->stmt_init();
$stmt->prepare('call rpt_cargas(?,?,?)');
$stmt->bind_param('sss',$fecha_desde,$fecha_hasta,$usuario);
if(!$stmt->execute()){
	throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
}else{
	$stmt->store_result(); //Sin esta línea no podemos obtener el total de resultados anticipadamente
	$cuantos_registros = $stmt->num_rows;
	if($cuantos_registros>0){
				$stmt->bind_result($infos,$tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$total_planilla,$deposito_id,$num_deposito,$banco,$deposito_monto,$num_aprob,$pto_venta_monto,$timbre_monto,$fecha_creado);
		$excel= "usuario: ".$usuario."\n";
		$excel .= "TIPO DE INFO\tNUM. DE PLANILLA\tTOTAL EN PLANILLA\tNUM. DE DEPOSITO\tBANCO\tMONTO DEL DEPOSITO\tAPROBACION DE TRANSACCION\tMONTO DE TRANSACCION\tMONTO TIMBRE\tFECHA DE OPERACION\n";	
		while($stmt->fetch()){
			$excel .= "$infos\t$num_planilla\t$total_planilla\t$num_deposito\t".trim(ConHtml($banco))."\t$deposito_monto\t$num_aprob\t".trim(bsf($pto_venta_monto))."\t".trim(bsf($timbre_monto))."\t$fecha_creado\n";
		}
		$excel = str_replace("\"", "", $excel);
		print $excel;
	}
}
$stmt->free_result();
$stmt->close();
while($con->next_result()) { }
$con->close();
