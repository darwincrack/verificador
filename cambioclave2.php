<?php include('conex.php');
include('seguridad_trans.php');
include ('funciones/funcion.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin t&iacute;tulo</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="funciones/funciones1.js"></script>
</head>
<body>
    <div class="contenedor">
        <h1>Cambio de Clave</h1>
    </div><?php
    $con = new mysqli($host,$user,$clave,$db,$puerto);
	if (mysqli_connect_error()) {
    	die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
	}
	$clave_actual	= 	texto_limpio($_POST["claveactual"]);
	$clave_nueva1	= 	texto_limpio($_POST["contrasena1"]);
	$clave_nueva2	= 	texto_limpio($_POST["contrasena2"]);
	$usuario		= 	texto_limpio($_SESSION["usuario"]);
	
	$stmt = $con->stmt_init();
	$stmt->prepare('call cambio_clave(?,?,?,?)');
	$stmt->bind_param('ssss',$usuario,$clave_actual,$clave_nueva1,$clave_nueva2);
	if(!$stmt->execute()){
		throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
	}else{
		$stmt->store_result();
		$cuantos_registros = $stmt->num_rows;
		if($cuantos_registros>0){
			$stmt->bind_result($switch,$mensaje);
			while($stmt->fetch()){?>
                <div class="contenedor" id="resp_serv">
                    <table width="58%" border="0" cellspacing="0" cellpadding="0"  align="center">
                        <tr><?php
                            if ($switch == 0){ 
                                echo '<td bgcolor="#FF0000">'. $mensaje .'</td>';
							}elseif ($switch == 1){
                                echo '<td bgcolor="#009900">'. $mensaje .'</td>';
                            }/*else{
								echo '<td bgcolor="#FF0000">'.$switch.' '.$mensaje.'</td>';
							}*/?>
                        </tr>
                     </table>
                </div><?php
			}
        }else{?>
			<div class="contenedor" id="resp_serv">
            	<h3>ERROR EN LA OPERACION, INTENTE DE NUEVO</h3>
            </div><?php
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
	}
	$con->close();?>
    <div class="link_foot">
      <p><a href="cambioclave.php">Volver a Cambiar Clave</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="body.php">Ir a Pagina Inicial</a></p>
    </div>		
</body>
</html>