<?php 
include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');
include('../funciones/mysql.php');
ob_start();
$tipo_acto_act=0;
$nombre_registro_ant=0;
$total_plan=0;
$cont=0;
 $total=0;
 $co=0;
 $id_planilla_ant=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Pago de Planillas</title>
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript" src="../funciones/calendarDateInput.js"></script>
<script type="text/javascript">
function ColorUno(src,color_entrada) {
	src.style.backgroundColor=color_entrada;
} 
function ColorDos(src,color_default) { 
    src.style.backgroundColor=color_default;
	//src.style.cursor="default"; 
}
function cont_planillas(id_td){
	var result=0;
	content = document.getElementById(id_td);
	var contador= content.innerHTML;
	contador_convert=parseInt(contador);
	var result = contador_convert + 1;
	document.getElementById(id_td).innerHTML=result;	
	}
	function enviar_form(){
		cadena=document.getElementById('contiene_table').innerHTML;
		document.env_tabla.tabla_html.value = cadena;
		}

</script>
</head>
<body onload="loadurl('menu.php','menu')">
<span id="menu"></span>
<div class="contenedor">
	<h1>Tipo de Acto</h1>
    <h3>Para listados de un solo dia, colocar la misma fecha en ambos cuadros.</h3>
   <form  action="" method="get" id="consulta" name="f_providencia">
        <table width="71%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas3" >
          <tr>
            <td align="center"><a>Fecha Desde:</a></td>
            <td  ><script>DateInput('fechadesde',true)</script></td>
            <td  align="center"><a>Fecha Hasta:</a></td>
            <td ><script>DateInput('fechahasta',true)</script></td>
          </tr><?php
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechadesde"],8,2);
                $mes=substr($_GET["fechadesde"],5,2)-1;
                $a�o=substr($_GET["fechadesde"],0,4);
                echo "<script>fechadesde_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechahasta"],8,2);
                $mes=substr($_GET["fechahasta"],5,2)-1;
                $a�o=substr($_GET["fechahasta"],0,4);
                echo "<script>fechahasta_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }?>
          <tr>
            <td colspan="4" align="center"><input name="buscar" type="submit" value="Buscar" class="boton_grande" /></td>
          </tr>
        </table>
    </form>
    <br  />
    <?php
	if (isset($_GET["fechadesde"]) && isset($_GET["fechahasta"])){
		$fecha_desde = $_GET["fechadesde"];
		$fecha_hasta = $_GET["fechahasta"];
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ')'. mysqli_connect_error());
		}	
		$stmt = $con->stmt_init();
		$stmt->prepare('call rpt_tipo_acto_con_pagos02(?,?)');
		$stmt->bind_param('ss',$fecha_desde,$fecha_hasta);
		if(!$stmt->execute()){
			set_time_limit(0);
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
			$cuantos_registros = $stmt->num_rows;			
			if($cuantos_registros>0){
				$stmt->bind_result($info,$registro_id,$nombre_registro,$tipo_acto,$numeral,$id_planilla); ?>
                
			
               <div id="contiene_table">
			<table width="66%" border="0" align="center" cellpadding="1" cellspacing="1" class="reporte"  id="table_id">		
					<thead id="cabecera">
                    <tr> 
                    	<td></td>
                    	<td width="29%">REGISTRO</td>
                    	<td width="53%">TIPO DE ACTO</td>
                        <td width="18%">CANTIDAD</td>
                    </tr>
				  </thead><tbody>
                  <?php 
				  while($stmt->fetch()){
					set_time_limit(0);
					  if($id_planilla_ant != $id_planilla)
					  {
					  $total=$total+1;
					  if (trim($tipo_acto)== trim($tipo_acto_act) && trim($registro_id)==trim($registro_id_ant) )
					  {
						 set_time_limit(0);
						echo'<script type="text/javascript"> cont_planillas('.$cont.') </script>';  
						  }
					else{
					  $cont++;
					  ?>
                       <tr>
                    <td><?php echo $cont?></td>
					<td><?php echo $nombre_registro ?></td>
					<td><?php echo $tipo_acto?></td>
                    <td id=<?php echo  $cont?>><?php echo 1 ?></td>
					</tr>
					<?php // $tipo_acto_act=$tipo_acto;
						  //$registro_id_ant=$registro_id;
						  //$id_planilla_ant= $id_planilla;
						  }
						  }
						  $tipo_acto_act=$tipo_acto;
						   $registro_id_ant=$registro_id;
						   $id_planilla_ant= $id_planilla;
				  		}?>
                       </tbody>
                       <tfoot>
                     <tr>
                       <td colspan="3">TOTAL</td>           
                       <td><?php echo $total?></td>
                     </tr>
                    </tfoot>
                       
				</table>
                
                </div>
                	<form action="export2.php"  method="post" id="env_tabla" name="env_tabla" onsubmit="return enviar_form()">
					<input type="hidden" name="tabla_html" value=""><br /> <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type = "submit" value = "Exportar a Excel" class="boton_grandeExport" align="center"/>
                    </form>
               
                <?php }
				     else{
						 ?>
                         <table width="420" border="1" align="center" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                  </table>
                  <?php 
						 }
}	
	$stmt->free_result();
	$stmt->close();
	while($con->next_result()) { }
	$con->close();
	}
	?>	
    </div>

</body>
</html>