<?php
    session_start();  
	//si no existe le mando otra vez a la portada
	$_SESSION = array();
	if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000,
			$params["path"], $params["domain"],
			$params["secure"], $params["httponly"]
		);
	}
// Finally, destroy the session.
	session_destroy();
	header("Location: login.php"); 
	/*echo "<script language=\"JavaScript\" src=\"funciones/funciones1.js\" type=\"text/javascript\"></script>\n"; 
	echo "<script>\n";
	echo "MenuClic('login.php')";
	echo "</script>\n";*/
    exit(); 
?>