<?php
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=Total_Registros.xls");
header("Pragma: no-cache");
header("Expires: 0");
include('./seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');

$fecha_desde =$_POST["desde"];
$fecha_hasta=$_POST["hasta"];

$con = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_error()) {
    die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}
$stmt = $con->stmt_init();
$stmt->prepare('call rpt_total_registros(?,?)');
$stmt->bind_param('ss',$fecha_desde,$fecha_hasta);
if(!$stmt->execute()){
	throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
}else{
	$stmt->store_result(); //Sin esta línea no podemos obtener el total de resultados anticipadamente
	$cuantos_registros = $stmt->num_rows;
	if($cuantos_registros>0){
		$stmt->bind_result($num_registro,$banco,$deposito_total,$pto_venta_total,$timbre_total);
		$total=0;
		$excel = "REGISTRO\tBANCOS\tTOTAL DEPOSITOS\tTOTAL PTO VENTA\tTOTAL TIMBRES\tTOTAL\n";				
		while($stmt->fetch()){
			$total = $deposito_total + $pto_venta_total + $timbre_total;
			$excel .= $num_registro."\t".trim($banco)."\t".trim(bsf($deposito_total))."\t".trim(bsf($pto_venta_total))."\t".trim(bsf($timbre_total))."\t".trim(bsf($total))."\n";
		}
		$excel = str_replace("\"", "", $excel);
		print $excel;
	}
}
$stmt->free_result();
$stmt->close();
while($con->next_result()) { }
$con->close();

?>