<?php include ('funciones/funcion.php');
include('seguridad_trans.php');
include ('conex.php');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="funciones/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="funciones/funciones1.js?<?php echo time(); ?>"></script>

<script type="text/javascript">

//llenar lista de parroquias
function llenar_select_parroquia() {
		$("#parroquia").html('<IMG SRC="ajax-loader_mini.gif">');
		$("#parroquia").load("list_parroquias.php",function(responseTxt,statusTxt,xhr){
    	if(statusTxt=="error")
		{
      		alert("Error: "+xhr.status+": "+xhr.statusText);
		}
		else
		{
			<?php
			
			if (isset($_GET["parroquia"]))
		   	{?>
				$("#parroquia option[value="+ <?php echo $_GET["parroquia"] ?> +"]").attr("selected",true)
			<?php }?>
		}
  		});
}



function submit_form() {

var campo_ced_present	= 	trim(f_monto.ced_present.value);
var planilla 			= 	trim(document.f_monto.planilla.value);
var folios 				= 	document.f_monto.i_folios.value;
var tipo_acto 			= 	$('#tipo_acto').val();


//var tipo_acto = $('#tipo_acto').val();
//DETERMINA SI ES UN TIPO DE ACTO VALIDO, PARA PODER VALIDAR EL RIF
//if (tipo_acto!='RESERVA DE NOMBRE'){
if ( ! validar_rif()) return false;
//}

if (!(IsNumericInt(campo_ced_present))){
alert("Error en la Cedula del Presentante");
return false;
}

if( ! validar_accionista_direc_local()) return false;

//var planilla = trim(document.f_monto.planilla.value);
		if (IsNumericInt(folios)){
			if (planilla != ''){
				if (IsNumericInt(planilla)){
					document.getElementById('nombre_parroquia').value=$("#parroquia option:selected").text();
					/*selector = document.getElementById('reg_id');
					indice = (selector.selectedIndex);	
					SelectorTexto = selector.options[indice].text;
					document.f_monto.registro.value=SelectorTexto;*/	
					document.f_monto.submit();
				}else{
					alert('ERROR EN NUMERO DE PLANILLA');
					return false;
				}
			}else{
				document.f_monto.submit();
			}
		}else{
			alert ('INTRODUCIR NUMERO CORRECTO DE FOLIOS');
			return false;
		}
}


function attach_file( p_script_url ) {
		// create new script element, set its relative URL, and load it
		script = document.createElement( 'script' );
		script.src = p_script_url;
		document.getElementsByTagName( 'head' )[0].appendChild( script );
}

function validacion(f)  {
if (isNaN(f_monto.ced_present.value)) {
alert("Error:\nEste campo debe tener s�lo n�meros.");
f_monto.ced_present.focus();
return (false);
 }
}


</script>
</head>
<?php 
	 $usuario = $_SESSION["usuario"];
	 $tipo_acto=$_GET["tipo_acto"];
	 $numeral=$_GET["lit"];
	 date_default_timezone_set('America/Caracas');
	 $fecha=date('d/m/Y');?>
<body onload="javascript:attach_file('menu.php');">
<div id="menu">
</div>
<div class="contenedor" id="consulta">
	<h1>Calculo de Actos Realizados Art. 13 de La Ley de Timbre Fiscal</h1>
    <h2>Numeral 9</h2>
    <h2>Todos los demas documentos distintos a los regulados por la ley que desean asentarse en los Registros</h2>
    <form action="lit_9.php" method="get"  name="f_monto">
    <table width="627" border="0" align="center" class="tablas" id="consulta">
        <tr>
          <td align="right"><strong>N� Planilla</strong></td>
          <td><input name="tipo_planilla" id="tipo_planilla" value="1" type="hidden" />FM-
          <input name="planilla" id="num_planilla" type="text" size="10" maxlength="7" onkeypress="return soloEnteros(event)"/><div id="img_num_planilla" style="display:inline;"></div></td>
        </tr>
        <tr>
          <td align="right"><strong>Oficina de Registro:</strong></td>
          <td><?php /*?><select name="reg_id" id="reg_id"><?php			 
                        $con = new mysqli($host,$user,$clave,$db,$puerto);
                        if (mysqli_connect_error()) {
                            die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
                        }
                        $stmt = $con->stmt_init();
                        $stmt->prepare('call sel_registro()');
                        if(!$stmt->execute()){
                            throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
                        }else{
                            $stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
                            $cuantos_registros = $stmt->num_rows;
                            if($cuantos_registros>0){
                                $stmt->bind_result($reg_id,$descrip);
                                    while($stmt->fetch()){	
                                        print "<option value=\"$reg_id\">$descrip</option>";
                                    }                      	
                            }
                        }
                        $stmt->free_result();
                        $stmt->close();
                        while($con->next_result()) { }
                        $con->close();?>
                 </select><input name="registro" type="hidden" value="" /><?php */?><?php echo $_SESSION["registro_desc"]; ?></td>
        </tr>
        <tr>
            <td align="right"><strong>Estado:</strong></td>
            <td><input name="estado" type="text" size="65" maxlength="65"  value="DISTRITO CAPITAL Y ESTADO MIRANDA" onblur="javascript:this.value=this.value.toUpperCase();" readonly="readonly"/></td>
        </tr>
        <tr>
            <td align="right"><strong>C.I:</strong></td>
            <td><select name="nacionalidad">
            <option value="" selected ></option>
            <option value="V" >V</option>
            <option value="E" >E</option>
            </select><input type="text" size="20" maxlength="13" onkeypress="return soloNumeros2(event)"  name="ced_present"/>
            (sin puntos) </td>
        </tr>
        <tr>
            <td align="right"><strong>Nombre del Presentante:</strong></td>
            <td><input name="nombre_presentante" type="text" size="65" maxlength="65"  onblur="javascript:this.value=this.value.toUpperCase();"/></td>
        </tr>

                                
        <tr>
            <td  align="right"><strong>RIF N� / C.I</strong>:</td>
            <td width="397"><input name="rif" id="rif" maxlength="10" type="text" onKeyPress="javascript:this.value=this.value.toUpperCase();"  onblur="javascript:this.value=this.value.toUpperCase();" size="20" maxlength="13" /><div id="img_rif" style="display:inline;"></div></td>
        </tr>
                <tr>
            <td align="right"><font size="-1"><strong>Nombre de la Persona Natural o Juridica Otorgante:</strong></font></td>
            <td><input name="nombre_empresa" id="nombre_empresa" type="text" size="65" maxlength="65"  onblur="javascript:this.value=this.value.toUpperCase();"/></td>
        </tr> 
        
                <tr>
          	<td align="right"><strong><u>Direcci�n Fiscal del Local:</u></strong></td>
        </tr>
        <tr>
        	<td align="right"><strong>Parroquia:</strong></td>
            <td><select name="parroquia" id="parroquia">
            		<script>llenar_select_parroquia();</script>
                </select>
                <input type="hidden" name="nombre_parroquia" id="nombre_parroquia"/>
            </td>
        </tr>
							<script type="text/javascript">
                            	$("#parroquia").val(<?php echo $parroquia_id ?>);
                            </script>                                                    
        <tr>
            <td align="right"><strong>Calle:</strong></td>
            <td><input name="calle" id="calle" type="text" size="70" maxlength="50" onchange="javascript:this.value=this.value.toUpperCase();" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
        </tr>
        <tr>
           <td align="right"><strong>Av:</strong></td>
               <td><input name="avenida" id="avenida" type="text" size="70" maxlength="50" onchange="javascript:this.value=this.value.toUpperCase();" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
         </tr>
         <tr>
         	<td align="right"><strong>Urb:</strong></td>
            <td><input name="urb" id="urb" type="text" size="70" maxlength="50"  onchange="javascript:this.value=this.value.toUpperCase();" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
          </tr>
          <tr>
          	<td align="right"><strong>Esq:</strong></td>
            <td><input name="esq" id="esq" type="text" size="70" maxlength="50" onchange="javascript:this.value=this.value.toUpperCase();" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
          </tr>
          <tr>
          	<td align="right"><strong>Edif:</strong></td>
            <td><input name="edif" id="edif" type="text" size="70"  maxlength="50" onchange="javascript:this.value=this.value.toUpperCase();" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
          </tr>
          <tr>
          	<td align="right"><strong>Casa:</strong></td>
            <td><input name="casa" id="casa" type="text" size="70"  maxlength="50" onchange="javascript:this.value=this.value.toUpperCase();" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
          </tr>
          <tr>
            <td align="right"><strong>C.C:</strong></td>
            <td><input name="cc" id="cc" type="text"  size="70" maxlength="50" onchange="javascript:this.value=this.value.toUpperCase();" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
         </tr>
         <tr>
         	<td align="right"><strong>Galpon:</strong></td>
            <td><input name="galpon" id="galpon" type="text" size="70" maxlength="50"  onchange="javascript:this.value=this.value.toUpperCase();" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
         </tr>
         <tr>
         	<td align="right"><strong>Local:</strong></td>
            <td><input name="local" id="local" type="text"  size="70" maxlength="50" onchange="javascript:this.value=this.value.toUpperCase();" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
         </tr>
         <tr>
        	<td align="right"><strong>Cedula accionista:</strong></td>
        	<td><select name="nacionalidad_accionista" id="nacionalidad_accionista">
            		<option value="" selected ></option>
            		<option value="V" >V</option>
            		<option value="E" >E</option>
                     <option value="J" >J</option>
            	</select>
            <input type="text" name="ci_accionista" id="ci_accionista" size="20" maxlength="13" onkeypress="return soloNumeros2(event)" /> <div id="img" style="display:inline;"></div>(sin puntos)
        	</td>
        </tr>
         <tr>
        	<td align="right" title="Colocar al accionista que posea mayor cantidad de acciones, y en caso de que todos posean la misma cantidad de acciones colocar el primero que aparezca en el documento." style="cursor:help;"><img src="images/help.png" /><strong>Nombre Accionista</strong></td>
        	<td id="nombre_accionista">
        	<div id="content_name_accionista" style="display:inline;">
            	<input type="text" name="name1" id="name1" placeholder="Nombre1" style="width:100px;" required/>
        		<input type="text" name="name2" placeholder="Nombre2" style="width:100px;"/>
            	<input type="text" name="apellido1" id="apellido1" placeholder="Apellido1" style="width:100px;" required/>
            	<input type="text" name="apellido2" placeholder="Apellido2" style="width:100px;"/>
            </div>
           <div id="content_razon_social" style="display:none">
            	<input type="text" name="razon_social" id="razonsocial" placeholder="Ingrese Razon Social" style="width:99%;" required/>
            </div>
        	</td>
        </tr>
        
        <tr>
            <td  align="right"width="216"><strong>Tipo de Acto:</strong></td>
            <td><input name="tipo_acto" id="tipo_acto" type="hidden" value="<?php echo $tipo_acto ?>" /><?php echo $tipo_acto ?></td>
        </tr>
        <tr>
            <td  align="right"width="216"><strong>Fecha:</strong></td>
            <td><strong><?php echo $fecha ?></strong><input name="fecha" type="hidden" value="<?php echo $fecha ?>" />
            	<input name="lit" type="hidden" value="<?php echo $numeral ?>" />
            </td>
        </tr>
    </table>
    <br />
    <table align="center" id="consulta" class="tablas" border="0">
    	<tr align="center">
        	<td  align="right" width="215"><strong>Introducir N� de Folios:</strong></td>
            <td align="left" width="245"><input name="i_folios" type="text" size="20" maxlength="20" onkeypress="return soloEnteros(event)" /></td>
        </tr>        
        <tr>
        	<td height="25" colspan="2" align="center"><input name="boton" type="button" value="    Enviar    " onclick="submit_form()" /></td>
        </tr>
    </table>
    </form>
</div>
<?php 
if (isset($_SESSION["registro_id"])){
	$registro_id=$_SESSION["registro_id"];
	?>
    <script type="text/javascript">$("#reg_id option[value="+ <?php echo $registro_id ?> +"]").attr("selected",true);</script>
    <?php 
}
?>
</body>
</html>
