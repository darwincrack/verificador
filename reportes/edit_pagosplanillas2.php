<?php
header('Content-Type: text/html; charset=UTF-8');  
include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');
$planilla_id=0;
echo "<html>
<head>
<title>Editar Pagos Planillas</title>
<link rel='stylesheet' href='../styles/jquery-ui.css' />
<link href='../styles/contenido.css' rel='stylesheet' type='text/css' />
</head>
<body onload=loadurl('menu.php','menu')>
<span id='menu'></span>
<br />";

echo "<div class='contenedor' id='consulta'>
    <form id='f_monto' name='f_monto' method='get' >
        <table width='57%' border='0' align='center' cellpadding='1' cellspacing='1' class='' style='border: 1px solid; border-radius: 10px; border-color:#808080;'>
          <tr>
          <td colspan='3' style='
    border: 1px solid;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    border-color: gray;
    background: gray;
    text-align: center;
    font-weight: bold;'>Buscar por Número de Planilla </td></tr>
	 <tr>
          <td width='43%' style='font-size:12px; padding-top:10px;'><h2>Introducir Nº de Planilla:</h2></td>
          <td width='38%' align='center' >
          <select name='tplid' id='tplid'>";	 

$mysqli = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_errno())
{
	printf('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. %s\n', mysqli_connect_error());
   	exit();
}
$result = $mysqli->query("CALL list_tipo_planilla()");
if(!$result) die("CALL failed: (" . $mysqli->errno . ") " . $mysqli->error);

if($result->num_rows > 0) 
{
	while($rows[]=$row = mysqli_fetch_array($result, MYSQLI_ASSOC))
	{
		if (isset($_GET['tplid'])){
			if($row["tipo_planilla_id"]==$_GET["tplid"])
			{
				$selected='selected=selected';
			}
			else
			{
			$selected='';
			}
		}
		 echo "<option value='".$row["tipo_planilla_id"]."'".$selected.">".$row["descripcion"]."</option>";
	}    
}		
else 
{
    echo"X";
}
if (isset($_GET['planilla'])) $num_planilla=$_GET['planilla'];
echo"</select>
            <input name='planilla' type='text' size='10' maxlength='7' value='$num_planilla'/></td>
            <td width='19%'><input type='submit' value='  Enviar  ' class='boton'/></td>
          </tr>
        </table>
    </form>
	</div>";

if (isset($_GET["tplid"]) && isset($_GET["planilla"]))
{
	
	$num_planilla=texto_limpio($_GET["planilla"]);
	$tipo_planilla_id =texto_limpio($_GET["tplid"]);	
	
	$mysqli = new mysqli($host,$user,$clave,$db,$puerto);
	if (mysqli_connect_errno())
	{
		printf('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. %s\n', mysqli_connect_error());
   		exit();
	}	

	$result = $mysqli->query("CALL list_pagos_planilla('$num_planilla','$tipo_planilla_id')");
	if(!$result) die("CALL failed: (" . $mysqli->errno . ") " . $mysqli->error);	

	if($result->num_rows > 0) 
	{
		
		  echo"<div class='contenedor2'  id='content_info_depo' style='width:90%'> 
		  <table class='table' id='table_radius'>
                   		<thead>
                        <tr style='font-size:10px;'>
                            <th width='4%' style='border-top-left-radius:10px;'>N&deg;</th>
                            <th ondblclick='pid()'>Num. Planilla</a></th>
							<th >Monto. Planilla</th>
                            <th>Registro</th>
                            <th >Tipo de Pago</th>
                            <th >Banco</th>
                            <th >Numero de Cuenta</th>
                            <th >Fecha Deposito</th>
                            <th >Num. Deposito</th>
                            <th >Monto</th>
                            <th >Observaciones</th>
                            <th style='border-top-right-radius:10px;'>Editar</th>                                
                        </tr> </thead><tbody id='tb_deposito'>";
		$cont=1;
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			
			$id_deposito=encrypt($row["id_deposito"],'123');
			$planilla_id=$row["planilla_id"];
			 echo "<tr>
			<td>".$cont++."</>
			<td>".$row["num_planilla"]."</td>
			<td id='montop_$cont'>".bsf($row["monto_planilla"])."</td>
			<td>".$row["descripcion_registro"]."</td>
			<td id='tipo_$cont'>".$row["descripcion_pago"]."</td>
			<td id='banco_$cont'>".$row["banco_nombre"]."</td>
			<td id='num_cuenta_$cont' width='18%'>".$row["NumCuenta"]."</td>
			<td id='fecha_depo_$cont' width='10%'>".$row["fecha_depo"]."</td>
			<td id='depo_$cont'>".$row["num_deposito"]."</td>
			<td id='monto_$cont'>".bsf($row["monto"])."</td>
			<td>".str_replace('|','<br><hr>',$row["observaciones"])."</td>";?>
			<td><input name="" type="button" value="Editar" onclick="abrir_dialog(<?php echo $cont.','.$row["banco_id"].','.$row["cuentas_id"].',\''.$id_deposito.'\'';?>)" /></td>
 		</tr> 
		<?php }echo"</tbody>
                </table>
            </div>";
	}
	else 
	{
    	echo"<div class='contenedor' align='center'>
                <table width='420' class='table_radius'>
                  	<tr><td width='396' align='center' style='border-top-left-radius:10px; color:#F00; border-top-right-radius:10px;'>  NO SE ENCONTRARON REGISTROS PARA SU SOLICITUD  </td></tr>
                </table>
				</div>";
	}
	echo "</div>";
 /* liberar el resultset */
$result->free();
/* cerrar la conexión */
$mysqli->close();
}

////diseño de la ventana de dialogo
echo "<input type='hidden' value='".$planilla_id."' id='pid' />
<div id='dialog' title='Basic dialog' style='display:none'>
<div id='form_edit_depo'>
<table class='table' style='margin-top:15px;'>
<tr style='font-weight:bold;font-size:12px;' >
<th style='border-top-left-radius:10px;'>Banco</th>
<th>Cuenta</th>
<th>Fecha Deposito</th>
<th>N° Deposito</th>
<th>Monto</th>
<th>Motivo</th>
<th style='border-top-right-radius:10px;'>&nbsp;</th>
</tr>
<tr style='font-size:10px;'>
<td><select name='sel_banco' id='sel_banco' onchange='select_banco(this.value)' onkeyup='select_banco(this.value)'>
<option value='0' selected='selected'></option>";

$mysqli = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_errno())
{
	printf('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. %s\n', mysqli_connect_error());
   	exit();
}
$result = $mysqli->query("CALL list_banco()");
if(!$result) die("CALL failed: (" . $mysqli->errno . ") " . $mysqli->error);

if($result->num_rows > 0) 
{
	while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
	{
		 echo "<option value='".$row["banco_id"]."'>".$row["Nombre"]."</option>";
	}
}
else 
{
    echo"X";
}
 /* liberar el resultset */
$result->free();
/* cerrar la conexión */
$mysqli->close();
echo "</select></td>

<td><div id='cuentas'>1</div></td>
<td><input type='text' readonly='true' id='datepicker' name='datepicker' style='cursor:pointer; width:100px;'></td>
<td width='10%'><input name='num_deposito' id='num_deposito' type='text' size='13' maxlength='10' onkeypress='return soloEnteros(event)' style='width:80px;'/></td>
<td><input name='monto_deposito' id='monto_deposito' type='text' size='20' maxlength='10' onkeypress='return soloNumeros2(event)' style='width:60px;' /></td>
<td><input name='motivo' id='motivo' type='text' size='20' maxlength='2000' style=' width:200px;'/></td>
<td><input name='enviar' type='button' onclick='get_form_depositos();' value='Guardar' /></td>
</tr>
</table>
<center><div id='existe_depo' style=' font-size:8px;'></div></center>
<input type='hidden' name='id_depo' id='id_depo' value=''>
</div>
</div>
</body>
<script type='text/javascript' src='../funciones/ajax.js'></script>
<script type='text/javascript' src='../funciones/funciones1.js'></script>
<script type='text/javascript' src='../funciones/jquery-1.7.1.min.js'></script>
<script type='text/javascript' src='../funciones/jquery-ui.js'></script>
<script type='text/javascript' src='../funciones/jquery.ui.datepicker-es.js'></script>
<script type='text/javascript' src='../funciones/update_pagos/update_pagos2.js'></script>
</html>";

