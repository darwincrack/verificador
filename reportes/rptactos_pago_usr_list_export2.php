<?php 
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=rptactos_pago_usr_list_export.xls");
header("Pragma: no-cache");
header("Expires: 0");
include('../seguridad_trans.php'); 
include('../conex.php');
include('../funciones/funcion.php');
$fecha_desde =$_POST["fecha_desde"];
$fecha_hasta=$_POST["fecha_hasta"];
$usuario=$_POST["usuario"];
$registro_id=$_POST["registro_id"];
$tipo_acto=$_POST["tipo_acto"];
$con = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_error()) {
    die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}
$stmt = $con->stmt_init();
$stmt->prepare('call rpt_tipo_acto_con_pagos_usr_list(?,?,?,?,?)');
		$stmt->bind_param('sssis',$fecha_desde,$fecha_hasta,$usuario,$registro_id,$tipo_acto);
if(!$stmt->execute()){
	throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
}else{
	$stmt->store_result(); //Sin esta línea no podemos obtener el total de resultados anticipadamente
	$cuantos_registros = $stmt->num_rows;
	if($cuantos_registros>0){
		$stmt->bind_result($planilla_id,$tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$fecha,$registro,$nombre_presentante,$ci_presentante,$nombre_empresa,$rif,$tipo_acto,$total);
		$excel= "usuario: ".$usuario."\n";
		$excel .= "NUM. DE PLANILLA\tFECHA EN PLANILLA\tREGISTRO\tCEDULA O RIF(OTORGANTE)\tPERSONA OTORGANTE(NATURAL O JURIDICA)\tCEDULA PRESENTANTE\tNOMBRE DEL PRESENTANTE\tCLASE DE ACTO\tTOTAL PLANILLA\n";
		while($stmt->fetch()){
			$excel .= "$tipo_planilla_desc".'-'."$num_planilla\t$fecha\t$registro\t$rif\t$nombre_empresa\t$ci_presentante\t$nombre_empresa\t$tipo_acto\t".trim(bsf($total))."\n";

		}
		$excel = str_replace("\"", "", $excel);
		print $excel;
	}
}
$stmt->free_result();
$stmt->close();
while($con->next_result()) { }
$con->close(); ?>