<?php
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=Total_Registros.xls");
header("Pragma: no-cache");
header("Expires: 0");
include('./seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');

$fecha_desde =$_POST["desde"];
$fecha_hasta=$_POST["hasta"];

$con = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_error()) {
    die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}
$stmt = $con->stmt_init();
$stmt->prepare('call rpt_pago_planilla2(?,?)');
$stmt->bind_param('ss',$fecha_desde,$fecha_hasta);
if(!$stmt->execute()){
	throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
}else{
	$stmt->store_result(); //Sin esta línea no podemos obtener el total de resultados anticipadamente
	$cuantos_registros = $stmt->num_rows;
	if($cuantos_registros>0){
		$stmt->bind_result($tipo,$tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$total_planilla,$deposito_id,$num_deposito,$banco,$deposito_monto,$num_aprob,$pto_venta_monto,$timbre_monto,$creadopor,$fecha_creado,$ip);
		$excel = "TIPO DE INFO\tNUM. DE PLANILLA\tTOTAL EN PLANILLA\tNUM. DE DEPOSITO\tBANCO\tMONTO DEL DEPOSITO\tAPROBACION DE TRANSACCION\tMONTO DE TRANSACCION\tMONTO TIMBRE\tREGISTRADO POR\tFECHA DE OPERACION\tIP\n\n";	
		while($stmt->fetch()){
			$excel .= "$tipo\t$tipo_planilla_desc-$num_planilla\t".bsf($total_planilla)."\t$num_deposito\t".trim(ConHtml($banco))."\t"
						.trim(bsf($deposito_monto))."\t$num_aprob\t".trim(bsf($pto_venta_monto))."\t"
						.trim(bsf($timbre_monto))."\t$creadopor\t$fecha_creado\t$ip\n";
		}
		$excel = str_replace("\"", "", $excel);
		print $excel;
	}
}
$stmt->free_result();
$stmt->close();
while($con->next_result()) { }
$con->close();

?>