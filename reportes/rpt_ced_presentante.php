<?php include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');

	if (isset($_GET["numero_cedula"])){
		 $numero_cedula = $_GET["numero_cedula"];
	}else{
		$numero_cedula = '';
	}

 if(!function_exists('str_ireplace')) {
    function str_ireplace($search,$replace,$subject) {
    $search = preg_quote($search, "/");
    return preg_replace("/".$search."/i", $replace, $subject); } }
    
    function resaltar($palabra, $texto) {
        $aux=$reemp=str_ireplace($palabra,'%s',$texto);
        $veces=substr_count($reemp,'%s');
        if($veces==0)return $texto;
        $palabras_originales=array();
        for($i=0;$i<$veces;$i++){
              $palabras_originales[]='<span class="highlight">'.substr($texto,strpos($aux,'%s'),strlen($palabra)).'</span>';
              $aux=substr($aux,0,strpos($aux,'%s')).$palabra.substr($aux,strlen(substr($aux,0,strpos($aux,'%s')))+2);
        }
        return vsprintf($reemp,$palabras_originales);
  }  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript" src="../funciones/funciones1.js"></script>
<script type="text/javascript" src="../jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../funciones/jquery.highlight-3.js"></script>
<link href="../styles/estilo.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
	function ColorUno(src,color_entrada) {
		src.style.backgroundColor=color_entrada;
	} 
	function ColorDos(src,color_default) { 
		src.style.backgroundColor=color_default;
		//src.style.cursor="default"; 
	}
//*********************************************************************************
// Function que valida que un campo contenga un string y no solamente un " "
// Es tipico que al validar un string se diga
//    if(campo == "") ? alert(Error)
// Si el campo contiene " " entonces la validacion anterior no funciona
//*********************************************************************************

//busca caracteres que no sean espacio en blanco en una cadena
function vacio(q) {
        for ( i = 0; i < q.length; i++ ) {
                if ( q.charAt(i) != " " ) {
                        return true
                }
        }
        return false
}

//valida que el campo no este vacio y no tenga solo espacios en blanco
function valida(F) {
        var contador;
		/*f_nombre = document.getElementById('numero').value;
		alert(f_nombre);*/
		
		/*document.getElementById('consulta').submit()*/
        if( vacio(F.numero_cedula.value) == false ) {
                alert("Introduzca texto.")
                return false
        } else {              
			   return true 
        }   
}
</script>
</head>
<body onload="loadurl('menu.php','menu')">
<?php $usuario = $_SESSION["usuario"];?>
<span id="menu"></span>
<div class="contenedor">
<br>
<br>
<h1>Buscar Presentante por Cedula de Identidad </h1>
    <FORM NAME="f_providencia" METHOD="get" id="consulta"  onSubmit="return valida(this);">
        <table width="47%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas3" >
          <tr>
            <td align="center"><a>Cedula Presentante:</a></td>                        
            <td  align="center"><a><input  type="text" id="numero_cedula" name="numero_cedula" onkeypress="return soloNumeros2(event)" value="<?php echo $numero_cedula ?>"></a></td>
          </tr>
          <tr>
            <td colspan="4" align="center"><input name="buscar2" type="submit" value="Buscar" class="boton_grande" /></td>
          </tr>
        </table><br>
  </form>
</div>
<?php
	if (isset($_GET["numero_cedula"])){
		 $numero_cedula = $_GET["numero_cedula"];
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call rpt_ci_presentante(?)');
		$stmt->bind_param('i',$numero_cedula);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result();
			$cuantos_registros = $stmt->num_rows;
			if($cuantos_registros>0){
						$stmt->bind_result($tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$fecha,$descripcion,$estado,$nombre,$ci_presentante,
						$empresa,$rif,$numeral,$tipo_acto,$ut,$capital,$monto,$porcent,$folios,$tasas,$total,$nombre,$num_deposito,	$fecha_deposito,$monto_1,$usuario,$fecha,$ip,$tipo_pago,$name_accionista,$direccion_local);	
				
				?><div class="contenedor2" align="left">                
                  <table id="t_deposito"  border="1" cellpadding="0" cellspacing="0" align="left">
                    <tbody id="tb_deposito">
                        <tr bgcolor="#9999FF">
                            <td width="4%">N�</td>
                            <td >Num Planilla</td>
                            <td >Fecha</td>
                            <td >Descripcion</td>
                            <td >Estado</td>                            
                            <td >Nombre</td>
                            <td >CI presentante</td>
                            <td >Empresa</td>
                            <td>Rif</td>
							<td>Direccion Local</td>
                            <td>Accionista</td>
                            <td>Numeral</td>
                            <td >Tipo de Acto</td>
                            <td >UT</td>
                            <td >Capital</td>
                            <td >Monto</td> 
                            <td>porcentaje</td>
                            <td>Folios</td>
                            <td>Tasas</td>
                            <td >Total</td>
                            <td >Nombre</td>
                            <td>Tipo Operacion</td>
                            <td >Num de Operacion</td>
                            <td >Fecha de Operacion</td>
                            <td >Monto (Operacion)</td>
                            <td >Usuario</td>
                            <td >Fecha</td>
                            <td >IP</td>                                                                                 
                        </tr><?php
						
				$cont=0;
				$cco=0;
				while($stmt->fetch()){
					$cco++; 
					$cont++;

					if(($cco%2)==0){ 
						$color = '#FFFFFF'; 
					}else{
						$color = '#E6F2E1'; 
					} ?>
					<tr onmouseover="ColorUno(this,'#CCCCCC');" onmouseout="ColorDos(this,'<?php echo $color; ?>')" bgcolor="<?php echo $color; ?>">
                    		<td><?php echo $cont ?></td>
                            <td ><a href="../redirect2.php?p=2&planilla=<?php echo $num_planilla ?>&tplid=<?php echo $tipo_planilla_id ?>&adm=1"><?php echo $tipo_planilla_desc.'-'.$num_planilla ?></a></td>
                            <td ><?php echo $fecha ?></td>
                           <!-- <td ><a href="../redirect2.php?p=2&planilla=<?php //echo $deposito_id ?>&adm=1"><?php //echo $num_deposito ?></a></td>-->                            
                            <td ><?php echo $descripcion ?></td>
                            <td ><?php echo $estado ?></td>
                            <td><?php echo $nombre ?></span></td>                            
                            <td><?php echo $ci_presentante ?></td>                                                        
                            <!--<td >dfdf  <span class="highlight"></span>   mas info</td>--> 
                            <td><?php echo $empresa?></td> 
                            <td ><?php echo $rif ?></td>
							<td ><?php echo $direccion_local ?></td>
                            <td ><?php echo  $name_accionista?></td> 
                            <td ><?php echo $numeral ?></td>                            
                            <td ><?php echo $tipo_acto ?></td>
                            <td ><?php echo $ut ?></td>
                            <td ><?php echo $capital ?></td>
                            <td ><?php echo $monto ?></td>
                            <td ><?php echo $porcent ?></td>
                            <td ><?php echo $folios ?></td>
                            <td ><?php echo $tasas ?></td>
                            <td ><?php echo $total ?></td>
                             <td ><?php echo $nombre ?></td> 
                             <td><?php echo $tipo_pago ?></td>
                             <td ><?php echo $num_deposito ?></td>
                             <td ><?php echo $fecha_deposito ?></td>
                            <td ><?php echo $monto_1 ?></td>
                            <td ><?php echo $usuario ?></td>
                            <td ><?php echo $fecha ?></td>
                             <td ><?php echo $ip ?></td>
                                                                                      
                                                        
					</tr><?php
											
				}?>
                    <tr><td colspan="25">FIN DEL LISTADO</td></tr>     
                 </tbody>
                
                </table><br /><br />
                
          
				</div>
				<?php
			}else{
				?><div class="contenedor" align="center">
                <table width="420" border="1" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                </table>
				</div><?php
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
		$con->close();
	}?>


</body>
</html>
