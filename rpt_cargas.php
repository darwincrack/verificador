<?php include('seguridad_trans.php'); 
include('conex.php');
include('funciones/funcion.php');
if (isset($_GET["fechadesde"]) && isset($_GET["fechahasta"])){
	$desde =$_GET["fechadesde"];
	$hasta=$_GET["fechahasta"];
}else{
	$desde ="";
	$hasta="";
}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="funciones/ajax.js"></script>
<script type="text/javascript" src="funciones/calendarDateInput.js"></script>
<script type="text/javascript">
function ColorUno(src,color_entrada) {
	src.style.backgroundColor=color_entrada;
} 
function ColorDos(src,color_default) { 
    src.style.backgroundColor=color_default;
	//src.style.cursor="default"; 
}
function enviar_form(){
		cadena=document.getElementById('contiene_table').innerHTML;
		document.env_tabla.tabla_html.value = cadena;
}

</script>

</head>
<body onload="javascript:loadurl('menu2.php','menu');">
<?php $usuario=$_SESSION["usuario"]; ?>
<span id="menu"></span>

<div class="contenedor">
<br>
<br>
	<h1>Mis Cargas (Rango de Fechas)</h1>
    <form  action="" method="get" id="consulta" name="f_providencia">
        <table width="71%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas3" >
          <tr>
            <td align="center"><a>Fecha Desde:</a></td>
            <td  ><script>DateInput('fechadesde',true)</script></td>
            <td  align="center"><a>Fecha Hasta:</a></td>
            <td ><script>DateInput('fechahasta',true)</script></td>
          </tr><?php
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechadesde"],8,2);
                $mes=substr($_GET["fechadesde"],5,2)-1;
                $a�o=substr($_GET["fechadesde"],0,4);
                echo "<script>fechadesde_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechahasta"],8,2);
                $mes=substr($_GET["fechahasta"],5,2)-1;
                $a�o=substr($_GET["fechahasta"],0,4);
                echo "<script>fechahasta_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }?>
          <tr>
            <td colspan="4" align="center"><input name="buscar" type="submit" value="Buscar" class="boton_grande" /></td>
          </tr>
        </table>
    </form><br  /><?php
	if (isset($_GET["fechadesde"]) || isset($_GET["fechahasta"])){
		$fecha_desde = $_GET["fechadesde"];
		$fecha_hasta = $_GET["fechahasta"];
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call rpt_cargas(?,?,?)');
		$stmt->bind_param('sss',$fecha_desde,$fecha_hasta,$usuario);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result();
			$cuantos_registros = $stmt->num_rows;
			if($cuantos_registros>0){
				$stmt->bind_result($tipo,$tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$total_planilla,$deposito_id,$num_deposito,$banco,$deposito_monto,$num_aprob,$pto_venta_monto,$timbre_monto,$fecha_creado);
				
				?><div class="contenedor2" id="contiene_table">   
                <table id="t_deposito"  border="1" cellpadding="0" cellspacing="0">
                	<thead bgcolor="#9999FF">
                    	<tr><td colspan="11">USUARIO: <?php echo $usuario ?></td></tr>
                        <tr bgcolor="#9999FF">
                            <td width="4%"></td>
                            <td width="12%">Tipo de Info</td>
                            <td >Num. Planilla</td>
                            <td >Total en Planilla</td>
                            <td >Num. Deposito</td>
                            <td >Banco</td>
                            <td >Monto Deposito</td>
                            <td >Num. Aprob</td>
                            <td >Monto de Transaccion</td>
                            <td >Monto por Timbre</td>      
                            <td width="15%">Fecha Operacion</td>
                        </tr></thead><tbody id="tb_deposito"><?php
				$cont=0;
				$cco=0;
				while($stmt->fetch()){
					$cco++; 
					$cont++;
					if(($cco%2)==0){ 
						$color = '#FFFFFF'; 
					}else{
						$color = '#E6F2E1'; 
					}?>
					<tr onmouseover="ColorUno(this,'#CCCCCC');" onmouseout="ColorDos(this,'<?php echo $color; ?>')" bgcolor="<?php echo $color; ?>">
                    		<td><?php echo $cont ?></td>
                            <td><?php echo $tipo ?></td>
                            <td ><a href="redirect2.php?p=2&planilla=<?php echo $num_planilla ?>&tplid=<?php echo $tipo_planilla_id ?>&adm=0"><?php echo $tipo_planilla_desc.'-'.$num_planilla ?></a></td>
                            <td ><?php echo bsf($total_planilla) ?></td>
                           <!-- <td ><a href="../redirect2.php?p=2&planilla=<?php //echo $deposito_id ?>&adm=1"><?php //echo $num_deposito ?></a></td>-->
                            <td><?php echo $num_deposito ?></td>                                                        
                            <td ><?php echo $banco ?></td>
                            <td ><?php echo bsf($deposito_monto) ?></td>
                            <td ><?php echo $num_aprob ?></td>
                            <td ><?php echo bsf($pto_venta_monto) ?></td>
                            <td ><?php echo bsf($timbre_monto) ?></td>                       
                            <td ><?php echo $fecha_creado ?></td>
                                     
                             
                             
					</tr><?php
				}?></tbody>
                </table>
				</div>
				<div class="contenedor2">
			<br />
            <table>
            	<tr>
                	<td>            
                        <form action="rpt_cargas2_export.php" method = "POST" name"f_exportt" >
                            <input type="hidden" name="desde" value="<?php echo $desde ?>" />
                            <input type="hidden" name="hasta" value="<?php echo $hasta ?>" />
                            <input type="hidden" name="usuario" value="<?php echo $usuario ?>" />
                            <input type = "submit" value = "Exportar a Excel" />
                        </form>
                    </td>
                    <td>                        
                    <td>
                        <form action="rpt_cargas_export.php"  method="post" id="env_tabla" name="env_tabla" onsubmit="return enviar_form()">
                        	<input type="hidden" name="tabla_html" id="tabla_html" value="">
                            <input type="hidden" name="f_nombre" value="Detalles_Registros" />
                            <input type = "submit" value = "Exportar a Excel # 2"  />
                        </form>
                    </td>
                </tr>
            </table>                                     
				</div><?php
			}else{
				?><table width="420" border="1" align="center" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                  </table><?php
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
	}?>
</div>
</body>
</html>