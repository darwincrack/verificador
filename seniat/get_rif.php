<?php
/*
code_result:
-1: no hay soporte a curl
0: no hay conexion a internet
1: existe rif consultado
otherwise:
450:formato de rif invalido
452:rif no existe
seniat:
    nombre:[CADENA CON EL NOMBRE]
    agenteretensioniva:[SI|NO]
    contribuyenteiva:[SI|NO]
    tasa:[VACIO|ENTERO MONTO TASA]
*/

$rif=$_GET['rif'];
$response_json=array('code_result'=>'', 'seniat'=>array());
if(function_exists('curl_init')){ // Comprobamos si hay soporte para cURL
       //$url="http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=".$_POST['pref_rif'].$_POST['rif_prove'];
	   $url="http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=$rif";
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_TIMEOUT, 12);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
       $resultado = curl_exec ($ch);
       if($resultado){
               try{
                       if(substr($resultado,0,1)!='<')
                                throw new Exception($resultado);
                       $xml = simplexml_load_string($resultado);
                       if(!is_bool($xml)){
                               $elements=$xml->children('rif');
                               $seniat=array();
                               $response_json['code_result']=1;
                               foreach($elements as $indice => $node){
                                       $index=strtolower($node->getName());
                                       $seniat[$index]=(string)$node;
                               }
                               $response_json['seniat']=$seniat;
                       }
               }catch(Exception $e){
                       $result=explode(' ', $resultado, 2);
                       $response_json['code_result']=(int) $result[0];
               }
       }else
               $response_json['code_result']=0;//No hay conexion a internet
}else
       $response_json['code_result']=-1;//No hay soporte a curl_php
echo json_encode($response_json);
?>