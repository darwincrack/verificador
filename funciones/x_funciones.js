function rndDecimal(valor,cant_decimales){
	var valorNdecimales = null;
	valorNdecimales=Math.round(valor*cant_decimales*10) / (cant_decimales*10); 
	return valorNdecimales.toFixed(2) ;
}
function Mostrar(Obj){
	document.getElementById(Obj).style.display='block';
}
function Ocultar(Obj){
	document.getElementById(Obj).style.display='none';
}	
function MostrarTabla(obj_id){
	document.getElementById(obj_id).style.display='table';
}

function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}
 
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++){
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1){
         IsNumber = false;
      }
   }
   return IsNumber;
}
function IsNumericInt(sText)
{ //funcion identica a la anterior pero solo para numero enteros
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++){
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1){
         IsNumber = false;
      }
   }
   return IsNumber;
}

function soloNumeros(evt){
//asignamos el valor de la tecla a keynum
	if(window.event){// IE
		keynum = evt.keyCode;
	}else{
		keynum = evt.which;
	}
	//comprobamos si se encuentra en el rango
	if((keynum>47 && keynum<58) || keynum == 8 || keynum==46 || keynum ==0){  //46 es el punto, 47 es el simbolo de dividir
		return true;
	}else{
		return false;
	}
}

function soloEnteros(evt){ //para retornar solo numeros enteros
//asignamos el valor de la tecla a keynum
	if(window.event){// IE
		keynum = evt.keyCode;
	}else{
		keynum = evt.which;
	}
	//alert (keynum);
	//comprobamos si se encuentra en el rango
	if((keynum>47 && keynum<58) || keynum == 8 || keynum ==0){
		return true;
	}else{
		return false;
	}
}


//hecho por darwin 28/11/2012
function soloNumeros2(evt){
//asignamos el valor de la tecla a keynum
	if(window.event){// IE
		keynum = evt.keyCode;
	}else{
		keynum = evt.which;
	}
	//comprobamos si se encuentra en el rango
	if((keynum>47 && keynum<58) || keynum == 8  || keynum ==0){  //46 es el punto, 47 es el simbolo de dividir
		return true;
	}else{
		return false;
	}
}