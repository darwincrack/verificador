<?php include ('funciones/funcion.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="funciones/funciones.js"></script>
<script type="text/javascript">
function submit_form() {
		var monto = document.f_monto.i_monto.value;
		var folios = document.f_monto.i_folios.value
		if(IsNumeric(monto)) {
			if (IsNumericInt(folios)){
				document.f_monto.submit();
			}else{
				alert ('INTRODUCIR NUMERO CORRECTO DE FOLIOS');
				return false;
			}
		}else{
			alert('DEBE INTRODUCIR NUMEROS');
			return false;			
	 	}
}
</script>
</head>
<body>
<div class="contenedor" id="consulta">
	<h1>Calculo de Actos Realizados Art. 13 de La Ley de Timbre Fiscal</h1>
    <h2>Numeral 3</h2>
    <h2>Inscripcion/Modificacion de Actos Constitutivos </h2>
    <form action="lit_5.php" method="get"  name="f_monto">
    <table align="center" id="consulta" class="tablas" border="0">
    	<tr align="center">
        	<td  align="right" width="215"><strong>Introducir Monto:</strong></td>
            <td align="left" width="245"><input name="i_monto" type="text" size="20" maxlength="20" onkeypress="return soloNumeros(event)" /></td>
        </tr>
    	<tr align="center">
        	<td  align="right" width="215"><strong>Introducir N� de Folios:</strong></td>
            <td align="left" width="245"><input name="i_folios" type="text" size="20" maxlength="20" onkeypress="return soloEnterosEnt(event)" /></td>
        </tr>        
        <tr>
        	<td align="center" colspan="2"><input name="boton" type="button" value="    Enviar    " onclick="submit_form()" /></td>
        </tr>
    </table>
    </form>
</div>
</body>
</html>
