<?php include('seguridad_trans.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reportes</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />


<script type="text/javascript">
function attach_file( p_script_url ) {
		// create new script element, set its relative URL, and load it
		script = document.createElement( 'script' );
		script.src = p_script_url;
		document.getElementsByTagName( 'head' )[0].appendChild( script );
}
</script>
</head>


<body onload="javascript:attach_file('menu.php');">

<?php $usuario=$_SESSION["usuario"]; ?>
<span id="menu"></span>

<br>
<div class="contenedor" align="center">
<h1>Reportes</h1>
<ul>
<li><a href ="rptdeposito.php">Buscar por numero de deposito</a></li>
<li><a href ="rptempresa.php">Buscar Empresa</a></li>
<li><a href ="rptplanilla.php">Buscar Planillas</a></li>
<li><a href ="rpt_cargas.php">Mis Cargas</a></li>
<li><a  href="reportes/rptactos_pago_usr.php">Cantidad de Actos Tramitados (con pagos asociados)</a></li>
<li><a  href="reportes/rpt_planilla_pago_validado.php">Listado de Planillas con Pago Validado</a></li>
</ul>
</div>
</body>
</html>