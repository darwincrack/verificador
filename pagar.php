<?php
include('seguridad_trans.php');
include('conex.php');
include('funciones/funcion.php');
$_SESSION['pago_admin']=$admin=($_SESSION['nivel']==1)?(true):(false);
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="pragma" content="no-cache; charset=iso-8859-1" />
<title>Pagar-SATDC</title>
<style>
select {
	font-size:11px;}
</style>
<link rel="stylesheet" href="styles/contenido.css"/>
<link rel="stylesheet" href="styles/jquery-ui.css"/>
</head>
<?php
if (!isset($_GET["plid"])){
	die('ERROR EN PLANILLA ID');
}else{
	$planilla_id=$_GET["plid"];
}
if (!isset($_GET["planilla"])){
	die('ERROR EN NUMERO DE PLANILLA');
}else{
	$planilla=$_GET["planilla"];
}
if (!isset($_GET["total"])){
	die('ERROR EN TOTAL A PAGAR');
}else{
	$total=$_GET["total"];
}
if (!isset($_GET["tplid"])){
	die('ERROR EN TIPO DE PLANILLA');
}else{
	$tipo_planilla_id=$_GET["tplid"];
	$con = new mysqli($host,$user,$clave,$db,$puerto);
	if (mysqli_connect_error()) {
		die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
	}
	$stmt = $con->stmt_init();
	$stmt->prepare('call sel_tipo_planilla(?)');
	$stmt->bind_param('i', $tipo_planilla_id);
	if(!$stmt->execute()){
		throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
	}else{
		$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
		$cuantos_registros = $stmt->num_rows;
		if($cuantos_registros>0){
			$stmt->bind_result($tipo_planilla_descrip_sp);
				while($stmt->fetch()){	
					$tipo_planilla=$tipo_planilla_descrip_sp;
				}                      	
		}
	}
	$stmt->free_result();
	$stmt->close();
	while($con->next_result()) { }
	$con->close();		
}

if(($admin==true)&&($_SESSION['location_user']!='1')){
	echo '<body onload="javascript:loadurl(\'reportes/menu2.php\',\'menu\');">';
}else{
	echo '<body onload="javascript:loadurl(\'menu2.php\',\'menu\');">';
}?>

<!--link del menu-->
<span id="menu"></span>
<div class="agrupar" style="width:1024px; font-family: verdana; padding:10px;">
	<div class="planillas">
    	<table class="table_radius">
        	<tr>
            <th colspan="2"><span class="titulo_informativo">INFORMACI�N</span></th>
            </tr>
        	<tr>
            	<td style="width:30%;">NUMERO DE PLANILLA</td>
                <td style="text-align:center;"><span class="total_planilla"><?php echo $tipo_planilla.'-'.$planilla ?></span></td>
            </tr>
            <tr class="even">
            	<td>TOTAL A PAGAR</td>
                <td style="text-align:center;"><span class="total_planilla"><?php echo bsf($total).' Bs'?></span></td>
            </tr>
        </table>
	</div>
    <!--inicio tabla depositos-->
    <div class= "depositos">
    <table class="table_radius" style="margin-top:12px;">
<tr><th colspan="10"> DEPOSITOS</th></tr>
<tr style="font-weight:bold;font-size:12px;" >
<td>Tipo de pago</td>
<td>Banco</td>
<td>Cuenta</td>
<td>Fecha Deposito</td>
<td style="cursor:help" title="Numero de Certificacion de Ingresos">Num RM</td>
<td>N&deg; Deposito</td>
<td>Monto</td>

<?php echo ($admin==true)?"<td id='td_lote' style='display:none';>Lote</td>":'';?>
<td>&nbsp;</td>
</tR>
<tr>
<td><select name="sel_tipo" id="sel_tipo" style=" font-size:10px;">
                            <option value="0" selected="selected"></option>
                            <?php $con = new mysqli($host,$user,$clave,$db,$puerto);
                                    if (mysqli_connect_error()) {
                                        die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA');
                                    }
                                    $stmt = $con->stmt_init();
                                    $stmt->prepare('call list_tipo_pago()');
                                    if(!$stmt->execute()){
                                        throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
                                    }else{
                                        $stmt->store_result(); //Sin esta línea no podemos obtener el total de resultados anticipadamente
                                        $cuantos_registros = $stmt->num_rows;
                                        if($cuantos_registros>0){
                                            $stmt->bind_result($id,$descripcion);
                                            while($stmt->fetch()){
                                                echo '<option value="' . $id . '">' . $descripcion . '</option>';
                                            }
                                        }
                                    }
                                    $stmt->free_result();
                                    $stmt->close();
                                    while($con->next_result()) { }
                                    $con->close();?>
    </select></td>
<td><select name="sel_banco" id="sel_banco" style=" font-size:10px;">
                            <option value="0" selected="selected"></option>
                            <?php $con = new mysqli($host,$user,$clave,$db,$puerto);
                                    if (mysqli_connect_error()) {
                                        die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA');
                                    }
                                    $stmt = $con->stmt_init();
                                    $stmt->prepare('call list_banco()');
                                    if(!$stmt->execute()){
                                        throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
                                    }else{
                                        $stmt->store_result(); //Sin esta línea no podemos obtener el total de resultados anticipadamente
                                        $cuantos_registros = $stmt->num_rows;
                                        if($cuantos_registros>0){
                                            $stmt->bind_result($id,$descripcion);
                                            while($stmt->fetch()){
                                                echo '<option value="' . $id . '">' . $descripcion . '</option>';
                                            }
                                        }
                                    }
                                    $stmt->free_result();
                                    $stmt->close();
                                    while($con->next_result()) { }
                                    $con->close();?>
    </select></td>
<td><span id="cuentas"><select><option>--Seleccione--</option></select></span></td>
<td><form id="f_deposito" name="f_deposito"><input type="text" readonly="true" id="datepicker" name="datepicker" style="cursor:pointer; width:100px;"></form></td>
<td><input type="text" name="num_rm" id="num_rm" placeholder="RM-2015-" style=" width:70px;" onkeypress="return soloEnteros(event)"/></td>
<td width="10%"><input name="num_deposito" id="num_deposito" type="text" size="13" maxlength="13" onkeypress="return soloEnteros(event)" onfocus="return max_lenght_depo(event)" style="width:80px;"/></td>
<td><input name="monto_deposito" id="monto_deposito" type="text" size="20" maxlength="20" onkeypress="return soloNumeros2(event)" style="width:60px;" /></td>
<?php echo ($admin==true)?"<td id='td_lote_n' style='display:none';><input name='num_lote_transac' id='num_lote_transac' type='text' maxlength='4' onkeypress='return soloEnteros(event)' style='width:30px;' /></td>":'';?>
<td><input name="enviar" id="boton_agregar" type="button" title="Antes de Agregarse el deposito en la tabla, este sera validado en la Base de Datos del SATDC"  value="Agregar" style="
    border-radius: 5px;
    width: 80px;
    font-weight: bold;
    cursor: pointer;"/></td>

</tr>
</table>  
    </div>
<!--comienzo gen_tablas_depositos-->
<div id="gen_tablas_depositos">
<form id="f_monto" name="f_monto">
   	<!--contiene el textarea observaciones-->
    <div id="observaciones" style="border-radius:5px; background:#FFF; padding: 5px 10px; margin-top: 12px; border: 1px solid #999;">
    	<textarea onkeyup="cuenta_caracteres()" style="width:100%; resize:none; border-radius:5px;" name="text_obser" id= "text_obser" placeholder="Observaciones, Maximo 2000 caracteres" maxlength='2000' ></textarea>
   	<div> <span id="tx_char_rest" style="font-size:11px; margin-left:82%;">Caracteres Restantes 2000</span></div>
	</div>
    <table id="t_deposito" style="display:none; margin-top:10px; text-align:center;" class="table_radius" >
    <tbody id="tb_deposito" style=" font-size:11px;">
        <tr>
            <th ></th>
            <th>NUM RM</th>
            <th >N&deg; DEPOSITO</th>
            <th >BANCO</th>
            <th >CUENTA</th>
            <th >FECHA DEPOSITO</th>
            <th >MONTO</th>
        </tr>
        </tbody>
    </table>
    <br />
    <table id="t_pto_venta" style="display:none; margin-top:10px; margin-bottom:10px; text-align:center;" class="table_radius">
        <tbody id="tb_pto_venta" style=" font-size:11px;">
        <tr>
            <th></th>
            <th>NUM RM</th>
			<?php echo ($admin==true)?'<th>N&deg; LOTE</th>':'';?>
            <th>N&deg; APROBACION</th>
            <th>BANCO</th>
             <th>CUENTA</th>
            <th>FECHA TRANSACCION</th>
            <th>MONTO</th>
           
        </tr>
        </tbody>
    </table>
    <br />
    <table id="t_timbre"  border="1" cellpadding="0" cellspacing="0" style="display:none">
        <tbody id="tb_timbre">
        <tr bgcolor="#9999FF">
            <td width="12%"></td>
            <td width="22%">VALOR DEL TIMBRE</td>
            <td width="14%">CANTIDAD</td>
            <td width="24%">UNIDAD TRIBUTARIA</td>            
            <td width="28%">MONTO</td>
        </tr>
        </tbody>
    </table>
<!--contiene el boton submit y el total a pagar-->
<div id="content_total" style="margin-top: -20px;
border-radius: 10px/20px;
text-shadow: 2px -1px 3px rgba(156, 150, 150, 0.4);
font-family: arial black;
background: linear-gradient(360deg,#ccc,white);">

<span id="total" style="width: 300px; float: left; margin-left: 280px;"></span>
<!--contiene el boton submit-->
<div id="boton" style="margin-left: 850px;">

<input name="miSubmit" id="miSubmit" type="button" onclick="submit_form()"  value="Enviar" style="display:none;
width: 150px;
border-radius: 10px /20px;
font-weight: bold;
cursor: pointer;" /></div>
<div style="clear:both"></div>
</div>   
		<input name="tplid" id="tplid" type="hidden" value="<?php echo $tipo_planilla_id ?>" />
    	<input name="num_planilla" id="num_planilla" type="hidden" value="<?php echo $planilla ?>" />
        <input name="total_planilla" id="total_planilla" type="hidden" value="<?php $total=str_replace(',','.',$total); echo number_format($total, 2, '.', '') ; ?>" />
        <input name="plid" id="plid" type="hidden" value="<?php echo $planilla_id ?>" />
        <input type="hidden" name="tablas_str" id="tablas_str" value="" />
        <input type="hidden" name="security_token" id="security_token" value="<?php echo md5(encrypt($_SESSION['nivel'],'12345')) ?>" />
        <span id="mensaje" style="text-align:center; display:block"></span>
    </form>
</div>
</div>
<script type="text/javascript" src="funciones/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="funciones/jquery-ui.js"></script>
<script type="text/javascript" src="funciones/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript" src="funciones/funciones1.js?a=1"></script>
<script type="text/javascript" src="funciones/pagar_verif.min.js"></script>
<script type="text/javascript" src="funciones/ajax.js"></script>
<script src="funciones/lib/numeral-js-1.5.3/numeral.js"></script> <!--libreria para formatear numeros en formato moneda-->
</body>
</html>