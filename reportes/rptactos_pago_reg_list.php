<?php
include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');
include('../funciones/mysql.php');
$fecha_desde=decrypt($_GET["fecha_desde"],'123');
$fecha_hasta=decrypt($_GET["fecha_hasta"],'123');
$registro_id=decrypt($_GET["registro_id"],'123');
$tipo_acto=decrypt($_GET["tipo_acto"],'123');
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Planilla con Pago Validado</title>
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript">

function attach_file( p_script_url ) {
		// create new script element, set its relative URL, and load it
		script = document.createElement( 'script' );
		script.src = p_script_url;
		document.getElementsByTagName( 'head' )[0].appendChild( script );
}



function ColorUno(src,color_entrada) {
	src.style.backgroundColor=color_entrada;
} 
function ColorDos(src,color_default) { 
    src.style.backgroundColor=color_default;
	//src.style.cursor="default"; 
}

function enviar_form(){
		cadena=document.getElementById('contiene_table').innerHTML;
		document.env_tabla.tabla_html.value = cadena;
}

</script>
</head>
<body onload="javascript:attach_file('menu_usr.php');">
<span id="menu"></span>
<br />
<br />
<div class="contenedor">

<?php
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call rptactos_pago_reg_list(?,?,?,?)');
		$stmt->bind_param('ssis',$fecha_desde,$fecha_hasta,$registro_id,$tipo_acto);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result();
			$cuantos_registros = $stmt->num_rows;
			if($cuantos_registros>0){
				$stmt->bind_result($planilla_id,$tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$fecha,$registro,$nombre_presentante,$ci_presentante,$nombre_empresa,$rif,$tipo_acto,$total,$creadopor,$fechacreado,$name_accionista,$direccion_local);
				?><div class="contenedor2" id="contiene_table">   
                <table id="t_deposito"  border="1" cellpadding="0" cellspacing="0">
                	<thead bgcolor="#9999FF">
                        <tr bgcolor="#9999FF">
                           <td width="4%"></td>
                            <td >Num. Planilla</td>
                            <td >Fecha en Planilla</td>
                            <td >Registro</td>
                            <td >Rif</td>
                            <td >Direccion Fiscal del Local</td>
                            <td >Accionista</td>
                            <td >Nombre Empresa</td>
                            <td >C.I Presentante</td>
                            <td >Nombre Presentante</td>
                            <td >Clase de Acto</td>
                            <td >Total Planilla</td>
                             <td >Usuario</td>
                             <td >Fecha Creado</td>  
                        </tr></thead><tbody id="tb_deposito"><?php
				$cont=0;
				$cco=0;
				while($stmt->fetch()){
					$cco++; 
					$cont++;
					if(($cco%2)==0){ 
						$color = '#FFFFFF'; 
					}else{
						$color = '#E6F2E1'; 
					}?>
					<tr onmouseover="ColorUno(this,'#CCCCCC');" onmouseout="ColorDos(this,'<?php echo $color; ?>')" bgcolor="<?php echo $color; ?>">
                    		<td><?php echo $cont ?></td>
                            <td ><a href="../redirect2.php?p=2&planilla=<?php echo $num_planilla ?>&tplid=<?php echo $tipo_planilla_id ?>&adm=1"><?php echo $tipo_planilla_desc.'-'.$num_planilla ?></a></td>
                            <td ><?php echo $fecha ?></td>
                            <td ><?php echo $registro ?></td>
                            <td ><?php echo $rif ?></td>
                            <td ><?php echo $direccion_local ?></td>
                            <td><?php echo $name_accionista ?></td> 
                            <td ><?php echo $nombre_empresa ?></td>
                             <td ><?php echo $ci_presentante ?></td>
                             <td ><?php echo $nombre_presentante ?></td>
                            <td ><?php echo $tipo_acto ?></td>
                            <td ><?php echo bsf($total) ?></td>
                             <td ><?php echo $creadopor ?></td>
                            <td ><?php echo $fechacreado ?></td>                             
					</tr><?php
				}?></tbody>
                </table>
				</div>
				<div class="contenedor2">
			<br />
            <table>
            	<tr> 
                	 	<td>            
                        <form action="rptactos_pago_reg_list_export.php" method = "POST" name"f_exportt" >
                            <input type="hidden" name="fecha_desde" value="<?php echo $fecha_desde ?>" />
                            <input type="hidden" name="fecha_hasta" value="<?php echo $fecha_hasta ?>" />
                            <input type="hidden" name="registro_id" value="<?php echo $registro_id ?>" />
                            <input type="hidden" name="tipo_acto" value="<?php echo $tipo_acto ?>" />
                            <input type = "submit" value = "Exportar a Excel" />
                        </form>
                    </td>                  
         <!--           <td>
                        <form action="rptactos_pago_usr_list_export.php"  method="post" id="env_tabla" name="env_tabla" onsubmit="return enviar_form()">
                        	<input type="hidden" name="tabla_html" id="tabla_html" value="">
                            <input type = "submit" value = "Exportar a Excel #2"  />
                        </form>
                    </td>-->
                </tr>
            </table>                                     
				</div><?php
			}else{
				?><table width="420" border="1" align="center" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                  </table><?php
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
	?>
</div>
</body>
</html>