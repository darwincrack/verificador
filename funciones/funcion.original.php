<?php 
include ('validacion.php');

function porcentaje($cantidad,$porciento){
	return $cantidad*($porciento/100);
}

function bsf($monto){
	if (trim($monto)==''){
		return '';
	}else{
		return number_format($monto,2,',','.');
	}
}

function texto_limpio($valor){
	return trim(stripslashes(html_entity_decode($valor,ENT_QUOTES)));
}
function cedula($ced){
	return number_format($ced,0,',','.');
}

function SinHtml($str){
	return remove_HTML(stripslashes(htmlspecialchars($str, ENT_QUOTES)));
}

function ConHtml($str){
	return htmlentities($str,ENT_QUOTES);
}
function mysql_fecha($fecha){
	$val=substr($fecha,6,4).'-'.substr($fecha,3,2).'-'.substr($fecha,0,2);
	return $val;
}


function getBrowser(){
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	if(strpos($useragent,"MSIE") !== false && strpos($useragent,"Opera") === false && strpos($useragent,"Netscape") === false) {
		$found = preg_match("/MSIE ([0-9]{1}\.[0-9]{1,2})/",$useragent, $mathes);
		if($found)  {
			return "Internet Explorer " . $mathes[1];
		}
	}elseif(strpos($useragent,"Gecko"))  {
		$found = preg_match("/Firefox\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
		if($found)  {
			return "Mozilla Firefox " . $mathes[1];
		}
		$found = preg_match("/Netscape\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);  
		if($found)  {
			return "Netscape " . $mathes[1];
		}
		$found = preg_match("/Chrome\/([^\s]+)/",$useragent, $mathes);
		if($found)  {
			return "Google Chrome " . $mathes[1];
		}
		$found = preg_match("/Safari\/([0-9]{2,3}(\.[0-9])?)/",$useragent, $mathes);
		if($found)  {
			return "Safari " . $mathes[1];
		}
		$found = preg_match("/Galeon\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
		if($found)  {
			return "Galeon " . $mathes[1];
		}
		$found = preg_match("/Konqueror\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
		if($found)  {
			return "Konqueror " . $mathes[1];  
		}
		return "Gecko based";
	}elseif(strpos($useragent,"Opera") !== false){
		$found = preg_match("/Opera[\/ ]([0-9]{1}\.[0-9]{1}([0-9])?)/",$useragent,$mathes);
		if($found)  {  
			return "Opera " . $mathes[1];
		}
	}elseif (strpos($useragent,"Lynx") !== false)  {
		$found = preg_match("/Lynx\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
		if($found)  {
			return "Lynx " . $mathes[1];
		}
	}elseif (strpos($useragent,"Netscape") !== false)  {
		$found = preg_match("/Netscape\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
		if($found)  {
			return "Netscape " . $mathes[1];
		}
	}else{
		return false;  
	}  
}



$ut = 76;


?>