<?php
include('seguridad_trans.php');
include('conex.php');
include('funciones/funcion.php');
$admin=$_SESSION['pago_admin'];
$con_lote=($admin==true)?'1':'0';  //VARIABLE QUE INDICA SI SE ESTA INTRODUCIENDO NUMEROS DE LOTE
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript">
	if (history.forward(1)){location.replace(history.forward(1))} 
</script>
<head>
<meta http-equiv="PRAGMA" content="NO-CACHE; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
if (!isset($_POST["tablas_str"])){
	die('ERROR_SET');
}else{
	$cadena=$_POST["tablas_str"];
}

if (!isset($_POST["plid"])){
	die('ERROR ID DE PLANILLA');
}else{
	$planilla_id=$_POST["plid"];
}
if (!isset($_POST["tplid"])){
	die('ERROR TIPO DE PLANILLA');
}else{
	$tipo_planilla_id=$_POST["plid"];
}


if (!isset($_POST["num_planilla"])){
	die('ERROR-SET');
}else{
	$num_planilla=$_POST["num_planilla"];
}

if (!isset($_SESSION["usuario"])){
	die();
}else{
	$usuario=$_SESSION["usuario"];
}


if (!isset($_POST["text_obser"])){
	die('ERROR_SET');
}else{
	
	$observaciones=stripslashes(html_entity_decode($_POST["text_obser"]));
	if ($observaciones == 'Observaciones...'){
		$observaciones='';
	}else{
		$observaciones==$observaciones;
	}
	
}


$ip= getRealIpAddr();


//die($cadena);


$con = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_error()) {
	die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
}
$stmt = $con->stmt_init();
$stmt->prepare('call sel_planilla2(?,?)'); // comprobamos que no se haya pagado la planilla, devuleve 1 si consigue el pago
$stmt->bind_param('ii',$num_planilla,$tipo_planilla_id);
if(!$stmt->execute()){
	throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
}else{
	$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
	$cuantos_registros = $stmt->num_rows;
	if($cuantos_registros>0){
		$stmt->bind_result($resp);
		while($stmt->fetch()){	
			if ($resp==1){ /*resp es 1 si existe pago registrado en la planilla*/
				?><link href="styles/contenido.css" rel="stylesheet" type="text/css" />
                 <div class="contenedor" id="resp_serv">
                    <br  /><br  /><br  />
                    <table width="58%" border="1" cellspacing="0" cellpadding="0"  align="center">
                        <tr>
                            <td bgcolor="#FF0000">LA PLANILLA YA FUE PAGADA</td>
                        </tr>
                    </table>
                </div><?php
				die();
			}
		}                      	
	}else{
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
		$con->close();


		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA');
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call ins_pago_lote(?,?,?,?,?,?)');
		$stmt->bind_param('sssiis',$cadena,$usuario,$ip,$planilla_id,$con_lote,$observaciones);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
			$cuantos_registros = $stmt->num_rows;
			if($cuantos_registros>0){
				$stmt->bind_result($switch,$comentario);
				while($stmt->fetch()){
					if ($switch == 0){?>
						<div class="contenedor" id="resp_serv">
							<table width="58%" border="0" cellspacing="0" cellpadding="0"  align="center">
								<tr><td bgcolor="#FF0000"><?php echo $comentario. ' ' ?><a href="javascript:history.go(-1);"><strong>INTENTE DE NUEVO</strong></a></td>
								</tr>
							 </table>
						</div><?php			
					}elseif ($switch == 1){
						//indica si el usuario es administrador, logeado desde el sitio principal es decir, q no esta en verificador.satdc.gob.ve/reportes
						if(($admin==true)&&($_SESSION['location_user']!='1')){
							header('location: reportes/body.php');
						}else{
							?>
							<script type="text/javascript">window.location="body.php"</script><?php
						}
					}
				}
			}else{
				echo 'ERROR, NO SE RECIBIO INFORMACION';
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
		$con->close(); 
	}
}
?>
</body>
</html>