<?php include ('funciones/funcion.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>

<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
if (isset($_GET["rif"])){
	$rif=texto_limpio($_GET["rif"]);
}else{
	die('ERROR EN RIF');
}
if (isset($_GET["nombre_empresa"])){
	$nombre_empresa= texto_limpio($_GET["nombre_empresa"]);
}else{
	die('ERROR EN NOMBRE DE EMPRESA');
}
if (isset($_GET["tipo_acto"])){
	$nombre_empresa= texto_limpio($_GET["tipo_acto"]);
}else{
	die('ERROR EN TIPO DE ACTO');
}
if (isset($_GET["fecha"])){
	$fecha= texto_limpio($_GET["fecha"]);
}else{
	die('ERROR EN FECHA');
}
if (!isset($_GET["tipo_persona"])){
	die('TIPO PERSONA');
}else{
	$tipo_persona = $_GET["tipo_persona"];
	if ($tipo_persona==1){ //Persona Juridica
		$ut_constante = 0.5;
		$texto = 'Persona Juridica';
	}elseif ($tipo_persona==2){ //Persona Natural
		$ut_constante = 0.1;
		$texto = 'Persona Natural';
	}
}

$resultado = $ut_constante * $ut;

$registro= $_GET["registro"];
$estado=$_GET["estado"];
$nombre_presentante=$_GET["nombre_presentante"];
$nacionalidad=$_GET["nacionalidad"];
$ced_present=$_GET["ced_present"];
$nombre_empresa=$_GET["nombre_empresa"];
$rif=$_GET["rif"];
$tipo_acto=$_GET["tipo_acto"];
$fecha=$_GET["fecha"];
?>
<div class="contenedor" id="consulta">
	<h1>Calculo de Actos Realizados Art. 13 de La Ley de Timbre Fiscal</h1>
    <h2>Numeral 7</h2>
    <br />
    <form action="imprime.php" method="get">    
        <table align="center" id="consulta" class="tablas" border="1">
            <tr>
                <td align="right"><strong>Oficina de Registro:</strong></td>
                <td><?php echo $registro ?><input name="registro" type="hidden" value="<?php echo $registro ?>" /></td>
            </tr>
            <tr>
                <td align="right"><strong>Estado:</strong></td>
                <td><?php echo $estado ?><input name="estado" type="hidden" value="<?php echo $estado ?>" /></td>
            </tr>
            <tr>
                <td align="right"><strong>Nombre del Presentante:</strong></td>
                <td><?php echo $nombre_presentante ?><input name="nombre_presentante" type="hidden" value="<?php echo $nombre_presentante ?>" /></td>
            </tr>
            <tr>
                <td align="right"><strong>C.I:</strong></td>
                <td><?php if ($nacionalidad<>''){echo $nacionalidad.'-'.$ced_present;}else{echo ($ced_present);} ?><input name="nacionalidad" type="hidden" value="<?php echo $nacionalidad ?>" /><input name="ced_present" type="hidden" value="<?php echo $ced_present ?>" /></td>
            </tr>
            <tr>
                <td align="right"><font size="-1"><strong>Nombre de la Persona Natural o Juridica Otorgante:</strong></font></td>
                <td><?php echo $nombre_empresa ?><input name="nombre_empresa" type="hidden" value="<?php echo $nombre_empresa ?>" /></td>
            </tr>                                 
            <tr>
                <td  align="right"><strong>RIF N� / C.I</strong>:</td>
                <td width="397"><?php echo $rif ?><input name="rif" type="hidden" value="<?php echo $rif ?>" /></td>
            </tr>
            <tr>
                <td align="right" width="216"><strong>Tipo de Acto:</strong></td>
                <td><?php echo $tipo_acto ?><input name="tipo_acto" type="hidden" value="<?php echo $tipo_acto ?>" /></td>
            </tr>
            <tr>
                <td  align="right"width="216"><strong>Fecha:</strong></td>
                <td><?php echo $fecha ?><input name="fecha" type="hidden" value="<?php echo $fecha ?>" /></td>
            </tr>    
        </table>
    <br />    
    <table align="center" id="consulta" class="tablas" border="1">
        <tr align="center">
        	<td width="183"><strong>Acto</strong></td>
            <td width="187"><strong>Calculo Usado</strong></td>
            <td width="210"><strong>Resultado</strong></td>
        </tr>
    	<tr align="center">
    	  	<td><strong>Cualquier Otro Documento Poder <br /><?php echo $texto ?></strong></td>
            <td><strong><?php echo $ut_constante ?> UT (<?php echo $ut ?> BsF)</strong></td>
      		<td><strong><?php echo bsf($resultado) ?> BsF</strong><input name="tasa" type="hidden" value="<?php echo bsf($resultado) ?>" /><input name="total" type="hidden" value="<?php echo bsf($resultado) ?>" /></td>
        </tr>
    </table>
    <table align="center">
        <tr width="571">
            <td ><input name="imprimir" type="submit" value="Imprimir" /></td>
        </tr>
    </table>    
  </form>    
</div>
</body>
</html>