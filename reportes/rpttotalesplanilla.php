<?php include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');

if (isset($_GET["fechadesde"]) && isset($_GET["fechahasta"])){
	$desde =$_GET["fechadesde"];
	$hasta=$_GET["fechahasta"];
}else{
	$desde ="";
	$hasta="";
}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Pago de Planillas</title>
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript" src="../funciones/calendarDateInput.js"></script>
<script type="text/javascript">
function ColorUno(src,color_entrada) {
	src.style.backgroundColor=color_entrada;
} 
function ColorDos(src,color_default) { 
    src.style.backgroundColor=color_default;
	//src.style.cursor="default"; 
}
function enviar_form(){
	cadena=document.getElementById('exp_t').innerHTML;
	document.env_tabla.tabla_html.value = cadena;
	}
</script>
</head>
<body onload="loadurl('menu.php','menu')">
<span id="menu"></span>
<div class="contenedor">
<h1>Totales Por Registros</h1>
    <h3>Para listados de un solo dia, colocar la misma fecha en ambos cuadros.</h3>
    <form  action="" method="get" id="consulta" name="f_providencia">
        <table width="71%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas3" >
          <tr>
            <td align="center"><a>Fecha Desde:</a></td>
            <td  ><script>DateInput('fechadesde',true)</script></td>
            <td  align="center"><a>Fecha Hasta:</a></td>
            <td ><script>DateInput('fechahasta',true)</script></td>
          </tr><?php
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechadesde"],8,2);
                $mes=substr($_GET["fechadesde"],5,2)-1;
                $a�o=substr($_GET["fechadesde"],0,4);
                echo "<script>fechadesde_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }
            if (isset($_GET["fechadesde"])){
                $dia=substr($_GET["fechahasta"],8,2);
                $mes=substr($_GET["fechahasta"],5,2)-1;
                $a�o=substr($_GET["fechahasta"],0,4);
                echo "<script>fechahasta_Object.pickDay2(". $dia . "," . $mes . "," . $a�o . ")</script>";
            }?>
          <tr>
            <td colspan="4" align="center"><input name="buscar" type="submit" value="Buscar" class="boton_grande" /></td>
          </tr>
        </table>
    </form><br  />
	</div>
	<?php
	if (isset($_GET["fechadesde"]) || isset($_GET["fechahasta"])){
		$fecha_desde = $_GET["fechadesde"];
		$fecha_hasta = $_GET["fechahasta"];
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call rpt_total_registros(?,?)');
		$stmt->bind_param('ss',$fecha_desde,$fecha_hasta);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result();
			$cuantos_registros = $stmt->num_rows;
			$excel='';
			if($cuantos_registros>0){
				$stmt->bind_result($num_registro,$banco,$deposito_total,$pto_venta_total,$timbre_total);
				?><div class="contenedor2" id="exp_t">                
                  <table id="t_deposito"  border="1" cellpadding="0" cellspacing="0" >
                    <tbody id="tb_deposito">
                        <tr bgcolor="#9999FF">
                            <!--<td width="4%"></td>-->
                            <td >Num. Registro</td>
                            <td >Banco</td>
                            <td >Total (Deposito)</td>
                            <td >Total (Pto Venta)</td>
                            <td >Total (Timbre)</td>
                            <td >Total</td>                                                                                   
                        </tr><?php
						
				$cont=0;
				$cco=0;
				$total=0;
				$color1='#FFFFFF';
				$color2='#E6F2E1';
				$color_temp='';
				$num_planilla_ultimo='';
				$registro_actual='';
				$total_deposito=0;
				$total_pto_venta=0;
				$total_timbre=0;
				$total_registro=0;
				$acumulado_deposito=0;
				$acumulado_pto_venta=0;
				$acumulado_timbre=0;
				$acumulado_total=0;
				$color_total ='#FFFF99';								
				while($stmt->fetch()){
					$cco++; 
					$cont++;
					$total = $deposito_total + $pto_venta_total + $timbre_total;
					$color = '#FFFFFF';					
					if(($registro_actual==$num_registro)||($registro_actual!=$num_registro && $cont==1)){ // VALIDAMOS SI ES PRIMER REGISTRO O DEL MISMO REGISTRO?>                  					
                        <tr onmouseover="ColorUno(this,'#CCCCCC');" onmouseout="ColorDos(this,'<?php echo $color; ?>')" bgcolor="<?php echo $color; ?>">                                                 
                            <td ><?php echo $num_registro ?></td>
                            <td ><?php echo trim($banco) ?></td>
                            <td ><?php echo trim(bsf($deposito_total)) ?></td>                           
                            <td><?php echo trim(bsf($pto_venta_total)) ?></td> 
                            <td ><?php echo trim(bsf($timbre_total)) ?></td>                                                       
                            <td ><?php echo trim(bsf($total)) ?></td>
                        </tr><?php
						$total_deposito=$total_deposito+$deposito_total;
						$total_pto_venta=$total_pto_venta+$pto_venta_total;
						$total_timbre=$total_timbre+$timbre_total;
						$total_registro=$total_registro+$total;
						$registro_etiqueta=$num_registro;
						$acumulado_deposito=$acumulado_deposito+$deposito_total;
						$acumulado_pto_venta=$acumulado_pto_venta+$pto_venta_total;
						$acumulado_timbre=$acumulado_timbre+$timbre_total;
						$acumulado_total=$acumulado_total+$total;						                           
					}elseif($registro_actual!=$num_registro){ //SI CAMBIO DE REGISTRO RESETEO EMPIEZO A sumar desde 0
						echo '<tr onmouseover="ColorUno(this,\'#CCCCCC\');" onmouseout="ColorDos(this,\''.$color_total.'\')" bgcolor="'.$color_total.'">';
						echo '<td colspan="2">TOTAL '.$registro_etiqueta.':</td>';
						echo '<td>'.bsf($total_deposito).'</td>';
						echo '<td>'.bsf($total_pto_venta).'</td>';
						echo '<td>'.bsf($total_timbre).'</td>';
						echo '<td>'.bsf($total_registro).'</td>';						
						echo '</tr>						
						<tr onmouseover="ColorUno(this,\'#CCCCCC\');" onmouseout="ColorDos(this,\''.$color.'\')" bgcolor="'.$color.'">                                                 
                            <td >'.$num_registro.'</td>
                            <td >'.trim($banco).'</td>
                            <td >'.trim(bsf($deposito_total)).'</td>                           
                            <td>'.trim(bsf($pto_venta_total)).'</td> 
                            <td >'.trim(bsf($timbre_total)).'</td>                                                       
                            <td >'.trim(bsf($total)).'</td>
                        </tr>';
						$total_deposito=0;
						$total_pto_venta=0;
						$total_timbre=0;
						$total_registro=0;
						$total_deposito=$total_deposito+$deposito_total;
						$total_pto_venta=$total_pto_venta+$pto_venta_total;
						$total_timbre=$total_timbre+$timbre_total;
						$total_registro=$total_registro+$total;
						$acumulado_deposito=$acumulado_deposito+$deposito_total;
						$acumulado_pto_venta=$acumulado_pto_venta+$pto_venta_total;
						$acumulado_timbre=$acumulado_timbre+$timbre_total;
						$acumulado_total=$acumulado_total+$total;
						$registro_etiqueta=$num_registro;												
					}									
					
					if($cuantos_registros==$cont){
						echo '<tr onmouseover="ColorUno(this,\'#CCCCCC\');" onmouseout="ColorDos(this,\''.$color_total.'\')" bgcolor="'.$color_total.'">';
						echo '<td colspan="2">TOTAL '.$registro_etiqueta.':</td>';
						echo '<td>'.bsf($total_deposito).'</td>';
						echo '<td>'.bsf($total_pto_venta).'</td>';
						echo '<td>'.bsf($total_timbre).'</td>';
						echo '<td>'.bsf($total_registro).'</td>';						
						echo '</tr>';
						echo '<tr onmouseover="ColorUno(this,\'#CCCCCC\');" onmouseout="ColorDos(this,\''.$color_total.'\')" bgcolor="'.$color_total.'">';
						echo '<td colspan="2">TOTAL:</td>';
						echo '<td>'.bsf($acumulado_deposito).'</td>';
						echo '<td>'.bsf($acumulado_pto_venta).'</td>';
						echo '<td>'.bsf($acumulado_timbre).'</td>';
						echo '<td>'.bsf($acumulado_total).'</td></tr>';						
					}					
					$registro_actual = $num_registro;					
				}?></tbody>
                </table>
              </div>
              <div class="contenedor2">
			<br />
            <table>
            	<tr>
                	<td>
                    	<form action="export_totalesplanilla.php" method = "POST" name"f_export" >
                            <input type="hidden" name="desde" value="<?php echo $desde ?>" />
                            <input type="hidden" name="hasta" value="<?php echo $hasta ?>" />
                            <input type = "submit" value = "Exportar a Excel" />
                         </form>
                    </td>
                    <td>
                          <form action="export_table.php"  method="post" id="env_tabla" name="env_tabla" onsubmit="return enviar_form()">
                            <input type="hidden" name="tabla_html" id="tabla_html" value="">
                            <input type="hidden" name="f_nombre" value="Total_Registros_<?php echo $desde.'_'.$hasta ?>" />
                            <input type = "submit" value = "Exportar a Excel # 2"   />
                          </form>                    
                    </td>
                </tr>
            </table>                            
			  </div><?php
			}else{
				?><div class="contenedor" align="center">
                <table width="420" border="1" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                </table>
				</div><?php
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
	}?>
</body>
</html>