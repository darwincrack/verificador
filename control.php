<?php
include "conex.php";
include('funciones/validacion.php');

$con = new mysqli($host,$user,$clave,$db,$puerto);

if (mysqli_connect_error()) {
    die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

$ip= getRealIpAddr();
$stmt = $con->stmt_init();
/*$log = ValidarDatos($_POST["usuario"]);
$pwd = ValidarDatos($_POST["contrasena"]);

$log=$con->real_escape_string($log);
$pwd=$con->real_escape_string($pwd);*/

$log = $_POST["usuario"];
$pwd = $_POST["contrasena"];

$stmt->prepare('call sel_usuario(?,?,?)');
$stmt->bind_param('sss', $log, $pwd, $ip);

if(!$stmt->execute()){
	throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
}else{
	$stmt->store_result(); //Sin esta línea no podemos obtener el total de resultados anticipadamente
	$cuantos_registros = $stmt->num_rows;
	if($cuantos_registros>0){
		$stmt->bind_result($usuario,$perfil_tipo_acto_id,$nivel_user,$registro_id,$registro_desc);
		while($stmt->fetch()){ 
			session_start();
			
		    $_SESSION["autentificado"]= "SI";
/*			$_SESSION["nombre"]= $nombre1 . " " . $nombre2 . " " . $apellido1 . " " . $apellido2;
			$_SESSION["area"]= $area_id;
			*/
			$_SESSION['nivel']=$nivel_user;
			$_SESSION["usuario"]=$usuario;
			$_SESSION["password"]=$pwd;
			$_SESSION["perfil_tipo_acto_id"]=$perfil_tipo_acto_id;
			$_SESSION['location_user']='1';
			$_SESSION['registro_id']=$registro_id;
			$_SESSION['registro_desc']=$registro_desc;
			$stmt->close();
			$con->close();			
			header("Location: body.php");
    		exit(); } 	
		 } else {
    		session_start();
			//si no existe le mando otra vez a la portada
			$_SESSION = array();
			if (ini_get("session.use_cookies")) {
				$params = session_get_cookie_params();
				setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]);
				}
			// Finally, destroy the session.
			session_destroy();
			$stmt->close();
			$con->close();
			header("Location: login.php?errorusuario=si");
} 
}
/*$stmt->close();
$con->close();*/
?>