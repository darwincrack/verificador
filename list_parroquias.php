<?php
require_once('seguridad_trans.php');
include('conex.php');

// es la lista de documentos que se han guardado para un determinado local
$mysqli = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA');
		}

$municipio=1;
$query = "CALL list_parroquias($municipio)";

if ($result = $mysqli->query($query)) {

    /* obtener array asociativo */
	echo "<option value=''>--Seleccione--</option>";
    while ($row = $result->fetch_assoc()){
    	
echo'<option value="'.$row["parroquia_id"].'"> '.$row["name_parroquia"].'</option>';
 } 
 
/* liberar el resultset */
$result->free();
}
/* cerrar la conexión */
$mysqli->close();
