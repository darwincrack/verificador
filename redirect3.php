<?php
include('seguridad_trans.php');
include('conex.php');
include('funciones/funcion.php');

if (!isset($_GET["p"])){
	die();
}else{
	$p=$_GET["p"];
}

if (!isset($_GET["planilla"])){
	die();
}else{
	$num_planilla=$_GET["planilla"];
}

if (isset($_GET["saved"])){
	$guardo=$_GET["saved"];
}else{
	$guardo=0; /*variable es uno o cero,  es para redirigir el browser a inicio despues de mandar a imprimir*/
}

//la variable $p nos indica si los datos que se van a buscar son para imprimir, o solo para mostrar

$con = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_error()) {
	die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
}
$stmt = $con->stmt_init();
$stmt->prepare('call sel_planilla2(?)'); // comprobamos que no se haya pagado la planilla, devuleve 1 si consigue el pago
$stmt->bind_param('i',$num_planilla);
if(!$stmt->execute()){
	throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
}else{
	$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
	$cuantos_registros = $stmt->num_rows;
	if($cuantos_registros>0){
		$stmt->bind_result($resp);
		while($stmt->fetch()){	
			if ($resp==1){ /*resp es 1 si existe pago registrado en la planilla*/
				?><link href="styles/contenido.css" rel="stylesheet" type="text/css" />
                <div class="contenedor" id="resp_serv">
                    <br  /><br  /><br  />
                    <table width="58%" border="1" cellspacing="0" cellpadding="0"  align="center">
                        <tr>
                            <td bgcolor="#FF0000">LA PLANILLA YA FUE PAGADA</td>
                        </tr>
                    </table>
                </div><?php
				die();
			}
		}                      	
	}else{
		?><script type="text/javascript">window.location="redirect2.php?p=0&planilla=<?php echo $num_planilla ?>";</script><?php
		die();
	}
}
$stmt->free_result();
$stmt->close();
while($con->next_result()) { }
$con->close();
?>