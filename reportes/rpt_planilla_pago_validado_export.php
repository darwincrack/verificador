<?php
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=rpt_planilla_pago_validado.xls");
header("Pragma: no-cache");
header("Expires: 0");
include('../seguridad_trans.php'); 
include('../conex.php');
include('../funciones/funcion.php');
$fecha_desde =$_POST["desde"];
$fecha_hasta=$_POST["hasta"];
$usuario=$_POST["usuario"];

$con = new mysqli($host,$user,$clave,$db,$puerto);
if (mysqli_connect_error()) {
    die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}
$stmt = $con->stmt_init();
$stmt->prepare('call rpt_planillaPagoValidado(?,?,?)');
$stmt->bind_param('sss',$fecha_desde,$fecha_hasta,$usuario);
if(!$stmt->execute()){
	throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
}else{
	$stmt->store_result(); //Sin esta línea no podemos obtener el total de resultados anticipadamente
	$cuantos_registros = $stmt->num_rows;
	if($cuantos_registros>0){
				$stmt->bind_result($planilla_id,$tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$fecha,$registro,$estado,$nombre_presentante,$cedula_presentante,$nombre_empresa,$rif,$numeral,$tipo_acto,$ut,$capital,$monto,$porcentaje,$folios,$tasa,$total);
		$excel= "usuario: ".$usuario."\n";
		$excel .= "NUM. DE PLANILLA\tFECHA EN PLANILLA\tREGISTRO\tCEDULA O RIF(OTORGANTE)\tPERSONA OTORGANTE(NATURAL O JURIDICA)\tCEDULA PRESENTANTE\tNOMBRE DEL PRESENTANTE\tCLASE DE ACTO\tTOTAL PLANILLA\n";
		while($stmt->fetch()){
			$excel .= "$tipo_planilla_desc".'-'."$num_planilla\t$fecha\t$registro\t$rif\t$nombre_empresa\t$cedula_presentante\t$nombre_presentante\t$tipo_acto\t".trim(bsf($total))."\n";
		}
		$excel = str_replace("\"", "", $excel);
		print $excel;
	}
}
$stmt->free_result();
$stmt->close();
while($con->next_result()) { }
$con->close();