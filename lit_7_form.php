<?php include ('funciones/funcion.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="funciones/funciones.js"></script>
<script type="text/javascript">
function check_radioboton(){
	var radioboton2 = document.f_monto.tipo_persona;
	for(i=0;i<radioboton2.length;i++){
		if (radioboton2[i].checked==false){
			valor_radioboton2=null;
		}else{
			valor_radioboton2 = radioboton2[i].value;
			break;
		}
		//break;			
	}
	if (valor_radioboton2==null){
		alert('DEBE SELECCIONAR PERSONA JURIDICA O NATURAL');
		return false;
	}else{
		return true;
	}
}
function submit_form() {
/*		var monto = document.f_monto.i_monto.value;
		var folios = document.f_monto.i_folios.value;*/
		if(check_radioboton()==true) {
			document.f_monto.submit();
		}else{
			return false;
		}
}
</script>
</head>
<body>
<div class="contenedor" id="consulta">
	<h1>Calculo de Actos Realizados Art. 13 de La Ley de Timbre Fiscal</h1>
    <h2>Numeral 7</h2>
    <h2>Cualquier Otro Documento Poder </h2>
    <form action="lit_7.php" method="get"  name="f_monto">
	 <?php date_default_timezone_set('America/Caracas');
	 $fecha=date('d/m/Y');
	 ?>
        <table width="627" border="0" align="center" class="tablas" id="consulta">
            <tr>
                <td align="right"><strong>Oficina de Registro:</strong></td>
                <td><select name="registro">
            		<option>REGISTRO PRIMERO</option>
                    <option>REGISTRO SEGUNDO</option>
                    <option>REGISTRO CUARTO</option>
                    <option>REGISTRO SEPTIMO</option>
                    <option>REGISTRO QUINTO</option>
                    </select>
                <!--<input name="registro" type="text" size="48" maxlength="48" onblur="javascript:this.value=this.value.toUpperCase();"/>--></td>
            </tr>
            <tr>
                <td align="right"><strong>Estado:</strong></td>
                <td><input name="estado" type="text" size="65" maxlength="65"  value="DISTRITO CAPITAL Y ESTADO MIRANDA" onblur="javascript:this.value=this.value.toUpperCase();"/></td>
            </tr>
             <tr>
                <td align="right"><strong>C.I:</strong></td>
                <td><select name="nacionalidad">
                <option value="" selected ></option>
                <option value="V" >V</option>
                <option value="E" >E</option>
                </select><input name="ced_present" type="text" size="20" maxlength="13"  onblur="javascript:this.value=this.value.toUpperCase();"/></td>
            </tr>
            <tr>
                <td align="right"><strong>Nombre del Presentante:</strong></td>
                <td><input name="nombre_presentante" type="text" size="65" maxlength="65"  onblur="javascript:this.value=this.value.toUpperCase();"/></td>
            </tr>

            <tr>
                <td align="right"><font size="-1"><strong>Nombre de la Persona Natural o Juridica Otorgante:</strong></font></td>
                <td><input name="nombre_empresa" type="text" size="65" maxlength="65"  onblur="javascript:this.value=this.value.toUpperCase();"/></td>
            </tr>                                 
            <tr>
                <td  align="right"><strong>RIF N� / C.I</strong>:</td>
                <td width="397"><input name="rif" type="text" onKeyPress="javascript:this.value=this.value.toUpperCase();"  onblur="javascript:this.value=this.value.toUpperCase();" size="20" maxlength="13" /></td>
            </tr>
            <tr>
                <td  align="right"width="216"><strong>Tipo de Acto:</strong></td>
                <td><input name="tipo_acto" type="text" size="65" maxlength="180" onKeyPress="javascript:this.value=this.value.toUpperCase();"  onblur="javascript:this.value=this.value.toUpperCase();" /></td>
            </tr>
            <tr>
                <td  align="right"width="216"><strong>Fecha:</strong></td>
                <td><strong><?php echo $fecha ?></strong><input name="fecha" type="hidden" value="<?php echo $fecha ?>" /></td>
            </tr>
        </table>     
	 <br />
    <table width="343" border="0" align="center" class="tablas" id="consulta">
        <tr>
        	<td align="center"><strong>Seleccione Tipo de Persona</strong></td>
        </tr>
        <tr align="center">
        	<td width="293"><p>
        	  <label>
        	    <input type="radio" name="tipo_persona" value="1" id="tipo_persona_0" />
        	    Persona Juridica</label>
        	  <br />
        	  <label>
        	    <input type="radio" name="tipo_persona" value="2" id="tipo_persona_1" />
        	    Persona Natural</label>
        	  <br />
      	  </p></td>
        </tr>
        <tr>
        	<td align="center"><input name="boton" type="button" value="    Enviar    " onclick="submit_form()" /></td>
        </tr>
    </table>
    </form>
</div>
</body>
</html>
