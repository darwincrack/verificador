<?php
	include "conex.php";
	include('funciones/validacion.php');
	
				
    session_start();
	$usuario=$_SESSION["usuario"];
	$ip= getRealIpAddr();
	//si no existe le mando otra vez a la portada
	$_SESSION = array();
	if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000,
			$params["path"], $params["domain"],
			$params["secure"], $params["httponly"]
		);
	}
// Finally, destroy the session.
	
	$con = new mysqli($host,$user,$clave,$db,$puerto);
	if (mysqli_connect_error()) {
		/*die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '
				. mysqli_connect_error());*/
		echo "<script>\n";
		echo "alert('ERROR, NO SE REGISTRO LA SALIDA')\n";
		echo "history.back()";
		echo "</script>\n";
		die(); 
	}
	$stmt = $con->stmt_init();	
	$stmt->prepare('call usuario_logout(?,?)');
	$stmt->bind_param('ss', $usuario, $ip);
	
	if(!$stmt->execute()){
		//throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		echo "<script>\n";
		echo "alert('ERROR, NO SE REGISTRO LA SALIDA')\n";
		echo "history.back()";
		echo "</script>\n";	
	}else{
		$stmt->store_result(); //Sin esta línea no podemos obtener el total de resultados anticipadamente
		$cuantos_registros = $stmt->num_rows;
		if($cuantos_registros>0){
			$stmt->bind_result($resultado);
			while($stmt->fetch()){
				if ($resultado==1){
					session_destroy();
					header("Location: login.php");
				}
			}
		}
	}
	echo "<script>\n";
	echo "alert('ERROR, NO SE REGISTRO LA SALIDA')\n";
	echo "history.back()";
	echo "</script>\n";
	die();		
	$stmt->close();
	$con->close();
?>