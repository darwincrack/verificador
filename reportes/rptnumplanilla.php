<?php include('seguridad_adm.php');
include('../conex.php');
include('../funciones/funcion.php');
if(isset($_GET['planilla'])){
	$num_planilla=texto_limpio($_GET['planilla']);
}else{
	$num_planilla='';
}?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="../funciones/funciones1.js"></script>
<script type="text/javascript" src="../funciones/ajax.js"></script>
<script type="text/javascript">
function attach_file( p_script_url ) {
		// create new script element, set its relative URL, and load it
		script = document.createElement( 'script' );
		script.src = p_script_url;
		document.getElementsByTagName( 'head' )[0].appendChild( script );
}
function submit_form(pag) {
	var planilla = trim(document.f_monto.planilla.value);
	if (planilla != ''){
		if (IsNumericInt(planilla)){
			//document.f_monto.action=pag;
			document.f_monto.submit();
		}else{
			alert('ERROR EN NUMERO DE PLANILLA');
			return false;
		}
	}else{
		alert('ERROR EN NUMERO DE PLANILLA');
		return false;
	}
}
function pulsar(e,pag) { 
  tecla = (document.all) ? e.keyCode :e.which; 
  
  if (tecla == 13){
  	return submit_form(pag); 
  }else{
  	return soloEnteros(e);
  }
}
function enviar_form(){
	cadena=document.getElementById('exp_t').innerHTML;
	document.env_tabla.tabla_html.value = cadena;
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="../styles/contenido.css" rel="stylesheet" type="text/css" />
</head>
<body onload="loadurl('menu.php','menu')">
<span id="menu"></span>
<br />
<div class="contenedor">
	<h1>Buscar por N�mero de Planilla</h1>
</div>
<div class="contenedor" id="consulta">
    <form id="f_monto" name="f_monto" method="get" >
        <table width="57%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas">
          <tr>
            <td width="43%" ><h2>Introducir N� de Planilla:</h2></td>
            <td width="38%" align="center" >
            <select name="tplid" id="tplid"><?php			 
                        $con = new mysqli($host,$user,$clave,$db,$puerto);
                        if (mysqli_connect_error()) {
                            die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
                        }
                        $stmt = $con->stmt_init();
                        $stmt->prepare('call list_tipo_planilla()');
                        if(!$stmt->execute()){
                            throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
                        }else{
                            $stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
                            $cuantos_registros = $stmt->num_rows;
                            if($cuantos_registros>0){
                                $stmt->bind_result($tipo_planilla_id,$descrip);
                                    while($stmt->fetch()){	
                                        print "<option value=\"$tipo_planilla_id\">$descrip</option>";
                                    }                      	
                            }
                        }
                        $stmt->free_result();
                        $stmt->close();
                        while($con->next_result()) { }
                        $con->close();?>
                 </select>
            <input name="planilla" type="text" size="10" maxlength="7" value="<?php echo $num_planilla ?>" onkeypress="return pulsar(event,'redirect2.php');"/><input name="p" type="hidden" value="2" /></td>
            <td width="19%"><input type="button" value="  Enviar  " class="boton" name="busca"  onclick="return submit_form('redirect2.php')" /></td>
          </tr>
        </table>
    </form>
</div>
	<?php
	if (isset($_GET["tplid"]) && isset($_GET["planilla"])){
		$num_planilla=texto_limpio($_GET["planilla"]);
		$tipo_planilla_id =texto_limpio($_GET["tplid"]);
		$con = new mysqli($host,$user,$clave,$db,$puerto);
		if (mysqli_connect_error()) {
			die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$stmt = $con->stmt_init();
		$stmt->prepare('call sel_planilla3(?,?)');
		$stmt->bind_param('ii',$num_planilla,$tipo_planilla_id);
		if(!$stmt->execute()){
			throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
		}else{
			$stmt->store_result();
			$cuantos_registros = $stmt->num_rows;
			$excel='';
			if($cuantos_registros>0){
				$stmt->bind_result($tipo,$tipo_planilla_id,$tipo_planilla_desc,$num_planilla,$total_planilla,$registro_id,$registro_nomb,$deposito_id,$num_deposito,$banco,$deposito_monto,
								   $num_aprob,$pto_venta_monto,$timbre_monto,$creadopor,$fecha_creado,$ip);
																								
				?><div class="contenedor2"  id="exp_t">                
                  <table id="t_deposito"  border="1" cellpadding="0" cellspacing="0">
                    <tbody id="tb_deposito">
                        <tr bgcolor="#9999FF">
                            <td width="4%"></td>
                            <td >Tipo de Info</td>
                            <td>Registro</td>
                            <td >Num. Planilla</td>
                            <td >Total en Planilla</td>
                            <td >Num. Deposito</td>
                            <td >Banco</td>
                            <td >Monto Deposito</td>
                            <td >Num. Aprob</td>
                            <td >Monto de Transaccion</td>
                            <td >Monto por Timbre</td>
                            <td >Operacion Registrada Por</td>
                            <td >Fecha Operacion</td>
                            <td >IP</td>                                                                                    
                        </tr><?php
						//$excel = "TIPO DE INFO\tNUM. DE PLANILLA\tTOTAL EN PLANILLA\tNUM. DE DEPOSITO\tBANCO\tMONTO DEL DEPOSITO\tAPROBACION DE TRANSACCION\tMONTO DE TRANSACCION\tMONTO TIMBRE\tREGISTRADO POR\tFECHA DE OPERACION\tIP\n\n";
				$cont=0;
				$cco=0;
				$color1='#FFFFFF';
				$color2='#E6F2E1';
				$color_temp='';
				$num_planilla_ultimo='';
				while($stmt->fetch()){
					$cco++; 
					$cont++;
/*					if ($cont==1){
						$num_planilla_ultimo=$num_planilla;	
					}
					
					if ($num_planilla_ultimo==$num_planilla){
						$color=$color1;
					}else{
						$color=$color2;
						$color_temp=$color1;
						$color1=$color2;
						$color2=$color_temp;
						$num_planilla_ultimo==$num_planilla;
					}*/
					
/*					if(($cco%2)==0){ 
						$color = '#FFFFFF'; 
					}else{
						$color = '#E6F2E1'; 
					}*/$color = '#FFFFFF'; 
					?><tr onmouseover="ColorUno(this,'#CCCCCC');" onmouseout="ColorDos(this,'<?php echo $color; ?>')" bgcolor="<?php echo $color; ?>"><td><?php echo $cont ?></td><td><?php echo $tipo ?></td><td><?php echo $registro_nomb ?></td><td ><a href="../redirect2.php?p=2&planilla=<?php echo $num_planilla ?>&tplid=<?php echo $tipo_planilla_id ?>&adm=1"><?php echo $tipo_planilla_desc.'-'.$num_planilla ?></a></td><td ><?php echo bsf($total_planilla) ?></td><td><?php echo $num_deposito ?></td><td ><?php echo $banco ?></td> <td ><?php echo bsf($deposito_monto) ?></td><td ><?php echo $num_aprob ?></td><td ><?php echo bsf($pto_venta_monto) ?></td><td ><?php echo bsf($timbre_monto) ?></td><td ><?php echo $creadopor ?></td><td ><?php echo $fecha_creado ?></td><td ><?php echo $ip ?></td></tr><?php										
				}?></tbody>
                </table>
            </div>
            <div class="contenedor2">
			<br />
            <table>
            	<tr>
                    <td>                        
                    <td>
                        <form action="export_table.php"  method="post" id="env_tabla" name="env_tabla" onsubmit="return enviar_form()">
                        	<input type="hidden" name="tabla_html" id="tabla_html" value="">
                            <input type="hidden" name="f_nombre" value="Planilla_<?php echo $num_planilla ?>" />
                            <input type = "submit" value = "Exportar a Excel"  />
                        </form>
                    </td>
                </tr>
            </table>                                     
				</div><?php
			}else{
				?><div class="contenedor" align="center">
                <table width="420" border="1" bgcolor="#FF0000">
                  	<tr><td width="396" align="center">  NO SE ENCUNTRAN REGISTROS PARA SU SOLICITUD  </td></tr>
                </table>
				</div><?php
			}
		}
		$stmt->free_result();
		$stmt->close();
		while($con->next_result()) { }
	}?>
</body>
</html>