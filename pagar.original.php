<?php include('seguridad_trans.php');
include('conex.php');
include('funciones/funcion.php');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="funciones/funciones1.js"></script>
<script type="text/javascript" src="funciones/calendarDateInput.js"></script>
<script type="text/javascript" src="funciones/ajax.js"></script>
<script type="text/javascript">
var forma_pago = null;
var banco_id_deposito = null;
var cuenta_deposito = null;
var num_deposito= null;
var monto_deposito = null;
var banco_id_transac =null;
var num_aprob_transac = null;
var monto_transac = null;

var selector = null;
var indice = null;

function OcultarTodo(){
	Ocultar('deposito');
	Ocultar('pto_venta');
	Ocultar('boton_enviar');
}
function MuestraTodo(){
	Mostrar('deposito');
	Mostrar('pto_venta');
	Mostrar('boton_enviar');
}
function attach_file( p_script_url ) {
		// create new script element, set its relative URL, and load it
		script = document.createElement( 'script' );
		script.src = p_script_url;
		document.getElementsByTagName( 'head' )[0].appendChild( script );
}
function select_banco(valor){
	if (valor==0){
		Ocultar('sel_cuenta');
		return false;
	}else{
		loadurl('form_sel_cuenta.php?banco_id='+valor,'cuentas');
		return true;
	}
}
function check_selected(valor){
	if (valor==0){
		OcultarTodo();
		return false;
	}else if (valor==1){
		OcultarTodo();
		Mostrar('deposito');
		Mostrar('boton_enviar');
		return true;
	}else if (valor==2){
		OcultarTodo();
		Mostrar('pto_venta');
		Mostrar('boton_enviar');
		return true;
	}else if (valor==3){
		MuestraTodo();
		return true;
	}
}
function valida_deposito(){
	banco_id_deposito=document.f_monto.sel_banco.value;
	num_deposito=trim(document.f_monto.num_deposito.value);
	monto_deposito=trim(document.f_monto.monto_deposito.value);
	if (banco_id_deposito==0){
		alert('NO SE HA SELECCIONADO UN BANCO EN LA SECCION DEPOSITO');
		document.f_monto.sel_banco.focus();
		return false;
	}
	if(monto_deposito!=''){
		if(IsNumeric(monto_deposito)) {
			if(num_deposito!=''){
				if (IsNumericInt(num_deposito)){
					return true;
				}else{
					alert('ERROR EN EL NUMERO DEL DEPOSITO');
					document.f_monto.num_deposito.focus();
					return false;
				}
			}else{
				alert('ERROR EN EL NUMERO DEL DEPOSITO');
				document.f_monto.num_deposito.focus();
				return false;			
			}
		}else{
			alert('ERROR EN EL MONTO DEL DEPOSITO');
			document.f_monto.monto_deposito.focus();
			return false;
		}
	}else{
		alert('ERROR EN EL MONTO DEL DEPOSITO');
		document.f_monto.monto_deposito.focus();
		return false;	
	}	
}
function valida_pto_venta(){
	banco_id_transac =document.f_monto.sel_banco_transac.value;
	num_aprob_transac = trim(document.f_monto.num_aprob_transac.value);
	monto_transac = trim(document.f_monto.monto_transac.value);
	if (banco_id_transac==0){
		alert('NO SE HA SELECCIONADO UN BANCO EN LA SECCION PUNTO DE VENTA');
		document.f_monto.sel_banco_transac.focus();
		return false;
	}
	if (num_aprob_transac!=''){
		if (IsNumericInt(num_aprob_transac)){
			if (monto_transac!=''){
				if(IsNumeric(monto_transac)) {
					return true;
				}else{
					alert('ERROR EN MONTO (PUNTO DE VENTA)');
					document.f_monto.monto_transac.focus();
					return false;			
				}
			}else{
				alert('ERROR EN MONTO (PUNTO DE VENTA)');
				document.f_monto.monto_transac.focus();
				return false;		
			}
		}else{
			alert('ERROR EN NUMERO DE APROBACION');
			document.f_monto.num_aprob_transac.focus();
			return false;
		}
	}else{
		alert('ERROR EN NUMERO DE APROBACION');
		document.f_monto.num_aprob_transac.focus();
		return false;	
	}
}
function submit_form(){
	forma_pago=document.f_monto.sel_forma_pago.value
	if (forma_pago==1){ // 1. DEPOSITO
		if(valida_deposito()==true){
			/*AQUI VA EL CODIGO QUE CHEQUEA EL MONTO Y SI NO ES TOTAL RECARGA LA PAGINA CON OTRO FORM DE DEPOSITO*/
			alert('submit');
		}
	}else if (forma_pago==2){ // 2. PTO VENTA
		if(valida_pto_venta()==true){
			/*AQUI VA EL CODIGO QUE CHEQUEA EL MONTO Y SI NO ES TOTAL RECARGA LA PAGINA CON OTRO FORM DE PTO VENTA*/
			alert('submit');
		}
	}else if (forma_pago==3){ // 3. MIXTO
		if(valida_pto_venta()==true){
			if(valida_deposito()==true){
				alert('submit');
			}
		}		
	}
}
</script>
</head>
<?php
if (!isset($_GET["planilla"])){
	die('ERROR EN NUMERO DE PLANILLA');
}else{
	$planilla=$_GET["planilla"];
}
if (!isset($_GET["total"])){
	die('ERROR EN TOTAL A PAGAR');
}else{
	$total=$_GET["total"];
}


?>
<body onload="javascript:loadurl('menu2.php','menu');">
<span id="menu"></span>
<br />
<div class="contenedor" align="center">
	<h2>Planilla N�: <?php echo $planilla ?></h2>
    <h2>Total: <?php echo $total ?></h2>
</div>
<div class="contenedor" id="consulta">
    <form id="f_monto" name="f_monto" method="post" >
        <table width="54%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas">
          <tr>
            <td width="56%" ><h2>Seleccione Forma de Pago:</h2></td>
            <td width="25%" align="center" >
         <select name="sel_forma_pago" id="sel_forma_pago" onChange="check_selected(this.value)" onkeyup="check_selected(this.value)" >
		 <option value="0" selected="selected"></option><?php
			$con = new mysqli($host,$user,$clave,$db,$puerto);
			if (mysqli_connect_error()) {
				die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
			}
			$stmt = $con->stmt_init();
			$stmt->prepare('call list_forma_pago()');
			if(!$stmt->execute()){
				throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
			}else{
				$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
				$cuantos_registros = $stmt->num_rows;
				if($cuantos_registros>0){
					$stmt->bind_result($forma_pago_id,$forma_pago_descripcion,$orden);
					while($stmt->fetch()){
						echo '<option value="' . $forma_pago_id . '">' . $forma_pago_descripcion . '</option>';
					}
				}
			}
			$stmt->free_result();
			$stmt->close();
			while($con->next_result()) { }
			$con->close();					            	
            ?></select>
            <input name="planilla" type="hidden" value="" /></td>
          </tr>
        </table>
        <span id="deposito" style="display:block">
            <br  />
            <table border="1" align="center" class="tablas">
                <tr>
                    <td  align="right"><strong>Seleccione Banco:</strong></td>
                    <td >
                   	  <select name="sel_banco" id="sel_banco" onChange="select_banco(this.value)" onkeyup="select_banco(this.value)">
					  		<option value="0" selected="selected"></option><?php
							$con = new mysqli($host,$user,$clave,$db,$puerto);
							if (mysqli_connect_error()) {
								die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
							}
							$stmt = $con->stmt_init();
							$stmt->prepare('call list_banco()');
							if(!$stmt->execute()){
								throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
							}else{
								$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
								$cuantos_registros = $stmt->num_rows;
								if($cuantos_registros>0){
									$stmt->bind_result($id,$descripcion);
									while($stmt->fetch()){
										echo '<option value="' . $id . '">' . $descripcion . '</option>';
									}
								}
							}
							$stmt->free_result();
							$stmt->close();
							while($con->next_result()) { }
							$con->close();                        	
                    	?></select>
                    </td>
                </tr>
                <tr>
                    <td align="right"><strong>Seleccione Cuenta:</strong></td>
                    <td><span id="cuentas"></span></td>
                </tr>
                <tr>
                    <td align="right"><strong>Fecha del Deposito:</strong>
                    	<br />
                        <strong><font size="-3">(Click en el Calendario)</font></strong>
                  </td>
                    <td><script>DateInput('fecha_deposito',true)</script></td>
                </tr>
                <tr>
                    <td align="right"><strong>N� Deposito:</strong></td>
                    <td><input name="num_deposito" type="text" size="13" maxlength="13" onkeypress="return soloEnteros(event)" /></td>
                </tr>
                <tr>
                    <td align="right"><strong>Monto:</strong></td>
                    <td><input name="monto_deposito" type="text" size="20" maxlength="20" onkeypress="return soloNumeros(event)" /></td>
                </tr>                  
            </table>
        </span>
        <span id="pto_venta" style="display:block">
        <br />
        <table align="center" class="tablas" border="1">
                <tr>
                    <td align="right"><strong>Seleccione Banco:</strong></td>
                    <td><select name="sel_banco_transac" id="sel_banco_transac">
                        	<option value="2">BANCO DEL TESORO</option>                           
                        </select>
					</td>
                </tr>
                <tr>
                    <td align="right"><strong>N&ordm; Aprobacion:</strong></td>
                    <td><input name="num_aprob_transac" type="text" size="13" maxlength="13" onkeypress="return soloEnteros(event)" /></td>
                </tr>
                <tr>
                    <td align="right"><strong>Fecha de la Transaccion:</strong>
                    	<br />
                        <strong><font size="-3">(Click en el Calendario)</font></strong>
                  </td>
                    <td><script>DateInput('fecha_transac',true)</script></td>
                </tr>
                <tr>
                    <td align="right"><strong>Monto:</strong></td>
                    <td><input name="monto_transac" type="text" size="20" maxlength="20" onkeypress="return soloNumeros(event)" /></td>
                </tr>  
            </table>            
        </span>
        <span id="boton_enviar" style="display:block">
            <table align="center">
                <tr>
                	<td align="center"><input name="enviar" type="button" onClick="submit_form();" value="   Enviar   " /></td>
                </tr>    
            </table>            
        </span>              
    </form>
</div>
</body>
</html>