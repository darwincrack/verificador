<?php 
include('seguridad_trans.php'); 
include('conex.php');

$usuario = $_SESSION["usuario"];
$pwd=$_SESSION["password"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="styles/contenido.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="funciones/ajax.js"></script>
<script type="text/javascript">
/*javascript:window.history.forward(1);*/ 
var selector = null;
var indice = null;
var SelectorTexto = null;
function submit_form() {
		if(document.f_actos.sel_numeral.value == "0") {
			alert('DEBE SELECCIONAR UN NUMERAL');
			return false;
		}else{
			selector = document.getElementById('sel_numeral');
			indice = (selector.selectedIndex);	
			SelectorTexto = selector.options[indice].text;
			document.f_actos.tipo_acto.value=SelectorTexto;
			document.f_actos.submit(); 
		}
}
function attach_file( p_script_url ) {
		// create new script element, set its relative URL, and load it
		script = document.createElement( 'script' );
		script.src = p_script_url;
		document.getElementsByTagName( 'head' )[0].appendChild( script );
}
</script>
</head>
<body onload="javascript:loadurl('menu2.php','menu');">
<span id="menu">
</span>
<div class="contenedor">
	<h1>Calculo de Actos Realizados Art. 13 de La Ley de Timbre Fiscal</h1>
</div>
<div class="contenedor" id="consulta">
<form id="f_actos" name="f_actos" method="get" action="redirect.php" >
<table width="85%" border="0" align="center" cellpadding="1" cellspacing="1" class="tablas">
      <tr>
        <td ><h2>Acto/Numeral:</h2></td>
        <td >
			<select name="sel_numeral" id="sel_numeral" >
            <option selected value="0">Seleccione Numeral</option><?php 
			$con = new mysqli($host,$user,$clave,$db,$puerto);
			if (mysqli_connect_error()) {
				die('HA HABIDO UN ERROR EN LA CONEXION, CONTACTAR A COORDINACION DE INFORMATICA. NUMERO ERROR: (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
			}
			$stmt = $con->stmt_init();
			$stmt->prepare('call list_acto(?,?)');
			$stmt->bind_param('ss', $usuario, $pwd);
			if(!$stmt->execute()){
				throw new Exception('No se pudo realizar la consulta:' . $stmt->error);
			}else{
				$stmt->store_result(); //Sin esta l�nea no podemos obtener el total de resultados anticipadamente
				$cuantos_registros = $stmt->num_rows;
				if($cuantos_registros>0){
					$stmt->bind_result($acto,$valor);
						while($stmt->fetch()){	
						print "<option value=\"$valor\">$acto</option>";
					}                      	
				}
			}
			$stmt->free_result();
			$stmt->close();
			while($con->next_result()) { }
			$con->close();?>
            </select><input name="tipo_acto" type="hidden" value="" />           
        </td>
        <td width="10%"><input type="button" value="  Enviar  " class="boton" name="busca"  onclick="return submit_form()" /></td>
      </tr>
</table>
</form>
</div>
</body>
</html>